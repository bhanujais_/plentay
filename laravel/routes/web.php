<?php
//Clear Cache facade value:
// Route::get('/now/clear-cache', function() {
//     $exitCode = Artisan::call('cache:clear');
//     return '<h1>Cache facade value cleared</h1>';
// });


// //Clear Route cache:
// Route::get('/route-clear', function() {
//     $exitCode = Artisan::call('route:clear');
//     return '<h1>Route cache cleared</h1>';
// });

// //Clear View cache:
// Route::get('/view-clear', function() {
//     $exitCode = Artisan::call('view:clear');
//     return '<h1>View cache cleared</h1>';
// });
// //Clear View cache:
Route::get('/quick-clean', function() {
    $exitCode = Artisan::call('route:clear');
    $exitCode = Artisan::call('view:clear');
    $exitCode = Artisan::call('cache:clear');
    // $exitCode = Artisan::call('config:cache');
    return '<h1>Cleared</h1>';
});
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('/test', function () {
//     return count(App\User::find(1)->products);
// });

//homepage
// Route::get('/', function () {
//     return view('index');
// });

// Rest Api
Route::get('/api/about-us',[
    'uses'=>'ApiController@aboutUs',
]);
Route::get('/api/privacy-policy',[
    'uses'=>'ApiController@privacyPolicy',
]);
Route::get('/api/terms-conditions',[
    'uses'=>'ApiController@termsConditions',
]);

Route::post('/api/contact-us',[
        'uses'=>'ApiController@contact_send',
]);
// Rest api end


Route::get('/',[
    'uses'=>'FrontEndController@index',
    'as'=>'index'
]);
/////////Blog Posts
Route::get('/blogs',[
    'uses'=>'FrontEndController@blogs',
    'as'=>'blogs'
]);
//post page url slug
Route::get('/post/{slug}',[
    'uses'=>'FrontEndController@post_page',
    'as'=>'post_page'
]);
////////////////////////////////////////////
//Products
Route::get('/products',[
    'uses'=>'ProductsController@products',
    'as'=>'products'
]);
//Product Link url
Route::get('/link/{slug}',[
    'uses'=>'ProductsController@link',
    'as'=>'link'
]);
//product page url slug
Route::get('/{slug}-{id}',[
    'uses'=>'ProductsController@product_page',
    'as'=>'product_page'
]);
///////////////////////////////////////////
//category page url slug
Route::get('/category/{slug}',[
    'uses'=>'FrontEndController@category_page',
    'as'=>'category_page'
]);
//merchant url slug
Route::get('/merchant/{slug}',[
    'uses'=>'FrontEndController@merchant_page',
    'as'=>'merchant_page'
]);
//merchants
Route::get('/merchants',[
    'uses'=>'FrontEndController@merchants',
    'as'=>'merchants'
]);
Route::get('/page/{slug}',[
    'uses'=>'FrontEndController@single_page',
    'as'=>'single_page'
]);
//Search
Route::get('/search',[
    'uses'=>'ProductsController@search',
    'as'=>'search'
]);

Route::get('/feed',[
    'uses'=>'FrontEndController@feed',
    'as'=>'feed'
]);
//contact page
Route::get('/contact',[
    'uses'=>'FrontEndController@contact',
    'as'=>'contact'
]);
Route::post('/contact/send',[
    'uses'=>'FrontEndController@contact_send',
    'as'=>'contact.send'
]);
Route::get('register/verify/{token}', [
  'uses' => 'Auth\RegisterController@verifyUser',
  'as' => 'verifyUser'
]);


/*
|--------------------------------------------------------------------------
| Application Public Environment
|--------------------------------------------------------------------------
|
| This value determines the public features of the application is currently
| running in. This may determine how you prefer to configure various
| services your application utilizes.
|
*/
////////////////////////////////////////////////////////////////////////////
Route::get('/osx', function () {
    return view('osx');
});

Auth::routes();

// Route::get('/work/app', function () {
//     return view('work.layouts.app');
// });
Route::get('/work/login',[
      'uses'=>'Auth\AdminLoginController@showLoginForm',
      'as'=>'work.login'
  ]);
  Route::post('/work/login/submit',[
        'uses'=>'Auth\AdminLoginController@login',
        'as'=>'work.login.submit'
    ]);
  Route::get('/work',[
          'uses'=>'work\AdminsController@index',
          'as'=>'work.index'
    ]);
Route::post('/work/logout',[
          'uses'=>'Auth\AdminLoginController@adminLogout',
          'as'=>'work.logout'
      ]);


//////////////////CRAWLER///////////////////////////////////
Route::post('/work/crawl/konga_run',[
          'uses'=>'work\crawler\KongaController@konga_run',
          'as'=>'work.crawl.konga_run'
      ]);
Route::get('/work/crawl/konga',[
                'uses'=>'work\crawler\KongaController@index',
                'as'=>'work.crawl.konga'
      ]);
Route::get('/work/edit/konga_affiliate_id',[
                'uses'=>'work\crawler\KongaController@edit_konga',
                'as'=>'work.edit.konga'
      ]);
Route::post('/work/save/konga_affiliate_id',[
          'uses'=>'work\crawler\KongaController@save_konga',
          'as'=>'work.save.konga'
      ]);
///////
Route::post('/work/crawl/jumia_run',[
          'uses'=>'work\crawler\JumiaController@jumia_run',
          'as'=>'work.crawl.jumia_run'
      ]);
Route::get('/work/crawl/jumia',[
                'uses'=>'work\crawler\JumiaController@index',
                'as'=>'work.crawl.jumia'
      ]);
Route::get('/work/edit/jumia_affiliate_id',[
                'uses'=>'work\crawler\JumiaController@edit_jumia',
                'as'=>'work.edit.jumia'
      ]);
Route::post('/work/save/save_jumia',[
          'uses'=>'work\crawler\JumiaController@save_jumia',
          'as'=>'work.save.jumia'
      ]);
///////
Route::post('/work/crawl/ebay_run',[
          'uses'=>'work\crawler\EbayController@ebay_run',
          'as'=>'work.crawl.ebay_run'
      ]);
Route::get('/work/crawl/ebay',[
                'uses'=>'work\crawler\EbayController@index',
                'as'=>'work.crawl.ebay'
      ]);
Route::post('/work/save/ebay_affiliate_id',[
          'uses'=>'work\crawler\EbayController@save_ebay',
          'as'=>'work.save.ebay'
      ]);
Route::get('/work/edit/ebay_affiliate_id',[
                'uses'=>'work\crawler\EbayController@edit_ebay',
                'as'=>'work.edit.ebay'
      ]);
Route::post('/work/save/ebay_affiliate_id',[
          'uses'=>'work\crawler\EbayController@save_ebay',
          'as'=>'work.save.ebay'
      ]);
//////////////////CRAWLER//////////////////////////////////////
Route::post('/work/get_admins_data',[
                'uses'=>'work\AdminsController@get_admins_data',
                'as'=>'work.get_admins_data'
]);
Route::get('/work/admin/jumia',[
          'uses'=>'work\AdminsController@create',
          'as'=>'work.admin.create'
    ]);
      Route::post('/work/admin/store',[
          'uses'=>'work\AdminsController@store',
          'as'=>'work.admin.store'
    ]);
      Route::get('/work/admins',[
          'uses'=>'work\AdminsController@view_admins',
          'as'=>'admins'
    ]);
      Route::get('/work/admin/edit/{id}',[
          'uses'=>'work\AdminsController@edit',
          'as'=>'work.admin.edit'
    ]);
      Route::get('/work/admin/csv/{id}',[
          'uses'=>'work\AdminsController@csv',
          'as'=>'work.admin.csv'
    ]);
      Route::get('/work/admin/delete/{id}',[
          'uses'=>'work\AdminsController@destroy',
          'as'=>'work.admin.delete'
    ]);
      Route::post('/work/admin/update/{id}',[
          'uses'=>'work\AdminsController@update',
          'as'=>'work.admin.update'
      ]);
      Route::get('/work/profile',[
          'uses'=>'work\AdminsController@admin_profile',
          'as'=>'work.admin.profile'
      ]);
      Route::get('/work/password',[
          'uses'=>'work\AdminsController@password',
          'as'=>'work.admin.password'
      ]);
      Route::post('/work/store_password',[
          'uses'=>'work\AdminsController@store_password',
          'as'=>'work.admin.store_password'
      ]);
      // Route::get('user/deactivatework/{id}',[
      //     'uses'=>'UsersController@deactivatework',
      //     'as'=>'user.deactivatework'
      // ]);
      // Route::get('user/activatework/{id}',[
      //     'uses'=>'UsersController@activatework',
      //     'as'=>'user.activatework'
      // ]);
      //
      ////////Admins//////////////////

      ///////Company////////////////////
            Route::get('/work/get_companys_data',[
                            'uses'=>'work\CompanyController@get_companys_data',
                            'as'=>'work.get_companys_data'
            ]); 
            Route::get('/work/company/create',[
                'uses'=>'work\CompanyController@create',
                'as'=>'work.company.create'
            ]);
            Route::post('/work/company/store',[
                'uses'=>'work\CompanyController@store',
                'as'=>'work.company.store'
            ]);
            Route::get('/work/companys',[
                'uses'=>'work\CompanyController@view_companys',
                'as'=>'companys'
            ]);
            Route::get('/work/companys/user/{id}',[
                'uses'=>'work\CompanyController@view_companys_users',
                'as'=>'company.users'
            ]);
            /*Route::get('/work/user/edit/{id}',[
                'uses'=>'work\CompanyController@edit',
                'as'=>'work.user.edit'
            ]);
            Route::get('/work/user/delete/{id}',[
                'uses'=>'work\CompanyController@destroy',
                'as'=>'work.user.delete'
            ]);
            Route::post('/work/user/update/{id}',[
                'uses'=>'work\CompanyController@update',
                'as'=>'work.user.update'
            ]);
            Route::get('/work/user/csv/{id}',[
                'uses'=>'work\CompanyController@csv',
                'as'=>'work.user.csv'
            ]);
            Route::get('/work/user/profile/{id}',[
               'uses'=>'work\CompanyController@profile',
               'as'=>'work.user.profile'
           ]);
           Route::get('/work/user/profile/update/{id}',[
               'uses'=>'work\CompanyController@profile_update',
               'as'=>'work.user.profile_update'
           ]);
           Route::get('/work/user/activate/{id}',[
              'uses'=>'work\CompanyController@activate',
              'as'=>'work.user.activate'
          ]);
          Route::get('/work/user/deactivate/{id}',[
              'uses'=>'work\CompanyController@deactivate',
              'as'=>'work.user.deactivate'
          ]);
          Route::get('/work/user/update_products/{id}',[
             'uses'=>'work\CompanyController@update_products',
             'as'=>'work.user.update_products'
         ]);*/
//////Company/////////////////////////

      ///////Users////////////////////
      Route::get('/work/get_users_data',[
                      'uses'=>'work\UsersController@get_users_data',
                      'as'=>'work.get_users_data'
      ]);

      Route::get('/work/user/create',[
                'uses'=>'work\UsersController@create',
                'as'=>'work.user.create'
            ]);
            Route::post('/work/user/store',[
                'uses'=>'work\UsersController@store',
                'as'=>'work.user.store'
            ]);
            Route::get('/work/users',[
                'uses'=>'work\UsersController@view_users',
                'as'=>'users'
            ]);
            Route::get('/work/user/edit/{id}',[
                'uses'=>'work\UsersController@edit',
                'as'=>'work.user.edit'
            ]);
            Route::get('/work/user/delete/{id}',[
                'uses'=>'work\UsersController@destroy',
                'as'=>'work.user.delete'
            ]);
            Route::post('/work/user/update/{id}',[
                'uses'=>'work\UsersController@update',
                'as'=>'work.user.update'
            ]);
            Route::get('/work/user/csv/{id}',[
                'uses'=>'work\UsersController@csv',
                'as'=>'work.user.csv'
            ]);
            Route::get('/work/user/profile/{id}',[
               'uses'=>'work\UsersController@profile',
               'as'=>'work.user.profile'
           ]);
           Route::get('/work/user/profile/update/{id}',[
               'uses'=>'work\UsersController@profile_update',
               'as'=>'work.user.profile_update'
           ]);
           Route::get('/work/user/activate/{id}',[
              'uses'=>'work\UsersController@activate',
              'as'=>'work.user.activate'
          ]);
          Route::get('/work/user/deactivate/{id}',[
              'uses'=>'work\UsersController@deactivate',
              'as'=>'work.user.deactivate'
          ]);
          Route::get('/work/user/update_products/{id}',[
             'uses'=>'work\UsersController@update_products',
             'as'=>'work.user.update_products'
         ]);
//////Users/////////////////////////
////////////Categories//////////////
Route::get('/work/get_categories_data',[
                'uses'=>'work\CategoriesController@get_categories_data',
                'as'=>'work.get_categories_data'
]);

Route::get('/work/category/create',[
          'uses'=>'work\CategoriesController@create',
          'as'=>'work.category.create'
      ]);
      Route::post('/work/category/store',[
          'uses'=>'work\CategoriesController@store',
          'as'=>'work.category.store'
      ]);
      Route::get('/work/categories',[
          'uses'=>'work\CategoriesController@view_categories',
          'as'=>'categories'
      ]);
      Route::get('/work/category/edit/{id}',[
          'uses'=>'work\CategoriesController@edit',
          'as'=>'work.category.edit'
      ]);
      Route::get('/work/category/delete/{id}',[
          'uses'=>'work\CategoriesController@destroy',
          'as'=>'work.category.delete'
      ]);
      Route::post('/work/category/update/{id}',[
          'uses'=>'work\CategoriesController@update',
          'as'=>'work.category.update'
      ]);
////////////Categories//////////////
////////// Start Add Products  //////
Route::get('/work/get_products_data',[
                'uses'=>'work\ProductsController@get_products_data',
                'as'=>'work.get_products_data'
]);

Route::get('/work/product/create',[
          'uses'=>'work\ProductsController@create',
          'as'=>'work.product.create'
      ]);
Route::post('/work/product/store',[
          'uses'=>'work\ProductsController@store',
          'as'=>'work.product.store'
]);
      Route::get('/work/products',[
          'uses'=>'work\ProductsController@index',
          'as'=>'products'
      ]);
      Route::get('/work/product/edit/{id}',[
          'uses'=>'work\ProductsController@edit',
          'as'=>'work.product.edit'
      ]);
      Route::get('/work/product/delete/{id}',[
          'uses'=>'work\ProductsController@destroy',
          'as'=>'work.product.delete'
      ]);
      Route::post('/work/product/update/{id}',[
          'uses'=>'work\ProductsController@update',
          'as'=>'work.product.update'
      ]);
      Route::get('/work/product/update_product/{id}',[
          'uses'=>'work\ProductsController@update_product',
          'as'=>'work.product.update_product'
      ]);
      Route::get('/work/product/import',[
                'uses'=>'work\ProductsController@import',
                'as'=>'work.product.import'
      ]);
      Route::post('/work/product/csv_upload',[
                'uses'=>'work\ProductsController@csv_upload',
                'as'=>'work.product.csv_upload'
      ]);
      Route::post('/work/product/csv_gz_unzip',[
                'uses'=>'work\ProductsController@csv_gz_unzip',
                'as'=>'work.product.csv_gz_unzip'
      ]);
      Route::post('/work/product/csv_import',[
                'uses'=>'work\ProductsController@csv_import',
                'as'=>'work.product.csv_import'
      ]);
////////// End Add Product   ////////
////////// Credits   ////////////
Route::get('/work/finance/credit',[
    'uses'=>'work\CreditsController@index',
    'as'=>'work.credits'
]);
Route::get('/work/get_credit_data',[
                'uses'=>'work\CreditsController@get_credit_data',
                'as'=>'work.get_credit_data'
]);
Route::get('/work/finance/edit_credit/{id}',[
    'uses'=>'work\CreditsController@edit',
    'as'=>'work.credit.edit'
]);
Route::post('/work/finance/update/{id}',[
    'uses'=>'work\CreditsController@update',
    'as'=>'work.credit.update'
]);
////////// End Credits   ////////
////////// Invoices   //////////
Route::get('/work/get_invoice_data',[
                'uses'=>'work\InvoicesController@get_invoice_data',
                'as'=>'work.get_invoice_data'
]);
Route::post('/work/finance/store',[
          'uses'=>'work\InvoicesController@store',
          'as'=>'work.invoice.store'
]);
Route::get('/work/finance/invoices',[
    'uses'=>'work\InvoicesController@index',
    'as'=>'work.invoices'
]);
Route::get('/work/finance/delete/{id}',[
    'uses'=>'work\InvoicesController@destroy',
    'as'=>'work.invoice.delete'
]);
Route::get('/work/finance/create_invoice',[
          'uses'=>'work\InvoicesController@create',
          'as'=>'work.invoice.create'
      ]);
Route::get('/work/finance/csv/{id}',[
    'uses'=>'work\InvoicesController@csv',
    'as'=>'work.invoice.csv'
]);
Route::get('/work/finance/pdf/{id}',[
    'uses'=>'work\InvoicesController@pdf',
    'as'=>'work.invoice.pdf'
]);
Route::get('/work/finance/view/{id}',[
     'uses'=>'work\InvoicesController@view',
     'as'=>'work.invoice.view'
]);
Route::get('/work/finance/approve/{id}',[
         'uses'=>'work\InvoicesController@approve',
         'as'=>'work.invoice.approve'
      ]);
Route::get('/work/finance/disapprove/{id}',[
         'uses'=>'work\InvoicesController@disapprove',
         'as'=>'work.invoice.disapprove'
      ]);
Route::get('/work/finance/invoice_details/{id}',[
          'uses'=>'work\InvoicesController@disapprove',
          'as'=>'work.invoice.details'
      ]);
//////////Reports //////////////
Route::get('/work/get_reports_data',[
                'uses'=>'work\ReportsController@get_reports_data',
                'as'=>'work.get_reports_data'
]);
Route::get('/work/finance/reports',[
    'uses'=>'work\ReportsController@index',
    'as'=>'work.reports'
]);
Route::get('/work/report/delete/{id}',[
    'uses'=>'work\ReportsController@destroy',
    'as'=>'work.report.delete'
]);
Route::get('/work/report/delete_all',[
    'uses'=>'work\ReportsController@delete_all',
    'as'=>'work.delete_all.reports'
]);
Route::get('/work/report/export',[
    'uses'=>'work\ReportsController@export',
    'as'=>'work.export.csv'
]);
Route::get('/work/report/csv/{id}',[
    'uses'=>'work\ReportsController@csv',
    'as'=>'work.report.csv'
]);
////////// Reports/////////////
///////// End Credits   ////////
////Banks Start////
Route::get('/work/get_banks_data',[
                'uses'=>'work\BanksController@get_banks_data',
                'as'=>'work.get_banks_data'
]);

Route::get('/work/finance/create_bank',[
          'uses'=>'work\BanksController@create',
          'as'=>'work.bank.create'
      ]);
Route::post('/work/finance/store_bank',[
          'uses'=>'work\BanksController@store',
          'as'=>'work.bank.store'
]);
      Route::get('/work/banks',[
          'uses'=>'work\BanksController@index',
          'as'=>'work.banks'
      ]);
      Route::get('/work/finance/edit_bank/{id}',[
          'uses'=>'work\BanksController@edit',
          'as'=>'work.bank.edit'
      ]);
      Route::get('/work/finance/delete_bank/{id}',[
          'uses'=>'work\BanksController@destroy',
          'as'=>'work.bank.delete'
      ]);
      Route::post('/work/finance/update_bank/{id}',[
          'uses'=>'work\BanksController@update',
          'as'=>'work.bank.update'
      ]);
////Banks End////
////Pages Start////
Route::get('/work/page/view/{id}',[
    'uses'=>'work\PagesController@view',
    'as'=>'work.page.view'
]);
Route::get('/work/get_pages_data',[
                'uses'=>'work\PagesController@get_pages_data',
                'as'=>'work.get_pages_data'
]);

Route::get('/work/page/create',[
          'uses'=>'work\PagesController@create',
          'as'=>'work.page.create'
      ]);
Route::post('/work/page/store',[
          'uses'=>'work\PagesController@store',
          'as'=>'work.page.store'
]);
      Route::get('/work/pages',[
          'uses'=>'work\PagesController@index',
          'as'=>'work.pages'
      ]);
      Route::get('/work/page/edit/{id}',[
          'uses'=>'work\PagesController@edit',
          'as'=>'work.page.edit'
      ]);
      Route::get('/work/page/delete/{id}',[
          'uses'=>'work\PagesController@destroy',
          'as'=>'work.page.delete'
      ]);
      Route::post('/work/page/update/{id}',[
          'uses'=>'work\PagesController@update',
          'as'=>'work.page.update'
      ]);
////Pages End////
////Posts Start////
Route::get('/work/post/view/{id}',[
    'uses'=>'work\PostsController@view',
    'as'=>'work.post.view'
]);
Route::get('/work/get_posts_data',[
                'uses'=>'work\PostsController@get_posts_data',
                'as'=>'work.get_posts_data'
]);

Route::get('/work/post/create',[
          'uses'=>'work\PostsController@create',
          'as'=>'work.post.create'
      ]);
Route::post('/work/post/store',[
          'uses'=>'work\PostsController@store',
          'as'=>'work.post.store'
]);
      Route::get('/work/posts',[
          'uses'=>'work\PostsController@index',
          'as'=>'work.posts'
      ]);
      Route::get('/work/post/edit/{id}',[
          'uses'=>'work\PostsController@edit',
          'as'=>'work.post.edit'
      ]);
      Route::get('/work/post/delete/{id}',[
          'uses'=>'work\PostsController@destroy',
          'as'=>'work.post.delete'
      ]);
      Route::post('/work/post/update/{id}',[
          'uses'=>'work\PostsController@update',
          'as'=>'work.post.update'
      ]);
////Posts End////
///////////////////////regex////////////////////////
Route::get('/work/regex',[
    'uses'=>'work\RegexController@index',
    'as'=>'work.regex'
]);

Route::post('/work/settings/run_regex',[
          'uses'=>'work\RegexController@run_regex',
          'as'=>'work.settings.run_regex'
]);
///////////////////////regex////////////////////////
/////////////////Slider//////////////////
Route::get('/work/get_slides_data',[
                'uses'=>'work\SliderController@get_slides_data',
                'as'=>'work.get_slides_data'
]);
Route::get('/work/slide/create',[
          'uses'=>'work\SliderController@create',
          'as'=>'work.slide.create'
      ]);
Route::post('/work/slide/store',[
          'uses'=>'work\SliderController@store',
          'as'=>'work.slide.store'
      ]);
Route::get('/work/slides',[
          'uses'=>'work\SliderController@index',
          'as'=>'work.slides'
      ]);
Route::get('/work/slide/delete/{id}',[
          'uses'=>'work\SliderController@destroy',
          'as'=>'work.slide.delete'
      ]);
////////////////slider///////////////////
/////////////////Settings////////////////
Route::get('/work/settings',[
          'uses'=>'work\SettingsController@index',
          'as'=>'work.settings'
      ]);
Route::get('work/generate/sitemap',[
            'uses'=>'work\SettingsController@sitemap',
            'as'=>'work.sitemap'
      ]);
Route::post('work/settings/update/',[
                  'uses'=>'work\SettingsController@update',
                  'as'=>'work.settings.update'
      ]);
/////////////////Settings////////////////


////////////////////Admin End //////////////////////
/*
====================================================
*/
////////////////////Admin End //////////////////////


/*//////////////////////////////////////////////////



////////////////////User Start //////////////////////
/*
====================================================
*/
////////////////////User Start //////////////////////
//root page of Merchant = Account Dashboard
Route::get('/account', 'account\AccountController@index');
Route::get('/account/logout',[
          'uses'=>'Auth\LoginController@userLogout',
          'as'=>'accout.logout'
      ]);
Route::get('/account/user/edit/',[
          'uses'=>'account\AccountController@edit',
          'as'=>'account.user.edit'
        ]);
Route::post('/account/user/update/',[
            'uses'=>'account\AccountController@update',
            'as'=>'account.user.update'
        ]);
Route::get('/account/profile',[
            'uses'=>'account\AccountController@profile',
            'as'=>'account.user.profile'
        ]);

///////////////////////regex////////////////////////
Route::get('/account/product/regex',[
          'uses'=>'account\RegexController@index',
          'as'=>'account.product.regex'
      ]);
Route::post('/account/product/run_regex',[
            'uses'=>'account\RegexController@run_regex',
            'as'=>'account.product.run_regex'
      ]);
///////////////////////regex////////////////////////
/////////////////// Invoices   /////////////////////
Route::get('/account/get_invoice_data',[
                'uses'=>'account\InvoicesController@get_invoice_data',
                'as'=>'account.get_invoice_data'
]);
Route::post('/account/invoice/store',[
          'uses'=>'account\InvoicesController@store',
          'as'=>'account.invoice.store'
]);
Route::get('/account/invoices/',[
    'uses'=>'account\InvoicesController@index',
    'as'=>'account.invoices'
]);
Route::get('/account/invoice/delete/{id}',[
    'uses'=>'account\InvoicesController@destroy',
    'as'=>'account.invoice.delete'
]);
Route::get('/account/invoice/create',[
          'uses'=>'account\InvoicesController@create',
          'as'=>'account.invoice.create'
      ]);
Route::get('/account/invoice/csv/{id}',[
    'uses'=>'account\InvoicesController@csv',
    'as'=>'account.invoice.csv'
]);
Route::get('/account/invoice/view/{id}',[
     'uses'=>'account\InvoicesController@view',
     'as'=>'account.invoice.view'
]);
///////// End Invoices   ////////
////////// Start Add Products  //////
Route::get('/account/get_products_data',[
                'uses'=>'account\ProductsController@get_products_data',
                'as'=>'account.get_products_data'
]);

Route::get('/account/product/create',[
          'uses'=>'account\ProductsController@create',
          'as'=>'account.product.create'
      ]);
Route::post('/account/product/store',[
          'uses'=>'account\ProductsController@store',
          'as'=>'account.product.store'
]);
      Route::get('/account/products',[
          'uses'=>'account\ProductsController@index',
          'as'=>'account.products'
      ]);
      Route::get('/account/product/edit/{id}',[
          'uses'=>'account\ProductsController@edit',
          'as'=>'account.product.edit'
      ]);
      Route::get('/account/product/delete/{id}',[
          'uses'=>'account\ProductsController@destroy',
          'as'=>'account.product.delete'
      ]);
      Route::post('/account/product/update/{id}',[
          'uses'=>'account\ProductsController@update',
          'as'=>'account.product.update'
      ]);
      Route::get('/account/product/update_product/{id}',[
          'uses'=>'account\ProductsController@update_product',
          'as'=>'account.product.update_product'
      ]);
      Route::get('/account/product/import',[
                'uses'=>'account\ProductsController@import',
                'as'=>'account.product.import'
      ]);
      Route::post('/account/product/csv_upload',[
                'uses'=>'account\ProductsController@csv_upload',
                'as'=>'account.product.csv_upload'
      ]);
      Route::post('/account/product/csv_gz_unzip',[
                'uses'=>'account\ProductsController@csv_gz_unzip',
                'as'=>'account.product.csv_gz_unzip'
      ]);
      Route::post('/account/product/csv_import',[
                'uses'=>'account\ProductsController@csv_import',
                'as'=>'account.product.csv_import'
      ]);
////////// End Add Product   ////////
//////////Reports //////////////
Route::get('/account/get_reports_data',[
                'uses'=>'account\ReportsController@get_reports_data',
                'as'=>'account.get_reports_data'
]);
Route::get('/account/reports',[
    'uses'=>'account\ReportsController@index',
    'as'=>'account.reports'
]);
Route::get('/account/report/delete/{id}',[
    'uses'=>'account\ReportsController@destroy',
    'as'=>'account.report.delete'
]);
Route::get('/account/report/delete_all',[
    'uses'=>'account\ReportsController@delete_all',
    'as'=>'account.delete_all.reports'
]);
Route::get('/account/report/csv/{id}',[
    'uses'=>'account\ReportsController@csv',
    'as'=>'account.report.csv'
]);
////////// Reports/////////////
////// Payment GateWays   //////////
Route::post('/account/gateway/stripe',[
          'uses'=>'account\gateway\StripeController@payment',
          'as'=>'account.gateway.stripe'
]);
Route::post('/account/gateway/voguepay_success',[
          'uses'=>'account\gateway\VoguePayController@success',
          'as'=>'account.gateway.voguepay_success'
]);
Route::post('/account/gateway/voguepay_fail',[
          'uses'=>'account\gateway\VoguePayController@fail',
          'as'=>'account.gateway.voguepay_fail'
]);
Route::get('/account/gateway/banks',[
          'uses'=>'account\gateway\BanksController@index',
          'as'=>'account.gateway.bank'
]);
Route::get('/account/get_banks_data',[
                'uses'=>'account\gateway\BanksController@get_banks_data',
                'as'=>'account.get_banks_data'
]);
Route::post('/account/gateway/paypal',[
          'uses'=>'account\gateway\PayPalController@payment',
          'as'=>'account.gateway.paypal'
]);
Route::get('/account/gateway/paypal_status',[
          'uses'=>'account\gateway\PayPalController@paypal_status',
          'as'=>'account.gateway.paypal_status'
]);
////// Payment GateWays   //////////





















////////////////////User End //////////////////////
/*
====================================================
*/
////////////////////Userdmin End //////////////////////
