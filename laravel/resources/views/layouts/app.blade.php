

    @include('layouts.includes.header')

        <!-- =========================
            Header Section
        ============================== -->

   <!-- =========================
            Content Section
    ============================== -->
                @yield('content')
    <!-- =========================
            Footer Section
    ============================== -->
    @include('layouts.includes.footer')
