@extends('layouts.app')
@section('title', ''.$product->name.'')
@section('description', ''.$product->name.' | '.$settings->site_name.'')
@section('content')

    <!-- =========================
        Product Details Section
    ============================== -->
    <section class="product-details">
    	<div class="container">
    		<div class="row">
				<div class="col-12 p0">
					<div class="page-location">
						<ul>
							<li><a href="{{ url('/products') }}">
								Home / Product <span class="divider">/</span>
							</a></li>
							<li><a class="page-location-active" href="{{route('product_page', ['slug'=>$product->slug,'id'=>$product->id])}}">
								{{$product->name}}
								<span class="divider">/</span>
							</a></li>
						</ul>
					</div>
				</div>
	    		<div class="col-12 product-details-section">
				    <!-- ====================================
				        Product Details Gallery Section
				    ========================================= -->
					<div class="row">
						<div class="product-gallery col-12 col-md-12 col-lg-6">
						    <!-- ====================================
						        Single Product Gallery Section
						    ========================================= -->
						    <div class="row">
								<div class="col-md-12 product-slier-details">
								    <ul id="lightSlider">
								        <li data-thumb="{{asset($product->image)}}">
								            <img class="figure-img img-fluid" src="{{asset($product->image)}}" alt="{{$product->name}}">
								        </li>
								    </ul>
								</div>
							</div>
						</div>
						<div class="col-6 col-12 col-md-12 col-lg-6">
							<div class="product-details-gallery">
								<div class="list-group">
									<h4 class="list-group-item-heading product-title">
										{{$product->name}}
									</h4>
								</div>
								<div class="list-group content-list">
									<p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Price : <span style="color:red">{!!$product->user->currency->symbol!!}{{number_format($product->amount,0)}}</span></p>
									<p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Views : {{$product->views_count}}</p>
                  <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Clicks : {{$product->click_count}}</p>
                  <p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Merchant : <span style="color:red">{!!$product->user->name!!}</span></p>
								</div>
							</div>
							<?php //dd($product); ?>
							<div class="product-store row">

								<div class="col-12 product-store-box">
									<div class="row">
										<div class="col-5 ">
											<img src="{{asset($product->user->image)}}" width="150" height="50" class="figure-img img-fluid" alt="Product Img">
										</div>
										
										<div class="col-3 ">
											<a rel="external nofollow" href="{{$product->affiliate_url}}" target="_blank" class="btn btn-primary pull-right">
												Go To Store
											</a>
                    					</div>

										<!-- <div class="col-3 ">
											<a rel="external nofollow" href="{{route('link', ['id'=>$product->id])}}" target="_blank" class="btn btn-primary wd-shop-btn pull-right">
												{{$settings->buy_button}}
											</a>
                    					</div> -->
									</div>
								</div>

							</div>

						</div>
					</div>
	    		</div>
    		</div>
			<div class="row">
				<div class="col-12">
				<div class="wd-tab-section">
					<div class="bd-example bd-example-tabs">
					  <ul class="nav nav-pills mb-3 wd-tab-menu" id="pills-tab" role="tablist">
					    <li class="nav-item col-6 col-md">
					      <a class="nav-link active" id="description-tab" data-toggle="pill" href="#description-section" role="tab" aria-controls="description-section" aria-expanded="true">Description</a>
					    </li>
					    <li class="nav-item col-6 col-md">
					      <a class="nav-link" id="full-specifiction-tab" data-toggle="pill" href="#full-specifiction" role="tab" aria-controls="full-specifiction" aria-expanded="false">More Info</a>
					    </li>
					  </ul>
						<div class="tab-content" id="pills-tabContent">
							<div class="tab-pane fade active show " id="description-section" role="tabpanel" aria-labelledby="description-tab" aria-expanded="true">
								<div class="product-tab-content">
									<h4 class="description-title">{{$product->name}} Details</h4>
									{!!$product->description!!}
								</div>
								<hr>
							</div>
							<div class="tab-pane fade" id="full-specifiction">
								<h6>Additional Imformation</h6>
								<ul class="list-group wd-info-section">
									<li class="list-group-item d-flex justify-content-between align-items-center p0">
										<div class="col-12 col-md-6 info-section">
											<p>Imported : {{$product->created_at->toFormattedDateString()}}</p>
										</div>
										<div class="col-12 col-md-5 info-section">
											<p>Updated : {{$product->updated_at->toFormattedDateString()}}</p>
										</div>
										<div class="col-1"></div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				</div>
			</div>
      <section class="comments">
                    <div class="comments">
                              <div class="heading text-center">
                                    <h4 class="h1 heading-title">Reviews</h4>
                                    <div class="heading-line">
                                    <span class="short-line"></span>
                                    <span class="long-line"></span>
                                    </div>
                              </div>


                              @include('layouts.includes.disqus')
                        </div>
      </section>
      <!-- =========================
          Compare Section
      ============================== -->

      @if(count($compared_products) > 0 )
      <section class="product-details wishlist-table">
        <div class="text-center"><h6><i class="fa fa-balance-scale"></i> Similar  Porducts</h6></div>
      	<div class="container">
  			<div class="row compare-product">
  				<div class="col-12 p0">
  			        <div id="no-more-tables">
  			            <table class="col-md-12 p0 table table-responsive">
  						  <thead>
  						    <tr class="wishlist-table-title">
  						      <th class="text-center">Image</th>
  						      <th class="text-center">Name</th>
  						      <th class="text-center">Merchant</th>
  						      <th class="text-center">Price</th>
  						      <th class="text-center">Information</th>
  						      <th class="text-center">Visit</th>
  						    </tr>
  						  </thead>
  			        		<tbody>
                      @foreach($compared_products as $compared_product)

                      <?php
                      //random button Colors
                      $random_btn = array('red-bg','Blue','orange-bg','blue-bg','green-bg');
                        $key = array_rand($random_btn);
                        //show percentage
                        similar_text($product->name, $compared_product->name, $perc);
                        ?>
  			        			<tr>
  			        				<td class="text-center">
  			        					<img src="{{asset($compared_product->image)}}" class="figure-img img-fluid" width="50" height="50" alt="product-img">
  			        				</td>
  			        				<td class="text-center">
  			        					<div class="vertical-center">
  			        						<p>{{$compared_product->name }} ({{round($perc,2)}}%)</p>
  				        				</div>
  			        				</td>
  			        				<td class="text-center">
  			        					<div class="vertical-center">
  			        						<span style="color:red">{{$compared_product->user->name}}</span>
  										</div>
  			        				</td>
  			        				<td class="text-center">
  			        					<div class="vertical-center">
  			        						<div class="wishlist-price">
  			        							<p>{!!$compared_product->user->currency->symbol!!}{{number_format($compared_product->amount,0)}}</p>
  			        						</div>
  			        					</div>
  			        				</td>
  			        				<td class="text-center">
  			        					<div class="vertical-center">
  			        						<a href="{{route('product_page', ['slug'=>$compared_product->slug,'id'=>$compared_product->id])}}"  class="btn btn-warning">Info</a>
  			        					</div>
  			        				</td>
  			        				<td class="text-center">
  			        					<div class="vertical-center">
  											<a rel="external nofollow" href="{{$compared_product->affiliate_url}}" target="_blank" class="btn btn-primary wd-shop-btn {{$random_btn[$key]}}">
  											   <!--  {{$settings->buy_button}} -->
  											   Go To Store </i>
  											</a>
  										</div>
  			        				</td>
  			        			</tr>
                      @endforeach

  			        		</tbody>
  			        	</table>
  			        </div>
  				</div>
  			</div>

      	</div>
      </section>
      @endif

       <!-- =========================
          Compare Section
      ============================== -->

			<div class="row related-product">
				<h4 class="related-product-title">Other Cool Porducts</h4>
				<div id="related-product" class="owl-carousel owl-theme">
            @foreach($rand_products as $rand_product)
	    			<div class="col-12">
						<figure class="figure product-box">
							<div class="product-box-img">
								<a href="{{route('product_page', ['slug'=>$rand_product->slug,'id'=>$rand_product->id])}}">
									<img src="{{asset($rand_product->image)}}" class="figure-img img-fluid" alt="{{$rand_product->name}}">
								</a>
							</div>
							<div class="quick-view-btn">
								<div class="compare-btn">
								</div>
							</div>
							<figcaption class="figure-caption text-center">
								<div class="price-start">
									<p><strong class="active-color"><u>{!!$rand_product->user->currency->symbol!!}{{number_format($rand_product->amount,0)}}</u></strong></p>
								</div>
								<div class="content-excerpt">
									<a href="{{route('product_page', ['slug'=>$rand_product->slug,'id'=>$rand_product->id])}}">
									<p>{{$rand_product->name}}</p>
								</a>
								</div>
                <!-- <div class="custom_btn text-center">
                  <a rel="external nofollow" href="{{route('link', ['id'=>$rand_product->id])}}" target="_blank" class="btn btn-primary ">{{$settings->buy_button}}</a> &nbsp
                  <a href="{{route('product_page', ['slug'=>$rand_product->slug,'id'=>$rand_product->id])}}"  class="btn btn-light">Info</a>
                </div> -->
							</figcaption>
						</figure>
	    			</div>
            @endforeach

				</div>
			</div>
    	</div>
    </section>




@endsection
