@extends('account.layouts.app')

@section('content')

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Merchant: {{$user->name}}
                                <small></small>
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="" class=" waves-effect waves-block">Refresh</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                          @if (count($errors)>0)
                            <ul class="list-group">
                              @foreach($errors->all() as $error)
                                <li class="list-group-item text-danger">
                                  {{$error}}
                                </li>
                              @endforeach

                            </ul>
                          @endif
                          <form action="{{route('account.user.update')}}" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="category">Currency</label>
                                <select id="currency_id" name="currency_id" class="form-control show-tick" >
                                  @foreach($currencies as $currency)
                                      <option value="{{$currency->id}}"
                                        @if($user->currency_id == $currency->id)
                                            selected
                                        @endif
                                        >{{$currency->name}} ({{$currency->symbol}})</option>
                                    @endforeach
                              </select>
                            </div>
                              <div class="form-group" >
                                  <img src="{{asset($user->image)}}" alt="{{$user->name}}" width="90px" height="90px" />
                              </div>
                              <div class="form-group form-float">
                                  <input id="image" class="form-control" name="image"  type="file">
                                      <label class="form-label">Featured image</label>
                              </div>
                              <div class="form-group form-float">
                                <span style="color:orange">**Leave Blank if you dont want to change **</span>
                                  <div class="form-line">
                                      <input id="password" class="form-control" name="password" type="password">
                                      <label class="form-label">Password</label>
                                  </div>
                              </div>
                              <div class="form-group form-float">
                                  <div class="form-line">
                                      <input id="contact_name" class="form-control" name="contact_name" value="{{$user->contact_name}}" required type="text">
                                      <label class="form-label">Contact Name</label>
                                  </div>
                              </div>
                              <div class="form-group form-float">
                                  <div class="form-line">
                                      <input id="url" class="form-control" name="url" value="{{$user->url}}" required type="text">
                                      <label class="form-label">Website</label>
                                  </div>
                              </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input id="phone_number" class="form-control" name="phone_number" value="{{$user->phone_number}}" type="text">
                                        <label class="form-label">Phone Number</label>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <label class="form-label">Price Update Element</label>
                                    <div class="form-line">
                                      <input id="price_update_element" class="form-control" name="price_update_element" value="{{$user->price_update_element}}" type="text">
                                      <input id="price_update_block" class="form-control" name="price_update_block" value="0" type="hidden">
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                  <label class="form-label">Description Update Element</label>
                                    <div class="form-line">
                                      <input id="description_update_element" class="form-control" name="description_update_element" value="{{$user->description_update_element}}" type="text">
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                  <label class="form-label">Shop Address</label>
                                    <div class="form-line">
                                        <textarea rows="3" class="form-control no-resize" id="summernote" name="address" placeholder="Input Shop Address..." >
                                            {{$user->address}}
                                        </textarea>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                  <label class="form-label">About</label>
                                    <div class="form-line">
                                        <textarea rows="4"  class="form-control no-resize" id="summernote1" name="about" placeholder="Input About..." >
                                          {{$user->about}}
                                        </textarea>
                                    </div>
                                </div>
                                        <input id="active" class="form-control" name="active" readonly value="{{$user->active}}" min="0" max="1" hidden required type="hidden">
                                        <input id="name" class="form-control" readonly name="name" value="{{$user->name}}"  required type="hidden">
                                        <input id="email_address" readonly class="form-control" required name="email" value="{{$user->email}}" type="hidden">
                                <br>
                                <button  type="submit" class="btn btn-primary m-t-15 waves-effect"><i class="material-icons">save</i>Update</button>
                            </form>
                        </div>
                    </div>
                </div>

@endsection
@section('mainjs_script')
<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
<script>
$(document).ready(function() {
  $('#summernote').summernote({
    height:150
  });

});
$(document).ready(function() {
  $('#summernote1').summernote({
    height:150
  });

});
</script>


@endsection
