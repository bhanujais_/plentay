@extends('account.layouts.app')

@section('content')

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                          <a href="{{route('account.user.edit')}}">
                          <button type="button" class="btn bg-orange btn-circle-lg waves-effect waves-circle waves-float" title="Edit">
                                    <i class="material-icons">mode_edit</i>
                          </button>
                          </a>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="" class=" waves-effect waves-block">Refresh</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                      <!-- body start -->


<div class="panel panel-default">
<div class="panel-heading">
   <h4 >Merchant: Profile</h4>
</div>
<div class="panel-body">
   <div class="box box-info">
      <div class="box-body">
         <div class="col-sm-6">
            <div  align="center">
               <img alt="User Pic" src="{{asset($user->image)}}" alt="{{$user->name}}" id="profile-image1" class="img-circle img-responsive">
               <div style="color:#999;" >profile image</div>
               <!--Upload Image Js And Css-->
            </div>
            <br>
            <!-- /input-group -->
         </div>
         <div class="col-sm-6">
            <h4 style="color:#00b1b1;">{!!$user->name!!} </h4>
            </span>
         </div>
         <div class="clearfix"></div>
         <hr style="margin:5px 0 5px 0;">
         <div class="col-sm-5 col-xs-6 tital " >Contact:</div>
         <div class="col-sm-7 col-xs-6 ">{!!$user->contact_name!!}</div>
         <div class="clearfix"></div>
         <div class="bot-border"></div>
         <div class="col-sm-5 col-xs-6 tital " >Email:</div>
         <div class="col-sm-7"> {!!$user->email!!}</div>
         <div class="clearfix"></div>
         <div class="bot-border"></div>
         <div class="col-sm-5 col-xs-6 tital " >Currency:</div>
         <div class="col-sm-7"> {!!$user->currency->name!!} ({!!$user->currency->symbol!!})</div>
         <div class="clearfix"></div>
         <div class="bot-border"></div>
         <div class="col-sm-5 col-xs-6 tital " >Url:</div>
         <div class="col-sm-7"> {!!$user->url!!}</div>
         <div class="clearfix"></div>
         <div class="bot-border"></div>
         <div class="col-sm-5 col-xs-6 tital " >Address:</div>
         <div class="col-sm-7">{!!$user->address!!}</div>
         <div class="clearfix"></div>
         <div class="bot-border"></div>
         <div class="col-sm-5 col-xs-6 tital " >About:</div>
         <div class="col-sm-7">{!!$user->about!!}</div>
         <div class="clearfix"></div>
         <div class="bot-border"></div>
         <div class="col-sm-5 col-xs-6 tital " >Phone:</div>
         <div class="col-sm-7">{!!$user->phone_number!!}</div>
         <div class="clearfix"></div>
         <div class="bot-border"></div>
         <div class="col-sm-5 col-xs-6 tital " >Credits:</div>
         <div class="col-sm-7">{!!$user->credit!!}</div>
         <div class="clearfix"></div>
         <div class="bot-border"></div>
         <div class="col-sm-5 col-xs-6 tital " >Products:</div>
         <div class="col-sm-7">{!!$user_pro_count!!}</div>
         <div class="clearfix"></div>
         <div class="bot-border"></div>
         <div class="col-sm-5 col-xs-6 tital " >Active:</div>
         <div class="col-sm-7">@if ($user->active ==1 ) Yes @else No @endif</div>
         <div class="clearfix"></div>
         <div class="bot-border"></div>
         <div class="col-sm-5 col-xs-6 tital " >Signup Date:</div>
         <div class="col-sm-7">{{$user->created_at}}</div>
         <div class="clearfix"></div>
         <div class="bot-border"></div>
         <div class="col-sm-5 col-xs-6 tital " ><strong><u>Regex</u></strong></div>
         <div class="col-sm-7"><strong><u>Settings</u></strong></div>
         <div class="clearfix"></div>
         <div class="bot-border"></div>
         <div class="col-sm-5 col-xs-6 tital " >Price Element:</div>
         <div class="col-sm-7">{!!$user->price_update_element!!}</div>
         <div class="clearfix"></div>
         <div class="bot-border"></div>
         <div class="col-sm-5 col-xs-6 tital " >Description Element:</div>
         <div class="col-sm-7">{!!$user->description_update_element!!}</div>
         <!-- /.box-body -->
      </div>
      <!-- /.box -->
   </div>
</div>
</div>
<style>
input.hidden {
position: absolute;
left: -9999px;
}

#profile-image1 {
cursor: pointer;

width: 100px;
height: 100px;
border:2px solid #03b1ce ;}
.tital{ font-size:16px; font-weight:500;}
.bot-border{ border-bottom:1px #f8f8f8 solid;  margin:5px 0  5px 0}
</style>



                      <!-- body end -->
                    </div>
                </div>

@endsection
@section('mainjs_script')
<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
@endsection
