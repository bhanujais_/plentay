@extends('account.layouts.app')

@section('content')

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Generate Invoice
                                <small></small>
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="" class=" waves-effect waves-block">Refresh</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                          @if (count($errors)>0)
                            <ul class="list-group">
                              @foreach($errors->all() as $error)
                                <li class="list-group-item text-danger">
                                  {{$error}}
                                </li>
                              @endforeach

                            </ul>
                          @endif
                            <form action="{{route('account.invoice.store')}}" method="post" enctype="multipart/form-data">
                              {{csrf_field()}}
                              <div class="form-group form-float">
                                  <label class="form-label"><u>Package Name:</u></label> {{$credit->package_name}}
                                  <input  class="form-control" required name="package_name" value="{{$credit->package_name}}" type="hidden" >
                              </div><hr />
                              <div class="form-group form-float">
                                <label class="form-label"><u>Description:</u></label> {!!$credit->description!!}
                                <input  class="form-control" required name="description" value="{{$credit->description}}" type="hidden" >
                              </div></hr>
                              <div class="form-group form-float">
                                <label class="form-label"><u>CPC Rate:</u></label> {{$settings->currency_symbol}}{{$credit->rate_per_click}} Per Click
                                <input  class="form-control" required name="rate_per_click" value="{{$credit->rate_per_click}}" type="hidden" >
                                </div></hr>

                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input id="amount" class="form-control" required name="amount" type="text" >
                                        <label class="form-label">Amount</label>
                                    </div>
                                </div>
                                <input  class="form-control" required name="user_id" value="{{$user->id}}" type="hidden" >
                                <br>
                                <button  type="submit" class="btn btn-primary m-t-15 waves-effect">Create</button>
                            </form>
                        </div>
                    </div>
                </div>

@endsection
@section('mainjs_script')
<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
@endsection
