@extends('account.layouts.app')

@section('content')
<!-- for pinting puroses -->
<div id="div1">
<!-- for pinting puroses -->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                          <a href="" onclick="printContent('div1')">
                          <button type="button" class="btn bg-blue btn-circle-lg waves-effect waves-circle waves-float" title="PDF">
                                    <i class="material-icons">print</i>
                          </button>

                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="" class=" waves-effect waves-block">Refresh</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                      <!-- body start -->


<div class="panel panel-default">
<div class="panel-heading">
</div>


<div class="container">

<p><img img src="{{asset($settings->logo)}}" alt="{{$settings->site_name}}" width="100" height="50" align="right"></p>

<div class="row">
    <div class="col-xs-12">
        <div class="text-center"><h1> {{$settings->site_name}} Invoice</h1>
            <i class="fa fa-search-plus pull-left icon"></i>
            <h3>{{$invoice->invoice_number}}</h3>
        </div>
        <hr>
        <div class="row">
            <div class="col-xs-12 col-md-4 col-lg-4 pull-left">
                <div class="panel panel-default height">
                    <div class="panel-heading">Billing Details</div>
                    <div class="panel-body">
                        <strong>Merchant:</strong> {{$invoice_user->name}}<br>
                        <strong>Mail:</strong> {{$invoice_user->email}}<br>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-4 col-lg-4">
                <div class="panel panel-default height">
                    <div class="panel-heading">Payment Information</div>
                    <div class="panel-body">
                        <strong>Payment Status:</strong> @if ($invoice->status == 1) Paid @else Unpaid @endif <br>
                        <strong>Method:</strong> {{$invoice->payment_method}}<br>
                        <strong>Date:</strong> {{$invoice->created_at}}<br>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-4 col-lg-4 pull-right">
                <div class="panel panel-default height">
                    <div class="panel-heading">Package Info</div>
                    <div class="panel-body">
                        <strong>Package Name:</strong> {{$invoice->package_name}} <br>
                        <strong>Package Default Rate:</strong> {{$invoice->rate_per_click}}<br>
                        <strong>Cost:</strong><strong> {!!$settings->currency_symbol!!}{{number_format($invoice->amount,2)}}</strong> <br>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="text-center"><strong>Order summary</strong></h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <td><strong>PaymentName</strong></td>
                                <td class="text-center"><strong>Description</strong></td>
                                <td class="text-center"><strong>Cost</strong></td>
                                <td class="text-right"><strong>Total</strong></td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{$invoice->package_name}} </td>
                                <td class="text-center"><p>{{$invoice->description}}</p> </td>
                                <td class="text-center">{!!$settings->currency_symbol!!}{{number_format($invoice->amount,2)}}</td>
                                <td class="text-right">{!!$settings->currency_symbol!!}{{number_format($invoice->amount,2)}}</td>
                            </tr>


                            <tr>
                                <td class="highrow"></td>
                                <td class="highrow"></td>
                                <td class="highrow text-center"><strong>Activation</strong></td>
                                <td class="highrow text-right"> FREE</td>
                            </tr>

                            <tr>
                                <td class="emptyrow"><i class="fa fa-barcode iconbig"></i></td>
                                <td class="emptyrow"></td>
                                <td class="emptyrow text-center"><strong>Total</strong></td>
                                <td class="emptyrow text-right"><strong> {!!$settings->currency_symbol!!}{{number_format($invoice->amount,2)}}</strong></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@if ($invoice->status == 0 )
<div class="row">
  @if($gateway->stripe_active == 1)
    <div class="col-xs-12 col-md-3 col-lg-3 pull-left">
      <div class="text-center">
      <form action="{{route('account.gateway.stripe')}}" method="POST">
        {{csrf_field()}}
      <script
        src="https://checkout.stripe.com/checkout.js" class="stripe-button"
        data-key="{{$gateway->stripe_publishable_key}}"
        data-amount="{{(int) ( ( (string) ( $invoice->amount * 100 ) ) )}}"
        data-name="{{$settings->site_name}}"
        data-description="Credit Payment"
        data-email="{{$user->email}}"
        data-currency="{{$settings->currency_code}}"
        data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
        data-locale="auto">
      </script>


      <script>
          // Hide default stripe button, be careful there if you
          // have more than 1 button of that class
          document.getElementsByClassName("stripe-button-el")[0].style.display = "none";
      </script>
      <button type="submit" class="btn bg-cyan waves-effect "><i class="fab fa-cc-stripe"></i> Stripe</button>
      <input type="hidden" name="org_invoice_id" value="{{$invoice->id}}">
      <input type="hidden" name="org_invoice_no" value="{{$invoice->invoice_number}}">
      <input type="hidden" name="org_desc" value="Credit Payment">
      <input type="hidden" name="org_amount" value="{{$invoice->amount}}">
      </form>
    </div>
    </div>
    @endif

    @if($gateway->paypal_active == 1)
    <!-- paypal -->
    <div class="col-xs-12 col-md-3 col-lg-3">
      <div class="text-center">
      <form action="{{route('account.gateway.paypal')}}" method="post" name="frmPayPal1">
        {{csrf_field()}}
          <input type="hidden" name="cmd" value="_xclick">
          <input type="hidden" name="item_name" value="Credit Payment {{$invoice->invoice_number}}"><!--inv name -->
          <input type="hidden" name="item_number" value="{{$invoice->id}}">
          <input type="hidden" name="amount" value="{{$invoice->amount}}">
          <input type="hidden" name="no_shipping" value="0">
          <input type="hidden" name="currency_code" value="{{$settings->currency_code}}">
          <input type="hidden" name="handling" value="0">
          <input type="hidden" name="cancel_return" value="">
          <input type="hidden" name="return" value=""><button type="submit" class="btn bg-primary waves-effect" onclick="return confirm('Pay Via PayPal ?');"><i class="fab fa-paypal"></i> PayPal</button>
          <img alt="" border="0" src="https://www.sandbox.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
      </form>
    </div>
  </div>
  @endif
  @if($gateway->voguepay_active == 1)
    <!-- VoguePay -->
    <div class="col-xs-12 col-md-3 col-lg-3">
      <div class="text-center">
      <form action="https://voguepay.com/pay/" method="post">
        <input type="hidden" name="total" readonly value="{{$invoice->amount}}" />
        <input type="hidden" name="store_id" value="{{$invoice->invoice_number}}" />
        <input type="hidden" name="v_merchant_id"  value="{{$gateway->voguepay_merchant_id}}" />
        <input type="hidden" name="memo"  value="Credit Payment" />
        <input type="hidden" name="developer_code" value="5a87c27f2d48a" />
        <input type="hidden" name="cur" value="{{$settings->currency_code}}" />
        <input type="hidden" name="merchant_ref" value="{{$invoice->id}}" />
        <input type="hidden" name="success_url" value="{{route('account.gateway.voguepay_success')}}" />
        <input type="hidden" name="fail_url" value="{{route('account.gateway.voguepay_fail')}}" />
        <button type="submit" name="submit" class="btn bg-indigo waves-effect" onclick="return confirm('Pay Via VoguePay ?');"><i class="fa fa-check"></i> VoguePay</button>
      </form>
    </div>
</div>
@endif
@if($gateway->bankwire_active == 1)
<!-- VoguePay -->
<div class="col-xs-12 col-md-3 col-lg-3 pull-right">
  <div class="text-center">
    <a href="{{route('account.gateway.bank')}}" class="btn bg-green waves-effect" ><i class="fa fa-university"></i> Bank</a>
  </div>
</div>
@endif
</div>

<br />
@else

@endif

</div>



</div>


</div>

<style>
.table {
	width: 100%;
}
.container{
  width: auto;
}
.height {
    min-height: 200px;
}

.icon {
    font-size: 47px;
    color: #5CB85C;
}

.iconbig {
    font-size: 77px;
    color: #5CB85C;
}

.table > tbody > tr > .emptyrow {
    border-top: none;
}

.table > thead > tr > .emptyrow {
    border-bottom: none;
}

.table > tbody > tr > .highrow {
    border-top: 3px solid;
}
</style>




                      <!-- body end -->
                    </div>
                </div>

@endsection
@section('mainjs_script')
<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
<script>
function printContent(el){
	var restorepage = document.body.innerHTML;
	var printcontent = document.getElementById(el).innerHTML;
	document.body.innerHTML = printcontent;
	window.print();
	document.body.innerHTML = restorepage;
}
</script>
<script src="//voguepay.com/js/voguepay.js"></script>
@endsection
