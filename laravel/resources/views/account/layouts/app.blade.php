﻿@include('account.layouts.includes.header')
    <!-- #Top Bar -->

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD, {{ date("l jS \of F Y h:i:s A") }}</h2>
            </div>
            <!-- Widgets -->
            @yield('content')


        </div>
    </section>
    @yield('mainjs_script')
@include('account.layouts.includes.footer')
