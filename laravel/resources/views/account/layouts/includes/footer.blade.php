<!-- Jquery Core Js -->
<!-- <script src="{{asset('app/plugins/jquery/jquery.min.js')}}"></script> -->

<!-- Bootstrap Core Js -->
<script src="{{asset('app/plugins/bootstrap/js/bootstrap.js')}}"></script>
<!-- Latest compiled and minified CSS -->
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->

<!-- Select Plugin Js -->
<script src="{{asset('app/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>

<!-- Slimscroll Plugin Js -->
<script src="{{asset('app/plugins/jquery-slimscroll/jquery.slimscroll.js')}}"></script>

<!-- Waves Effect Plugin Js -->
<script src="{{asset('app/plugins/node-waves/waves.js')}}"></script>

<!-- Jquery CountTo Plugin Js -->
<script src="{{asset('app/plugins/jquery-countto/jquery.countTo.js')}}"></script>

<!-- Morris Plugin Js -->
<script src="{{asset('app/plugins/raphael/raphael.min.js')}}"></script>
<!-- <script src="{{asset('app/plugins/morrisjs/morris.js')}}"></script> -->

<!-- ChartJs -->
<!-- <script src="{{asset('app/plugins/chartjs/Chart.bundle.js')}}"></script> -->

<!-- Flot Charts Plugin Js -->
<!-- <script src="{{asset('app/plugins/flot-charts/jquery.flot.js')}}"></script>
<script src="{{asset('app/plugins/flot-charts/jquery.flot.resize.js')}}"></script>
<script src="{{asset('app/plugins/flot-charts/jquery.flot.pie.js')}}"></script>
<script src="{{asset('app/plugins/flot-charts/jquery.flot.categories.js')}}"></script>
<script src="{{asset('app/plugins/flot-charts/jquery.flot.time.js')}}"></script> -->

<!-- Sparkline Chart Plugin Js -->
<script src="{{asset('app/plugins/jquery-sparkline/jquery.sparkline.js')}}"></script>

<!-- Custom Js -->
<script src="{{asset('app/js/admin.js')}}"></script>
<script src="{{asset('app/js/pages/index.js')}}"></script>
<!-- <script src="{{asset('app/js/pages/ui/notifications.js')}}"></script> -->

<!-- Demo Js -->
<!-- <script src="{{asset('app/js/demo.js')}}"></script> -->


<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>

<script>
  @if(Session::has('success'))
    toastr.success("{{Session::get('success')}}")
  @endif
  @if(Session::has('info'))
    toastr.info("{{Session::get('info')}}")
  @endif
  @if(Session::has('error'))
    toastr.error("{{Session::get('error')}}")
  @endif
  @if(Session::has('warning'))
    toastr.warning("{{Session::get('warning')}}")
  @endif
</script>
<script>
// $.noConflict();
// jQuery(document).ready(function(){
//     jQuery("button").click(function(){
//         jQuery("p").text("jQuery is still working!");
//     });
// });
</script>
<script type="text/javascript" language="javascript">
    // var jq = jQuery.noConflict();
</script>
</body>

</html>
