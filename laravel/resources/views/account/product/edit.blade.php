@extends('account.layouts.app')

@section('content')

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Product: {{$product->name}}
                                <small></small>
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="" class=" waves-effect waves-block">Refresh</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                          @if (count($errors)>0)
                            <ul class="list-group">
                              @foreach($errors->all() as $error)
                                <li class="list-group-item text-danger">
                                  {{$error}}
                                </li>
                              @endforeach

                            </ul>
                          @endif
                            <form action="{{route('account.product.update',['id'=>$product->id])}}" method="post" enctype="multipart/form-data">
                              {{csrf_field()}}
                              <div class="form-group form-float">
                                  <div class="form-line">
                                      <input id="name" class="form-control" name="name" value="{{$product->name}}"  required type="text">
                                      <label class="form-label">Product Name</label>
                                  </div>
                              </div>

                              <label for="category">Select a Category</label>
                              {!!$categories!!}
                              <div class="form-group form-float">
                                  <label class="form-label">Featured image</label><span style="color:orange">**Leave Blank if you dont want to change **</span>
                                  <input id="image" class="form-control" name="image"  type="file">
                              </div>
                              <div class="form-group" >
                                  <img src="{{asset($product_image)}}" alt="{{$product->name}}" width="100px" height="50px" />
                              </div>
                              <div class="form-group form-float">
                                  <div class="form-line">
                                      <input id="original_url" class="form-control" value="{{$product->original_url}}" name="original_url" title="For Update Purposes"  required type="text">
                                      <label class="form-label">Original Link</label>
                                  </div>
                              </div>
                              <div class="form-group form-float">
                                  <div class="form-line">
                                      <input id="affiliate_url" class="form-control" name="affiliate_url" value="{{$product->affiliate_url}}"  required type="text">
                                      <label class="form-label">Affiliate Link</label>
                                  </div>
                              </div>


                                <input id="active" class="form-control" name="active" value="1" type="hidden">
                                <div class="form-group form-float">
                                  <label class="form-label">Description</label>
                                    <div class="form-line">
                                        <textarea rows="5" id="summernote"class="form-control no-resize" name="description" placeholder="Input Shop Address..." >{!!$product->description!!}</textarea>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input id="amount" class="form-control" required value="{{$product->amount}}" name="amount" type="text">
                                        <label class="form-label">Amount</label>
                                    </div>
                                </div>
                                <input id="user_id" class="form-control" required readonly value="{{$user->id}}" name="user_id" type="hidden">


                                <br>
                                <button  type="submit" class="btn btn-primary m-t-15 waves-effect"><i class="material-icons">save</i>Update</button>
                            </form><hr />
                            <div class="panel panel-primary">
                               <div class="panel-heading" role="tab" id="headingOne_1">
                                   <h4 class="panel-title">
                                       <a role="button" data-toggle="collapse" data-parent="#accordion_1" href="#collapseOne_1" aria-expanded="false" aria-controls="collapseOne_1" class="collapsed">
                                        Info! [View] <i class="fa fa-angle-down"></i>
                                      </a>
                                   </h4>
                               </div>
                               <div id="collapseOne_1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne_1" aria-expanded="false" style="height: 0px;">
                                   <div class="panel-body">

                                     <div class="alert alert-info">
                                       <strong></strong><p>Dual Product links has been created because of the "Price Update" feature,
                                         "Original Link" should be without Affiliate Id</p>
                                         <p><u><b>Example</b></u> </p>
                                         <p>Original Link: https://www.konga.com/water-dispenser-nx-015-2567376</p>
                                         <p>Affiliate Link: https://www.konga.com/water-dispenser-nx-015-2567376?k_id=yourafflink</p>
                                         <p><u><b>Functions</b></u> </p>
                                       <p>Original Link:  Price and Description Update Purposes.</p>
                                       <p>Affiliate Link: Log Stats and Redirect Users to Merchant's Product Page.</p>
                                     </div>

                                   </div>
                               </div>
                           </div>
                        </div>
                    </div>
                </div>

@endsection
@section('mainjs_script')
<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
<script>
$(document).ready(function() {
  $('#summernote').summernote({
    height:150
  });

});
</script>
@endsection
