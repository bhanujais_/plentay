@extends('layouts.app')
@section('title', 'Merchants')
@section('description', 'Merchants | '.$settings->site_name.'')
@section('content')

        <!-- =========================
            Breadcrumb Section
        ============================== -->
        <section class="blog-slider-tow-grid wd-slider-section">
    		<div class="container">
    			<div class="row">
    				<div class="col-md-12">
    					<div class="text-center">
    						<h1 class="wishlist-slider-title">Merchants</h1>
    	                </div>
    				</div>
    			</div>
    		</div>
        </section>


       <!-- =========================
            Choose Category
        ============================== -->
        <section id="choose-category">
        	<div class="container-fluid custom-width">
        		<div class="choose-category-area-box">
    	    		<div class="row">
                <!-- <div class="col-md-12 text-center">
      						<h2 class="choose-category-main-title">Active</h2>
      					</div> -->
                @foreach($users as $user)
                  <div class="col-sm-6 col-md-4 col-lg-3 choose-category-box wow fadeIn animated" data-wow-delay="300ms">
                   <div class="d-flex justify-content-start align-items-center">
                      <div class="category-info">
                         <h6 class="choose-category-title">{{$user->name}}</h6>
                         <div class="choose-category-content">
                            <p><h4>{{count($user->products)}}</h4></p>
                         </div>
                         <a href="{{route('merchant_page', ['slug'=>$user->name])}}" class="badge badge-light choose-category-link">Product List</a>
                      </div>
                      <div class="category-img">
                         <a href="{{route('merchant_page', ['slug'=>$user->name])}}"><img class="img-fluid"  src="{{asset($user->image)}}" height="50" width="150" alt=""></a>
                      </div>
                   </div>
                   <span class="wd-border-bottom" style="background: #ff8a80"></span>
                  </div>
                  @endforeach

                  <div class="col-12 text-center">
                    <div class="row">
                			<div class="col-md-12">
            					<div class="float-right">
            						<nav class="wd-pagination">
            						  <ul class="pagination">
            						  	{{$users->links()}}
            						  </ul>
            						</nav>
            					</div>
                			</div>
                		</div>
                  </div>

    	    		</div>
        		</div>
        	</div>
        </section>
        <style>
        .wd-pagination a {
        font-size: 14px;
        color: #666666;
        font-weight: 400;
        }
        .wd-pagination a:hover {
        background: #ff9800;
        color: #fff;
        }
        .wd-pagination .page-link {
        border-color: #f5f5f5;
        }
        .wd-pagination .page-item:last-child .page-link,
        .wd-pagination .page-item:first-child .page-link {
        background: #666666;
        color: #fff;
        }
        .wd-pagination .page-item:last-child .page-link:hover,
        .wd-pagination .page-item:first-child .page-link:hover {
        background: #ff9800;
        }
        .wd-pagination .page-item.active .page-link {
        background: #ff9800;
        color: #fff;
        }
        </style>

    @endsection
