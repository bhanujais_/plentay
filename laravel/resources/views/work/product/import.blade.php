@extends('work.layouts.app')

@section('content')

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                          CSV Import Settings
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="" class=" waves-effect waves-block">Refresh</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                      <!-- body start -->


                        <div class="panel panel-default">
                        <div class="panel-heading">
                        </div>
                        <div class="panel-body">
                           <div class="box box-info">

                             <div class="col-md-12">
                            <div class="panel panel-primary">
                               <div class="panel-heading" role="tab" id="headingOne_1">
                                   <h4 class="panel-title">
                                       <a role="button" data-toggle="collapse" data-parent="#accordion_1" href="#collapseOne_1" aria-expanded="false" aria-controls="collapseOne_1" class="collapsed">
                                        View Files <i class="fa fa-angle-down"></i>
                                      </a>
                                   </h4>
                               </div>
                               <div id="collapseOne_1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne_1" aria-expanded="false" style="height: 0px;">
                                   <div class="panel-body">
                                     @foreach($files as $file)
                                        <strong>{{$file}}<br></strong>
                                     @endforeach
                                   </div>
                               </div>
                           </div>
                             <div class="container" >
                               @if (count($errors)>0)
                                 <ul class="list-group">
                                   @foreach($errors->all() as $error)
                                     <li class="list-group-item text-danger">
                                       {{$error}}
                                     </li>
                                   @endforeach

                                 </ul>
                               @endif
                               <h4>{!!Session::get('message')!!}</h4>
                              <div class="col-md-6">
                             	<form role="form" action="{{route('work.product.csv_upload')}}" method="post" enctype="multipart/form-data">
                                {{csrf_field()}}
                             		<div class="form-group">
                             		<center><input type="file" name="csv_file" id="csv_file" accept=".csv,.gz,csv.gz">
                             		 <label for="upload"  class="control-label">Only .csv or .csv.gz and .gz files are allowed. </label><br/><br/>
                                 <input type="submit" class="btn btn-primary" value="Upload" name="upload">
                                 <a href="{{ asset('uploads/imports/sample/import.csv') }}"  class="btn btn-default">Sample</a></center>
                             		</div>
                             </form>
                             </div>

                            <div class="col-md-6">
                              <form role="form" action="{{route('work.product.csv_import')}}" method="post" enctype="multipart/form-data">
                                {{csrf_field()}}
                             <div class="col-md-6">
                               <label for="category">Select Category</label>
                               {!!$categories!!}
                             </div>
                             <div class="col-md-6">
                               <div class="form-group">
                                   <label for="category">Assign Merchant</label>
                                   <select id="user_id" name="user_id" class="form-control show-tick" >
                                     @foreach($users as $user)
                                       <option value="{{$user->id}}">{{$user->name}}</option>
                                     @endforeach
                                 </select>
                               </div>
                             </div>
                             <center>
                              <input type="submit" class="btn btn-success text-center" value="Import Data" name="import">
                             </center>
                           </form>
                           </div>

                             	</div>
                             </div>



                           </div>
                        </div>
                        </div>



                      <!-- body end -->
                    </div>
                </div>
                <style>
                .container{
                  width: auto;
                }
                </style>

@endsection
@section('mainjs_script')
<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
@endsection
