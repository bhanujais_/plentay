@extends('work.layouts.app')


@section('content')

    <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
    <!-- <script src="https://cdn.datatables.net/responsive/2.2.2/js/dataTables.responsive.min.js"></script> -->
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>

    <!-- <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.2/css/responsive.dataTables.min.css" /> -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css" />


<!-- <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> -->


<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Products
                                <small>Controls</small>
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="" class=" waves-effect waves-block">Refresh</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="icon-and-text-button-demo">
                              <a href="{{ route('work.product.create') }}">
                                <button type="button" class="btn btn-primary waves-effect">
                                    <i class="material-icons">add_circle_outline</i>
                                    <span>Add Product</span>
                                </button>
                              </a>
                                <a href="">
                                <button type="button" class="btn btn-warning waves-effect">
                                    <i class="material-icons">cached</i>
                                    <span>Refresh</span>
                                </button>
                              </a>

                                <!-- <a id="" type="button" class="btn btn-warning waves-effect" href="">
                                  <i class="material-icons">cached</i> Refresh</a> -->




                            </div>
                        </div>
                    </div>
                </div>




<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="container">
              <div class="card"><br />
                        <div class="col-md-12">
                            <form class="example" action="" style="margin:auto;max-width:300px">
                             <input type="text" placeholder="Search.." name="query">
                            <button type="submit"><i class="fa fa-search"></i></button>
                            </form>
                          </div>
                        <div class="col-md-12">
                          <div class="col-md-9">
                            <h6 class="result"><b>Query:</b> {{$query}}</h6>
                          </div>
                          <div class="col-md-3">
                            <h6 class="result"><b>Total</b> {{$products->total()}} Showing  {{$products->count()}} results</h6>
                          </div>
                      </div>

                        <div class="body table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Amount</th>
                                        <th>Merchant</th>
                                        <th>Category</th>
                                        <th>Views</th>
                                        <th>Clicks</th>
                                        <th>Created</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   @foreach($products as $product)
                                   <?php
                                   $product_image_type= substr( $product->image, 0, 4 ) === "http";
                                   $product_image     = $product_image_type==1 ? $product->image : asset($product->image);
                                   $product_name      = $product->name;
                                   $product_name      = strlen($product_name) > 30 ? substr($product_name,0,30)."..." : $product_name;
                                   $product_page_link = url('/').'/'.$product->slug.'-'.$product->id;
                                   $delete_confirmation          = '\'Do You Want to Delete: '.$product->name.' ? \'';
                                   $update_confirmation          = '\'Do You Want to Update Price ? \'';

                                    ?>
                                    <tr>
                                        <td><a href="{{$product_page_link}}" target="_blank"> {{$product_name}}</a><br>
                                 	             <img src="{{$product_image}}"  alt=""  width="100" height="50" />
                                        </td>
                                        <td>{!!$product->user->currency->symbol!!}{{number_format($product->amount,2)}}</td>
                                        <td>{!!$product->user->name!!}</td>
                                        <td><b>{!!$product->category->name!!}</b></td>
                                        <td>{{$product->views_count}}</td>
                                        <td>{{$product->click_count}}</td>
                                        <td>{{\Carbon\Carbon::parse($product->created_at)->format('d-m-Y')}}</td>
                                        <?php //$reports_date = \Carbon\Carbon::parse($reports->created_at)->format('d-m-y');?>
                                        <td>
                                          <a href="{{route('work.product.update_product',$product->id)}}" class="btn btn-primary btn-xs" title="Update Price" onclick="return confirm({{$update_confirmation}});"><i class="material-icons">swap_vertical_circle</i></a>
                                          <a href="{{route('work.product.edit',$product->id)}}" class="btn btn-warning btn-xs" title="Edit"><i class="material-icons">mode_edit</i></a>
                                          <a href="{{route('work.product.delete',$product->id)}}" class="btn btn-danger btn-xs" title="Delete" onclick="return confirm({{$delete_confirmation}});"><i class="material-icons">delete</i></a>
                                        </td>
                                    </tr>
                                   @endforeach
                                </tbody>
                            </table>
                            {{$products->appends(request()->query())->links()}}
                        </div>

                    </div>


            </div>
            </div>
<style>
div.container {
        width: 100%;
    }

</style>




<hr/>
<style>
form.example input[type=text] {
    padding: 10px;
    font-size: 17px;
    border: 1px solid grey;
    float: left;
    width: 80%;
    background: #f1f1f1;
}

form.example button {
    float: left;
    width: 20%;
    padding: 10px;
    background: #2196F3;
    color: white;
    font-size: 17px;
    border: 1px solid grey;
    border-left: none;
    cursor: pointer;
}

form.example button:hover {
    background: #0b7dda;
}

form.example::after {
    content: "";
    clear: both;
    display: table;
}
</style>
@endsection
