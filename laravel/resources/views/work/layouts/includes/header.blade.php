<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Admin Control Panel</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <!-- Bootstrap Core Css -->
    <link href="{{asset('app/plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{asset('app/plugins/node-waves/waves.css')}}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{asset('app/plugins/animate-css/animate.css')}}" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <!-- <link href="{{asset('app/plugins/morrisjs/morris.css')}}" rel="stylesheet" /> -->

    <!-- Custom Css -->
    <link href="{{asset('app/css/style.css')}}" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{{asset('app/css/themes/all-themes.css')}}" rel="stylesheet" />

    <link href="{{asset('https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css')}}" rel="stylesheet">
    <!-- include summernote css/js -->
  <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css" rel="stylesheet" />
</head>

<body class="theme-black">
    <!-- Page Loader -->
    <!-- <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p><i class="material-icons">hourglass_full</i> Please wait...</p>
        </div>
    </div> -->
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <!-- <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div> -->
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="{{ url('/work') }}">Admin Control Panel</a>
                <!-- <a class="navbar-brand" title="Web" href="{{ url('/') }}"><i class="fa fa-globe" aria-hidden="true"></i>Site</a> -->
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <!-- Call Search -->
                    <!-- <li><a href="javascript:void(0);" class="js-search" data-close="true"><i class="material-icons">search</i></a></li> -->
                    <!-- #END# Call Search -->
                    <!-- Notifications -->
                    <!-- <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons">notifications</i>
                            <span class="label-count">1</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">INVOICES</li>
                            <li class="body">
                                <ul class="menu">

                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-green">
                                                <i class="material-icons">check</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4>Credit<b>7532</b> solid</h4>
                                                <p>
                                                    <i class="material-icons">access_time</i> 3 hours ago
                                                </p>
                                            </div>
                                        </a>
                                    </li>


                                </ul>
                            </li>
                            <li class="footer">
                                <a href="{{ route('work.invoices') }}">View All Invoices</a>
                            </li>
                        </ul>
                    </li> -->
                    <!-- #END# Notifications -->
                    <li class="pull-right"><a href="javascript:void(0);" class="js-right-sidebar" data-close="true"><i class="material-icons">more_vert</i></a></li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <!-- <div class="image">
                    <img src="{{asset('app/images/user.png')}}" width="48" height="48" alt="User" />
                </div> -->
                <div class="info-container">

                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Administrator</div>
                    <!-- <div class="email">john.doe@example.com</div> -->
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="{{ route('work.admin.profile') }}"><i class="material-icons">person</i>Profile</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="{{ url('/work/logout') }}"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();"><i class="material-icons">input</i>Sign Out</a></li>
                                <form id="logout-form" action="{{ url('/work/logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                </form>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">MAIN NAVIGATION</li>
                    <li class="active">
                        <a href="{{ url('/work') }}">
                            <i class="material-icons">home</i>
                            <span>Home</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">view_module</i>
                            <span>Products</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="{{ route('products') }}"><i class="material-icons">view_list</i>View Products</a>
                            </li>
                            <li>
                                <a href="{{ route('work.product.create') }}"><i class="material-icons">insert_drive_file</i> Add Product</a>
                            </li>
                            <li>
                                <a href="{{ route('categories') }}"><i class="material-icons">view_quilt</i>Categories</a>
                            </li>
                            <li>
                                <a href="{{ route('work.product.import') }}"><i class="material-icons">file_upload</i>Csv Import</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);" class="menu-toggle">
                                    <i class="fas fa-bug"></i><span>Crawler</span>
                                </a>
                                <ul class="ml-menu">
                                  <li>
                                        <a href="{{ route('work.crawl.jumia') }}"><i class="fas fa-magnet"></i><span> Jumia</span></a>
                                    </li>
                                </ul>
                            </li>

                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">group</i>
                            <span>Users</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="{{ route('companys') }}"><i class="material-icons">people</i>Company</a>
                            </li>
                            <li>
                                <a href="{{ route('users') }}"><i class="material-icons">person</i><span> Merchants</span></a>
                            </li>
                            <li>
                                <a href="{{ route('admins') }}"><i class="material-icons">verified_user</i> Admin</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                          <i class="fa fa-university fa-2x" aria-hidden="true" ></i>
                            <span>Finance</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="{{ route('work.credits') }}"><i class="material-icons">attach_money</i><span> Credit</span></a>
                            </li>
                            <li>
                                <a href="{{ route('work.invoices') }}"><i class="material-icons">receipt</i> Invoices</a>
                            </li>
                            <li>
                                <a href="{{ route('work.banks') }}"><i class="material-icons">attach_money</i> Bank</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{ route('work.reports') }} ">
                            <i class="material-icons">list</i>
                            <span>Reports</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">library_books</i>
                            <span>Blog & Pages</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="{{ route('work.posts') }}"><i class="material-icons">featured_play_list</i><span> View Blogs</span></a>
                            </li>
                            <li>
                                <a href="{{ route('work.pages') }}"><i class="material-icons">receipt</i> View Pages</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <!-- fa-spin col-red-->
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="fa fa-cog  fa-2x fa-fw " aria-hidden="true"></i>
                            <span>Settings</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="{{ route('work.slides') }}"><i class="material-icons">photo_size_select_actual</i><span> Slider</span></a>
                            </li>
                            <li>
                                <a href="{{ route('work.regex') }}"><i class="material-icons">developer_mode</i> Regex Test</a>
                            </li>
                            <li>
                                <a href="{{ route('work.settings') }}"><i class="material-icons">build</i><span> Site Settings</span></a>
                            </li>

                        </ul>
                    </li>
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <!-- <div class="copyright">
                    &copy; {{ date('Y') }} <a href="javascript:void(0);">Dashboard</a>.
                </div> -->
               <!--  <div class="version">
                    <b>Version: </b> {{$_ENV['DEV_VERSION']}}
                </div> -->
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
        <!-- Right Sidebar -->
        <aside id="rightsidebar" class="right-sidebar">
            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                <li role="presentation" class="active"><a href="#skins" data-toggle="tab">TIME</a></li>
                <li role="presentation"><a href="#settings" data-toggle="tab">SETTINGS</a></li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active in active" id="skins">
                    <ul class="demo-choose-skin">
                        <li data-theme="red" class="active">
                            <div class="red"></div>
                            <span>{{ date('Y-m-d H:i:s') }}</span>
                        </li>

                    </ul>
                </div>
                <!-- <div role="tabpanel" class="tab-pane fade" id="settings">
                    <div class="demo-settings">
                        <p>GENERAL SETTINGS</p>
                        <ul class="setting-list">
                            <li>
                                <span>Notifications</span>
                                <div class="switch">
                                    <label><input type="checkbox" checked><span class="lever"></span></label>
                                </div>
                            </li>
                        </ul>

                    </div>
                </div> -->
            </div>
        </aside>
        <!-- #END# Right Sidebar -->
    </section>
