@extends('work.layouts.app')

@section('content')

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Create Admin User
                                <small></small>
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="" class=" waves-effect waves-block">Refresh</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <form action="{{route('work.admin.store')}}" method="post">
                              {{csrf_field()}}
                              <div class="form-group form-float">
                                  <div class="form-line">
                                      <input id="name" class="form-control" name="name"  required type="text">
                                      <label class="form-label">Username</label>
                                  </div>
                              </div>
                              <div class="form-group form-float">
                                  <div class="form-line">
                                      <input id="display_name" class="form-control" name="display_name"  required type="text">
                                      <label class="form-label">Display Name</label>
                                  </div>
                              </div>

                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input id="email_address" class="form-control" required name="email" type="email">
                                        <label class="form-label">Email Address</label>
                                    </div>
                                </div>

                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input id="password" class="form-control" required name="password" type="password">
                                        <label class="form-label">Password</label>
                                    </div>
                                </div>

                                <br>
                                <button  type="submit" class="btn btn-primary m-t-15 waves-effect">Create</button>
                            </form>
                        </div>
                    </div>
                </div>

@endsection
@section('mainjs_script')
<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
@endsection
