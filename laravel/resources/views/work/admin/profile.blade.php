@extends('work.layouts.app')

@section('content')

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                          <a href="{{route('work.admin.edit',['id'=>$admin->id])}}">
                          <button type="button" class="btn bg-orange btn-circle-lg waves-effect waves-circle waves-float" title="Edit">
                                    <i class="material-icons">mode_edit</i>
                          </button>
                          </a>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="" class=" waves-effect waves-block">Refresh</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                      <!-- body start -->


<div class="panel panel-default">
<div class="panel-heading">
   <h4 >Admin: Profile</h4>
</div>
<div class="panel-body">
   <div class="box box-info">
      <div class="box-body">
         <div class="col-sm-6">
            <div  align="center">
               <!-- <img alt="admin Pic" src="{{asset($admin->image)}}" alt="{{$admin->adminname}}" id="profile-image1" class="img-circle img-responsive"> -->
               <div style="color:#999;" ></div>
               <!--Upload Image Js And Css-->
            </div>
            <br>
            <!-- /input-group -->
         </div>
         <div class="col-sm-6">
            <h4 style="color:#00b1b1;">{!!$admin->name!!} </h4>
            </span>
         </div>
         <div class="clearfix"></div>
         <hr style="margin:5px 0 5px 0;">
         <div class="col-sm-5 col-xs-6 tital " >Display Name:</div>
         <div class="col-sm-7 col-xs-6 ">{!!$admin->display_name!!}</div>
         <div class="clearfix"></div>
         <div class="bot-border"></div>
         <div class="col-sm-5 col-xs-6 tital " >Email:</div>
         <div class="col-sm-7"> {!!$admin->email!!}</div>
         <div class="clearfix"></div>
         <div class="bot-border"></div>
         <div class="col-sm-5 col-xs-6 tital " >Created:</div>
         <div class="col-sm-7"> {!!$admin->created_at!!}</div>
         <div class="clearfix"></div>
         <div class="bot-border"></div>
         <!-- /.box-body -->
      </div>
      <!-- /.box -->
   </div>
</div>
</div>
<style>
input.hidden {
position: absolute;
left: -9999px;
}

#profile-image1 {
cursor: pointer;

width: 100px;
height: 100px;
border:2px solid #03b1ce ;}
.tital{ font-size:16px; font-weight:500;}
.bot-border{ border-bottom:1px #f8f8f8 solid;  margin:5px 0  5px 0}
</style>



                      <!-- body end -->
                    </div>
                </div>

@endsection
@section('mainjs_script')
<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
@endsection
