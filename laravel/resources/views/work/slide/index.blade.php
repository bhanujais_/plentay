@extends('work.layouts.app')

@section('content')

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="" class=" waves-effect waves-block">Refresh</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                      <!-- body start -->


<div class="panel panel-default">
<div class="panel-heading">
  <a href="{{ route('work.slide.create') }}">
    <button type="button" class="btn btn-primary waves-effect">
        <i class="material-icons">add_circle_outline</i>
        <span>Add Slide</span>
    </button>
  </a>
</div>
<div class="panel-body">
   <div class="box box-info">

     <div class="row">
  <h3 ></h3>

<div class="col-xs-12">
  <center><h1>Active Slide</h1></center>
    @if(count($slides)>0)
    <div class="active">
            <img class="slide-image img-responsive" src="{{asset($last_slide->image)}}" alt="IMG" >
    </div>
    @endif
</div>
</div><!--Row-->

<hr />
<h1>Slider</h1>
<div class="row">
<div class="col-md-12">

  <div class="row">
          @if(!empty($slides))
          @foreach($slides as $slide)
            <div class="col-lg-4 text-center">
              <div class="panel panel-default">
                <div class="panel-body">
                  <img class="slide-image" src="{{asset($slide->image)}}" alt="IMG" width="220" height="140">
                </div>
                <a href="{{$slide->url}}"   class="btn btn-default ">Visit Link!</a> &nbsp
                <a class="btn btn-danger" href="{{route('work.slide.delete',$slide->id)}}" onclick="return confirm('Do You Want to Delete?');">Delete</a>
                <br/>
              </br>
            </div>
        </div>
        @endforeach
        @endif

  </div>
    <!-- /.row -->



</div>
</div><!--Row-->

   </div>
</div>
</div>
<style>
input.hidden {
position: absolute;
left: -9999px;
}

#profile-image1 {
cursor: pointer;

width: 100px;
height: 100px;
border:2px solid #03b1ce ;}
.tital{ font-size:16px; font-weight:500;}
.bot-border{ border-bottom:1px #f8f8f8 solid;  margin:5px 0  5px 0}
</style>



                      <!-- body end -->
                    </div>
                </div>

@endsection
@section('mainjs_script')
<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
@endsection
