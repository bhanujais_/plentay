@extends('work.layouts.app')

@section('content')
  <form action="{{route('work.settings.update')}}" method="post" enctype="multipart/form-data">
    {{csrf_field()}}
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="row clearfix">
              <!-- Spinners -->
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                  <div class="card">
                      <div class="header">
                          <h2>Logo</h2>
                      </div>
                      <div class="body">
                          <div class="row clearfix">
                              <div class="col-md-12 text-center">

                                <div class="form-group" >
                                    <img src="{{asset($settings->logo)}}" alt="{{$settings->site_name}}" width="150px" height="50px" />
                                </div>

                                <div class="form-group form-float">
                                    <input id="image" class="form-control" name="image"  type="file">
                                </div>

                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <!-- #END# Spinners -->
              <div class="row clearfix">
              <!-- Spinners -->
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                  <div class="card">
                      <div class="header">
                          <h2>Misc</h2>
                      </div>
                      <div class="body">
                          <div class="row clearfix">
                              <div class="col-md-12">
                                <a href="{{ route('work.sitemap') }}">
                              <button type="button" class="btn btn-primary waves-effect">
                                  <i class="material-icons">build</i>
                                  <span>Generate Sitemap</span>
                              </button>
                            </a>
                              </div>

                          </div>
                      </div>
                  </div>
              </div>
            </div>
              <!-- #END# Spinners -->



              <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="card">
                                    <div class="header">
                                        <h2>Socials</h2>
                                    </div>
                                    <div class="body">
                                <div class="row clearfix">
                                    <div class="col-md-4">
                                          <div class="form-group">
                                              <div class="form-line">
                                                <label for="facebook">FaceBook</label>
                                                  <input type="text" class="form-control" name="social_facebook" value="{{$settings->social_facebook}}" placeholder="facebook">
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-md-4">
                                          <div class="form-group">
                                              <div class="form-line">
                                                <label for="facebook">Twitter</label>
                                                  <input type="text" class="form-control" name="social_twitter" value="{{$settings->social_twitter}}" placeholder="twitter">
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-md-4">
                                          <div class="form-group">
                                              <div class="form-line">
                                                <label for="facebook">Instagram</label>
                                                  <input type="text" class="form-control" name="social_instagram" value="{{$settings->social_instagram}}" placeholder="instagram">
                                              </div>
                                          </div>
                                      </div>
                                  </div>

                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="card">
                                            <div class="header">
                                                <h2>Product Settings</h2>
                                            </div>
                                            <div class="body"><center><h4><u>Hompage</u></h4></center>
                                        <div class="row clearfix">
                                            <div class="col-md-4">
                                                  <div class="form-group">
                                                      <div class="form-line">
                                                        <label for="facebook">Random Products</label>
                                                          <input type="number" class="form-control" required name="home_rand_pro" value="{{$settings->home_rand_pro}}" placeholder="home_rand_pro">
                                                      </div>
                                                  </div>
                                              </div>
                                              <div class="col-md-4">
                                                  <div class="form-group">
                                                      <div class="form-line">
                                                        <label for="facebook">Blog Posts</label>
                                                          <input type="number" class="form-control" required name="home_posts" value="{{$settings->home_posts}}" placeholder="home_posts">
                                                      </div>
                                                  </div>
                                              </div>
                                              <div class="col-md-4">
                                                  <div class="form-group">
                                                      <div class="form-line">
                                                        <label for="facebook">Merchants</label>
                                                          <input type="number" class="form-control" required name="home_users" value="{{$settings->home_users}}" placeholder="home_users">
                                                      </div>
                                                  </div>
                                              </div>
                                          </div><hr />
                                          <div class="row clearfix">
                                              <div class="col-md-3">
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                          <label for="facebook">CSV Import Limit</label>
                                                            <input type="number" class="form-control" name="csv_import_limit" value="{{$settings->csv_import_limit}}" placeholder="csv_import_limit">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                      <div class="form-group">
                                                          <div class="form-line">
                                                            <label for="facebook">Compare Similarity [%]</label>
                                                              <input type="number" class="form-control" required name="compare_percentage" min="1" max="100" value="{{$settings->compare_percentage}}" placeholder="compare_percentage">
                                                          </div>
                                                      </div>
                                                  </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                          <label for="facebook">Products per Comparison</label>
                                                            <input type="number" class="form-control" required name="compared_products" value="{{$settings->compared_products}}" placeholder="compared_products">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                          <label for="facebook">Button Name</label>
                                                            <input type="text" class="form-control" required value="{{$settings->buy_button}}" name="buy_button" placeholder="buy_button">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                <div class="col-sm-6">
                                                  <div class="form-group">
                                                    <label>Search Element</label>
                                                    <select class="form-control show-tick" required name="search_element">
                                                      <option value="amount" @if($settings->search_element == 'amount') selected @endif > Amount </option>
                                                      <option value="name" @if($settings->search_element == 'name') selected @endif > Name </option>
                                                      <option value="created_at" @if($settings->search_element == 'created_at') selected @endif > Date </option>
                                                      <option value="click_count" @if($settings->search_element == 'click_count') selected @endif > Clicks </option>
                                                      <option value="views_count" @if($settings->search_element == 'views_count') selected @endif > Views </option>
                                                    </select>
                                                </div>
                                              </div>


                                            <div class="col-sm-6">
                                              <div class="form-group">
                                                <label>Search Order</label>
                                                <select class="form-control show-tick" required name="search_order">
                                                  <option value="desc" @if($settings->search_order == 'desc') selected @endif> High to low </option>
                                                  <option value="asc" @if($settings->search_order == 'asc') selected @endif>Low to high </option>
                                                </select>
                                            </div>
                                          </div>
                                          </div>

                                      </div>
                                      <div class="row clearfix">
                                          <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="form-line">
                                                      <label for="facebook">Set Disqus Js</label>
                                                        <input type="text" class="form-control" name="disqus" value="{{$settings->disqus}}" placeholder="Disqus">
                                                    </div>
                                                </div>
                                            </div>
                                          </div>

                                    </div>
                                  </div>
                                </div>
                              </div>



                              <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card">
                                                    <div class="header">
                                                        <h2>SEO</h2>
                                                    </div>
                                                    <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-md-3">
                                                          <div class="form-group">
                                                              <div class="form-line">
                                                                <label for="facebook">Sitename</label>
                                                                  <input type="text" class="form-control"required name="site_name" value="{{$settings->site_name}}" placeholder="site_name">
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-md-3">
                                                          <div class="form-group">
                                                              <div class="form-line">
                                                                <label for="facebook">Meta Name</label>
                                                                  <input type="text" class="form-control" name="meta_name" value="{{$settings->meta_name}}" placeholder="meta_name">
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-md-3">
                                                          <div class="form-group">
                                                              <div class="form-line">
                                                                <label for="facebook">About</label>
                                                                  <input type="text" class="form-control" name="site_about" value="{{$settings->site_about}}" placeholder="site_about">
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-md-3">
                                                          <div class="form-group">
                                                              <div class="form-line">
                                                                <label for="facebook">Keywords</label>
                                                                  <input type="text" class="form-control" name="keywords" value="{{$settings->keywords}}" placeholder="keywords">
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-md-3">
                                                          <div class="form-group">
                                                              <div class="form-line">
                                                                <label for="facebook">Currency Symbol</label>
                                                                  <input type="text" class="form-control" required name="currency_symbol" value="{{$settings->currency_symbol}}" placeholder="currency_symbol">
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-md-3">
                                                          <div class="form-group">
                                                              <div class="form-line">
                                                                <label for="facebook">Currency Name</label>
                                                                  <input type="text" class="form-control" name="currency_name" value="{{$settings->currency_name}}" placeholder="currency_name">
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-md-3">
                                                          <div class="form-group">
                                                              <div class="form-line">
                                                                <label for="facebook">Currency Code</label>
                                                                  <input type="text" class="form-control" required name="currency_code" value="{{$settings->currency_code}}" placeholder="currency_code">
                                                              </div>
                                                          </div>
                                                      </div>

                                                      <div class="col-md-3">
                                                          <div class="form-group">
                                                              <div class="form-line">
                                                                <label for="facebook">Email</label>
                                                                  <input type="text" class="form-control" required name="site_email" value="{{$settings->site_email}}" placeholder="site_email">
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-md-3">
                                                          <div class="form-group">
                                                              <div class="form-line">
                                                                <label for="facebook">Phone Number</label>
                                                                  <input type="text" class="form-control" name="site_number" value="{{$settings->site_number}}" placeholder="site_number">
                                                              </div>
                                                          </div>
                                                      </div>
                                                  </div>

                                            </div>
                                          </div>
                                        </div>
                                      </div>

                                      <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="card">
                                                            <div class="header">
                                                                <h2>Payment Gateways</h2>
                                                            </div>
                                                            <div class="body"><center><h4><u>PayPal</u></h4></center>
                                                        <div class="row clearfix">
                                                            <div class="col-md-2">
                                                                  <div class="form-group">
                                                                      <div class="form-line">
                                                                        <label for="facebook">PayPal Status</label>
                                                                        <div class="switch">
                                                                            <label>OFF
                                                                              <input type="checkbox" name="paypal_active" @if ($gateways->paypal_active ==1 ) checked @else @endif >
                                                                              <span class="lever"></span>
                                                                              ON</label>
                                                                        </div>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                              <div class="col-md-5">
                                                                  <div class="form-group">
                                                                      <div class="form-line">
                                                                        <label for="paypal_client_id">PayPal Client ID</label>
                                                                          <input type="text" class="form-control"  name="paypal_client_id" value="{{$gateways->paypal_client_id}}" placeholder="paypal_client_id">
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                              <div class="col-md-5">
                                                                  <div class="form-group">
                                                                      <div class="form-line">
                                                                        <label for="paypal_client_secret">PayPal Secret</label>
                                                                          <input type="text" class="form-control" name="paypal_client_secret" value="{{$gateways->paypal_client_secret}}" placeholder="paypal_client_secret">
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      <div class="row clearfix">
                                                          <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                      <label for="facebook">Stripe Status</label>
                                                                      <div class="switch">
                                                                          <label>OFF
                                                                            <input type="checkbox" name="stripe_active" @if ($gateways->stripe_active ==1 ) checked @else @endif >
                                                                            <span class="lever"></span>
                                                                            ON</label>
                                                                      </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-5">
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                      <label for="stripe_publishable_key">Stripe Publishable Key</label>
                                                                        <input type="text" class="form-control"  name="stripe_publishable_key" value="{{$gateways->stripe_publishable_key}}" placeholder="stripe_publishable_key">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-5">
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                      <label for="stripe_secret_key">Stripe Secret Key</label>
                                                                        <input type="text" class="form-control" name="stripe_secret_key" value="{{$gateways->stripe_secret_key}}" placeholder="stripe_secret_key">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                          <div class="row clearfix">
                                                              <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <div class="form-line">
                                                                          <label for="facebook">VoguePay Status</label>
                                                                          <div class="switch">
                                                                              <label>OFF
                                                                                <input type="checkbox" name="voguepay_active" @if ($gateways->voguepay_active ==1 ) checked @else @endif >
                                                                                <span class="lever"></span>
                                                                                ON</label>
                                                                          </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-10">
                                                                      <div class="form-group">
                                                                          <div class="form-line">
                                                                            <label for="facebook">VoguePay Merchant ID</label>
                                                                              <input type="text" class="form-control" name="voguepay_merchant_id" min="1" max="100" value="{{$gateways->voguepay_merchant_id}}" placeholder="voguepay_merchant_id">
                                                                          </div>
                                                                      </div>
                                                                  </div>

                                                                  <div class="row clearfix">
                                                                      <div class="col-md-2">
                                                                            <div class="form-group">
                                                                                <div class="form-line">
                                                                                  <label for="facebook">Bank Status</label>
                                                                                  <div class="switch">
                                                                                      <label>OFF
                                                                                        <input type="checkbox" name="bankwire_active" @if ($gateways->bankwire_active ==1 ) checked @else @endif >
                                                                                        <span class="lever"></span>
                                                                                        ON</label>
                                                                                  </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-10">
                                                                              <div class="form-group">
                                                                                  <div class="form-line">
                                                                                    <label for="facebook">Bank Wire</label>
                                                                                    <a href="{{ route('work.banks') }}">
                                                                                  <button type="button" class="btn btn-primary waves-effect">
                                                                                      <i class="material-icons">build</i>
                                                                                      <span>Click to view and add banks</span>
                                                                                  </button>
                                                                                </a>

                                                                                  </div>
                                                                              </div>
                                                                          </div>

                                                      </div>

                                                    </div>
                                                  </div>
                                                </div>
                                              </div>

                                      <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="card">
                                                            <div class="header">
                                                                <h2></h2>
                                                            </div>
                                                          <div class="body">
                                                        <div class="row clearfix">
                                                          <div class="col-md-12">
                                                            <div class="text-center">
                                                          <button type="submit" class="btn btn-primary waves-effect">
                                                                <i class="material-icons text">save</i>
                                                                <span>Save Settings</span>
                                                            </button>
                                                          </div>
                                                        </div>
                                                        </div>

                                                    </div>
                                                  </div>
                                                </div>
                                              </div>




</div>
</form>
@endsection
@section('mainjs_script')
<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
@endsection
