@extends('work.layouts.app')

@section('content')

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <div class="card">
                          <div class="header">
                              <h2>
                                  REGULAR EXPRESSION
                              </h2>
                              <ul class="header-dropdown m-r--5">
                                  <li class="dropdown">
                                      <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                          <i class="material-icons">more_vert</i>
                                      </a>
                                      <ul class="dropdown-menu pull-right">
                                          <li><a href="" class=" waves-effect waves-block">Refresh</a></li>
                                      </ul>
                                  </li>
                              </ul>
                          </div>
                          <div class="body">
                            @if (count($errors)>0)
                              <ul class="list-group">
                                @foreach($errors->all() as $error)
                                  <li class="list-group-item text-danger">
                                    {{$error}}
                                  </li>
                                @endforeach

                              </ul>
                            @endif
                            <h6>{!!Session::get('message')!!}</h6>

                              <form action="{{route('work.settings.run_regex')}}" method="post" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input id="product_url" class="form-control" name="product_url"  required type="text">
                                        <label class="form-label">Product URL </label>
                                    </div>
                                </div>
                                <!-- <div class="form-group form-float">
                                    <div class="form-line">
                                        <input id="product_block" class="form-control" name="product_block"  required type="text">
                                        <label class="form-label">Product Block </label>
                                    </div>
                                </div> -->
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input id="product_block_element" class="form-control" name="product_block_element"  required type="text">
                                        <label class="form-label">Product Block Element </label>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                    <label>Type</label>
                                    <select class="form-control show-tick" required name="type">
                                      <option value="1" selected>Numeric</option>
                                      <option value="2"> Text </option>
                                      <option value="3"> Html </option>
                                    </select>
                                </div>
                              </div>
                                  <br>
                                  <button  type="submit" class="btn btn-primary m-t-15 waves-effect">Run</button>
                              </form>
                          </div>
                      </div>

</div>

@endsection
@section('mainjs_script')
<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
@endsection
