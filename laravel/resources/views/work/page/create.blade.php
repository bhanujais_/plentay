@extends('work.layouts.app')

@section('content')

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Create Page
                                <small></small>
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="" class=" waves-effect waves-block">Refresh</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                          @if (count($errors)>0)
                            <ul class="list-group">
                              @foreach($errors->all() as $error)
                                <li class="list-group-item text-danger">
                                  {{$error}}
                                </li>
                              @endforeach

                            </ul>
                          @endif
                            <form action="{{route('work.page.store')}}" method="post" enctype="multipart/form-data">
                              {{csrf_field()}}
                              <div class="form-group form-float">
                                  <div class="form-line">
                                      <input id="title" class="form-control" name="title"  required type="text">
                                      <label class="form-label">Page Title </label>
                                  </div>
                              </div>
                              <div class="form-group form-float" >
                                  <label class="form-label">Contents</label>
                                    <div class="form-line">
                                        <textarea rows="5"  class="form-control no-resize" id="summernote" required name="content" placeholder="More Info" >
                                        </textarea>
                                    </div>
                              </div>
                                <br>
                                <button  type="submit" class="btn btn-primary m-t-15 waves-effect">Create</button>
                            </form>
                        </div>
                    </div>
                </div>

@endsection
@section('mainjs_script')
<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
<script>
$(document).ready(function() {
  $('#summernote').summernote({
    height:150
  });

});
</script>
@endsection
