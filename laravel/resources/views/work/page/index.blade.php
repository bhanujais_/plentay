@extends('work.layouts.app')


@section('content')

    <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
    <!-- <script src="https://cdn.datatables.net/responsive/2.2.2/js/dataTables.responsive.min.js"></script> -->
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>

    <!-- <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.2/css/responsive.dataTables.min.css" /> -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css" />


<!-- <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> -->


<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               Pages
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="" class=" waves-effect waves-block">Refresh</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="icon-and-text-button-demo">
                              <a href="{{ route('work.page.create') }}">
                                <button type="button" class="btn btn-primary waves-effect">
                                    <i class="material-icons">add_circle_outline</i>
                                    <span>Add Page</span>
                                </button>
                              </a>
                                <a href="">
                                <button type="button" class="btn btn-warning waves-effect">
                                    <i class="material-icons">cached</i>
                                    <span>Refresh</span>
                                </button>
                              </a>
                            </div>
                        </div>
                    </div>
                </div>




<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="container">
                <hr />
                <div class="table-responsive">
                <table id="data_table" class="table table-bordered table-striped table-hover js-basic-example dataTable">
                    <thead>
                        <tr>
							<th>ID</th>
                            <th>Title</th>
                            <th>Date</th>
                            <th>More</th>
                        </tr>
                    </thead>
                </table>
              </div>
            </div>
            </div>
<style>
div.container {
        width: 100%;
    }

</style>

            <script type="text/javascript">
            //get
                        $(document).ready(function() {
                             $('#data_table').DataTable({
                                "processing": true,
                                "serverSide": true,
                                "responsive": true,
                                "ajax": "{{ route('work.get_pages_data') }}",
                                "columns":[
									{ "data": "id" },
                                    { "data": "title" },
                                    { "data": "created_at" },
                                    { "data": "action","searchable":false,"orderable":false}
                                ],
                                order:[ [0, 'desc'] ],
                                "dom": 'Bfrtip',
                                "buttons": ['pageLength','copy', 'csv', 'excel', 'pdf', 'print']

                             });
                        });
                        </script>



<hr/>

@endsection
