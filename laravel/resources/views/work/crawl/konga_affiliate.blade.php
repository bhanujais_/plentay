@extends('work.layouts.app')

@section('content')

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <div class="card">
                          <div class="header">
                              <h2>Konga Affiliate ID <i class="fas fa-magnet"></i></h2>
                          </div>
                          <div class="body">
                            @if (count($errors)>0)
                              <ul class="list-group">
                                @foreach($errors->all() as $error)
                                  <li class="list-group-item text-danger">
                                    {{$error}}
                                  </li>
                                @endforeach

                              </ul>
                            @endif
                            <form action="{{route('work.save.konga')}}" method="post" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input id="affiliate_id" class="form-control" name="affiliate_id"  value="{{$crawler_konga->affiliate_id}}" required type="text">
                                        <label class="form-label">Affiliate Id</label>
                                    </div>
                                </div>
                                  <br>
                                  <button  type="submit" class="btn btn-primary m-t-15 waves-effect">Save</button>
                              </form>
                          </div>
                      </div>

</div>

@endsection
@section('mainjs_script')
<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
@endsection
