@extends('work.layouts.app')

@section('content')


<!-- #END# Widgets -->
<!-- CPU Usage -->
<div class="row clearfix">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="header">
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-6">
                        <h2></h2>
                    </div>
                </div>
            </div>
            <div class="body">
              <div class="row clearfix">

                  <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                      <div class="info-box bg-cyan hover-expand-effect">
                          <div class="icon">
                              <i class="material-icons">playlist_add_check</i>
                          </div>
                          <div class="content">
                              <div class="text">Products!</div>
                              <div class="number count-to" data-from="0" data-to="{{$products_count}}" data-speed="15" data-fresh-interval="20"></div>
                          </div>
                      </div>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                      <div class="info-box bg-cyan hover-expand-effect">
                          <div class="icon">
                              <i class="material-icons">exposure_plus_1</i>
                          </div>
                          <div class="content">
                              <div class="text">Impressions!</div>
                              <div class="number count-to" data-from="0" data-to="{{$products_impressions}}" data-speed="1000" data-fresh-interval="20"></div>
                          </div>
                      </div>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                      <div class="info-box bg-cyan hover-expand-effect">
                          <div class="icon">
                              <i class="material-icons">link</i>
                          </div>
                          <div class="content">
                              <div class="text">Clicks!</div>
                              <div class="number count-to" data-from="0" data-to="{{$products_clicks}}" data-speed="1000" data-fresh-interval="20"></div>
                          </div>
                      </div>
                  </div>
              </div>


              <div class="row clearfix">

                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="info-box bg-orange hover-expand-effect">
                        <i class="fa fa-credit-card fa-5x"></i>
                        <div class="content">
                            <div class="text">Total Credits! ({!!$settings->currency_symbol !!})</div>
                            <div class="number count-to" data-from="0" data-to="{{$users_credits}}" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-cyan hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">people</i>
                        </div>
                        <div class="content">
                            <div class="text">Marchant!</div>
                            <div class="number count-to" data-from="0" data-to="{{$marchant_users}}" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-light-green hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">people</i>
                        </div>
                        <div class="content">
                            <div class="text">Users!</div>
                            <div class="number count-to" data-from="0" data-to="{{$users_count}}" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>

                  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                      <div class="info-box bg-pink hover-expand-effect">
                          <div class="icon">
                              <i class="material-icons">do_not_disturb</i>
                          </div>
                          <div class="content">
                              <div class="text">Inactive Users!</div>
                              <div class="number count-to" data-from="0" data-to="{{$inactive_users}}" data-speed="15" data-fresh-interval="20"></div>
                          </div>
                      </div>
                  </div>


              </div>


            </div>
        </div>
    </div>
</div>

@endsection
@section('mainjs_script')
<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
@endsection
