@extends('work.layouts.app')

@section('content')

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Create Bank
                                <small></small>
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="" class=" waves-effect waves-block">Refresh</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                          @if (count($errors)>0)
                            <ul class="list-group">
                              @foreach($errors->all() as $error)
                                <li class="list-group-item text-danger">
                                  {{$error}}
                                </li>
                              @endforeach

                            </ul>
                          @endif
                            <form action="{{route('work.bank.store')}}" method="post" enctype="multipart/form-data">
                              {{csrf_field()}}
                              <div class="form-group form-float">
                                  <div class="form-line">
                                      <input id="bank_name" class="form-control" name="bank_name"  required type="text">
                                      <label class="form-label">Bank Name </label>
                                  </div>
                                </div>
                                <div class="form-group form-float">
                                      <div class="form-line">
                                          <input id="account_name" class="form-control" name="account_name"  required type="text">
                                          <label class="form-label">Account Name </label>
                                      </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input id="bank_name" class="form-control" name="account_number"  required type="text">
                                        <label class="form-label">Account Number</label>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                  <label class="form-label">Additional Information</label>
                                    <div class="form-line">
                                        <textarea rows="4"  class="form-control no-resize" id="summernote" name="other_details" placeholder="More Info" >
                                        </textarea>
                                    </div>
                                </div>
                                <br>
                                <button  type="submit" class="btn btn-primary m-t-15 waves-effect">Create</button>
                            </form>
                        </div>
                    </div>
                </div>

@endsection
@section('mainjs_script')
<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
@endsection
