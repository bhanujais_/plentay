@extends('work.layouts.app')

@section('content')

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Credit: Edit
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="" class=" waves-effect waves-block">Refresh</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                          @if (count($errors)>0)
                            <ul class="list-group">
                              @foreach($errors->all() as $error)
                                <li class="list-group-item text-danger">
                                  {{$error}}
                                </li>
                              @endforeach

                            </ul>
                          @endif
                          <form action="{{route('work.credit.update',['id'=>$credit->id])}}" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                              <div class="form-group form-float">
                                  <div class="form-line">
                                      <input id="package_name" class="form-control"  name="package_name" value="{{$credit->package_name}}"  required type="text">
                                      <label class="form-label">Package Name</label>
                                  </div>
                              </div>
                              <div class="form-group form-float">
                                  <div class="form-line">
                                      <input id="rate_per_click" class="form-control" name="rate_per_click" value="{{$credit->rate_per_click}}" required type="text">
                                      <label class="form-label">CPC Rate ({{$settings->currency_symbol}})</label>
                                  </div>
                              </div>

                                <div class="form-group form-float">
                                    <label class="form-label">Description</label>
                                    <div class="form-line">
                                        <textarea rows="2" id="summernote" required class="form-control no-resize" name="description">
                                          {{$credit->description}}
                                        </textarea>
                                    </div>
                                </div>

                                <br>
                                <button  type="submit" class="btn btn-primary m-t-15 waves-effect"><i class="material-icons">save</i>Update</button>
                            </form>
                        </div>
                    </div>
                </div>

@endsection
@section('mainjs_script')
<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
<script>
$(document).ready(function() {
  $('#summernote').summernote();
});
</script>


@endsection
