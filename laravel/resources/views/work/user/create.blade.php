@extends('work.layouts.app')

@section('content')

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Create Merchant User
                                <small></small>
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="" class=" waves-effect waves-block">Refresh</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                          @if (count($errors)>0)
                            <ul class="list-group">
                              @foreach($errors->all() as $error)
                                <li class="list-group-item text-danger">
                                  {{$error}}
                                </li>
                              @endforeach

                            </ul>
                          @endif
                            <form action="{{route('work.user.store')}}" method="post" enctype="multipart/form-data">
                              {{csrf_field()}}
                              <div class="form-group">
                                  <label for="category">Select Currency</label>
                                  <select id="currency_id" name="currency_id" class="form-control show-tick" >
                                    @foreach($currencies as $currency)
                                      <option value="{{$currency->id}}">{{$currency->name}} ({{$currency->symbol}})</option>
                                    @endforeach
                                </select>
                              </div>
                              <div class="form-group form-float">
                                  <div class="form-line">
                                      <input id="name" class="form-control" name="name"  required type="text">
                                      <label class="form-label">Username</label>
                                  </div>
                              </div>
                              <div class="form-group form-float">
                                  <label class="form-label">Featured image</label>
                                  <input id="image" class="form-control" name="image"  required type="file">
                              </div>
                              <div class="form-group form-float">
                                  <div class="form-line">
                                      <input id="contact_name" class="form-control" name="contact_name"  required type="text">
                                      <label class="form-label">Contact Name </label>
                                  </div>
                              </div>
                              <div class="form-group form-float">
                                  <div class="form-line">
                                      <input id="url" class="form-control" name="url"  required type="text">
                                      <label class="form-label">Website</label>
                                  </div>
                              </div>

                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input id="email_address" class="form-control" required name="email" type="email">
                                        <label class="form-label">Email Address</label>
                                    </div>
                                </div>
                                <input id="active" class="form-control" required name="active" value="1" type="hidden">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input id="password" class="form-control" required name="password" type="password">
                                        <label class="form-label">Password</label>
                                    </div>
                                </div>
                                <br>
                                <button  type="submit" class="btn btn-primary m-t-15 waves-effect">Create</button>
                            </form>
                        </div>
                    </div>
                </div>

@endsection
@section('mainjs_script')
<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
@endsection
