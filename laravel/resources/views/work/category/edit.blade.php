@extends('work.layouts.app')

@section('content')

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Edit Category: {{$category->name}}
                                <small></small>
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="" class=" waves-effect waves-block">Refresh</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>

                        <div class="body">
                            <form action="{{route('work.category.update',['id'=>$category->id])}}" method="post">
                              {{csrf_field()}}
                              <label for="category">Select a Category [General is the Root Directory]</label>
                              {!! $cat !!}
                              <div class="form-group form-float">
                                  <div class="form-line">
                                      <input id="name" class="form-control" name="name" value="{{$category->name}}"  required type="text">
                                      <label class="form-label">Name</label>
                                  </div>
                              </div>
                              <div class="form-group form-float">
                                  <div class="form-line">
                                      <input id="description" class="form-control" name="description" value="{{$category->description}}"  required type="text">
                                      <label class="form-label">Description</label>
                                  </div>
                              </div>

                                <br>
                                <button  type="submit" class="btn btn-primary m-t-15 waves-effect"><i class="material-icons">save</i>Update</button>
                            </form>
                        </div>
                    </div>
                </div>

@endsection
@section('mainjs_script')
<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
@endsection
