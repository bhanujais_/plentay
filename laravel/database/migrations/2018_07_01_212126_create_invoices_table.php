<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('package_name');
            $table->text('description');
            $table->integer('user_id');
            $table->string('invoice_number')->unique();
            $table->string('payment_method',50)->default('Nil');
            $table->decimal('rate_per_click', 9,2)->default(1);
            // $table->integer('rate_per_click')->default(1);
            // $table->bigInteger('amount')->default(0);
            // $table->bigInteger('amount_raw')->default(0);
            $table->decimal('amount', 9,2)->default(0);
            $table->decimal('amount_raw', 9,2)->default(0);
            $table->integer('status')->default(0);
            $table->integer('vat')->default(0);
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
