<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credits', function (Blueprint $table) {
            $table->increments('id');
            $table->string('package_name')->unique();
            $table->string('description')->nullable()->default('');
            // $table->integer('rate_per_click')->default(1);
            $table->decimal('rate_per_click', 9,2)->default(1);
            $table->integer('min')->default(1);
            $table->integer('max')->default(1000000);
            $table->integer('vat')->default(0);
            $table->timestamps();
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credits');
    }
}
