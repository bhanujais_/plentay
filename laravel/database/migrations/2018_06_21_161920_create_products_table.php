<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name');
            // $table->bigInteger('amount');
            $table->decimal('amount', 9,2)->nullable();
            $table->text('description')->nullable();
            $table->text('slug');
            $table->text('image');
            $table->text('affiliate_url');
            $table->text('original_url');
            $table->mediumInteger('user_id');
            $table->bigInteger('views_count')->default(0);
            $table->bigInteger('click_count')->default(0);
            $table->integer('category_id')->default(1);
            $table->integer('rating_id')->nullable()->default(4);
            $table->boolean('active')->default(1);
            $table->timestamps();
        });
    }






    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
