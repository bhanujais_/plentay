<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('contact_name')->nullable()->default('');
            $table->string('address')->nullable()->default('');
            $table->string('url')->nullable()->default('');
            $table->string('image')->nullable()->default('');
            $table->text('about')->nullable();
            $table->string('phone_number')->nullable()->default('');
            // $table->bigInteger('credit')->default(0);
            $table->decimal('credit', 9,2)->default(0.00);
            $table->integer('currency_id')->default(147);
            $table->string('validation_code')->default(0);
            $table->boolean('active')->default(0);
            $table->string('price_update_block')->nullable()->default('');
            $table->string('price_update_element')->nullable()->default('');
            $table->string('description_update_element')->nullable()->default('');
            $table->rememberToken();
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
