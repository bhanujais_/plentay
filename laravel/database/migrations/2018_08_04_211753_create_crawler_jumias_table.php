<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCrawlerJumiasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crawler_jumias', function (Blueprint $table) {
            $table->increments('id');
            $table->string('product_block_ini')->nullable()->default('div[class="sku -gallery"]');
            $table->string('product_name_element')->nullable()->default('span[class="name"]');
            $table->string('product_url_element')->nullable()->default('a[class="link"]');
            $table->string('product_image_element')->nullable()->default('.lazy.image');//*[class="lazy image"]
            $table->string('product_price_element')->nullable()->default('span.price');
            $table->string('affiliate_id_start')->default('');
            $table->string('affiliate_id_end')->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crawler_jumias');
    }
}
