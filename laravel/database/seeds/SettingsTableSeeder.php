<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      App\Setting::create([
      'disqus'=>'https://comparison-1.disqus.com/embed.js',
      'currency_symbol' =>'$',
      'currency_name' =>'United States Dollars',
      'currency_code'=>'USD',
      'logo'=>'uploads/logo/logo.png',
      'site_name'=>'Affiliate Boss Compare',
      'site_email'=>'info@products.com.ng',
      'site_about'=>'Lorem ipsum dolor sit amet, anim id est laborum. Sed ut perspconsectetur, adipisci vam aliquam qua',
      'site_address'=>'Lorem ipsum dolor sit amet, anim id est laborum. Sed ut perspconsectetur, adipisci vam aliquam qua',
      'site_number'=>'+123456789',
      'live_production'=>'1',
      'csv_import_limit'=>'1000',
      'home_rand_pro'=>'8',
      'home_posts'=>'4',
      'home_users'=>'6',
      'buy_button'=>'BuyNow!',

    ]);
    }
}
