<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PagesTableSeeder::class);
        $this->call(AdminsTableSeeder::class);
        $this->call(CategoryTableSeeder::class);
        $this->call(CreditTableSeeder::class);
        $this->call(BanksTableSeeder::class);
        $this->call(CurrencyTableSeeder::class);
        $this->call(GatewayTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
        $this->call(SliderTableSeeder::class);
        // CRAWLER
        $this->call(KongaTableSeeder::class);
        $this->call(JumiaTableSeeder::class);
        $this->call(EbayTableSeeder::class);



    }
}
