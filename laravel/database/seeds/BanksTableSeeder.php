<?php

use Illuminate\Database\Seeder;

class BanksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      App\Bank::create([
      'account_name' =>'John Doe',
      'account_number' =>'987654321',
      'bank_name' =>'Gurantee Trust Bank',
      'other_details'=>'Extra Information Such as Swift Code',
    ]);
    }
}
