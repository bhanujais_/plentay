<?php

use Illuminate\Database\Seeder;

class CreditTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      App\Credit::create([
      'package_name' =>'Package Name',
      'description' =>'Package Description',
      'rate_per_click' =>'5.00',
      'min' =>'50',
      'max' =>'1000000',
      'vat'=>'0',
    ]);
    }
}
