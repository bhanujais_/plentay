<div id="footer">
    <div>
    &laquo;EasyInstaller &raquo; <?php// echo EI_VERSION;?> &nbsp;<a href="https://scriptorigin.freshdesk.com/">Help</a>
    <?php if(EI_LICENSE_AGREEMENT_PAGE != ''){ ?>
        : <a href="<?php echo '../'.EI_LICENSE_AGREEMENT_PAGE;?>" target="_blank"><?php echo lang_key('license'); ?></a>
    <?php } ?>
    </div>
</div>
