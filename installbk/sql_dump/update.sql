-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 17, 2018 at 05:04 AM
-- Server version: 5.7.19
-- PHP Version: 7.1.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecommerce`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admins_name_unique` (`name`),
  UNIQUE KEY `admins_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

UPDATE `admins` SET `id` = 1,`name` = 'rootuser',`display_name` = 'Root',`email` = 'info@products.com.ng',`password` = '$2y$10$skJItBy6MkU9BqVglTIo/OCokvaipHNKHM2lYNOhUocecD0hpGtR.',`remember_token` = NULL,`created_at` = '2018-12-16 20:40:39',`updated_at` = '2018-12-16 20:40:39' WHERE `admins`.`id` = 1;

-- --------------------------------------------------------

--
-- Table structure for table `banks`
--

CREATE TABLE IF NOT EXISTS `banks` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `account_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bank_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `other_details` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banks`
--

UPDATE `banks` SET `id` = 1,`account_name` = 'John Doe',`account_number` = '987654321',`bank_name` = 'Gurantee Trust Bank',`other_details` = 'Extra Information Such as Swift Code',`created_at` = '2018-12-16 20:40:40',`updated_at` = '2018-12-16 20:40:40' WHERE `banks`.`id` = 1;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

UPDATE `categories` SET `id` = 1,`parent_id` = 0,`name` = 'General',`slug` = 'general',`description` = 'Default Root Table',`created_at` = '2018-12-16 20:40:39',`updated_at` = '2018-12-16 20:40:39' WHERE `categories`.`id` = 1;

-- --------------------------------------------------------

--
-- Table structure for table `crawler_amazons`
--

CREATE TABLE IF NOT EXISTS `crawler_amazons` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `crawler_ebays`
--

CREATE TABLE IF NOT EXISTS `crawler_ebays` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_block_ini` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'li.s-item',
  `product_name_element` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'h3.s-item__title',
  `product_url_element` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'a.s-item__link',
  `product_image_element` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'img.s-item__image-img',
  `product_price_element` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'span.s-item__price',
  `affiliate_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `crawler_ebays`
--

UPDATE `crawler_ebays` SET `id` = 1,`product_block_ini` = 'li.s-item',`product_name_element` = 'h3.s-item__title',`product_url_element` = 'a.s-item__link',`product_image_element` = 'img.s-item__image-img',`product_price_element` = 'span.s-item__price',`affiliate_id` = '',`created_at` = '2018-12-16 20:41:00',`updated_at` = '2018-12-16 20:41:00' WHERE `crawler_ebays`.`id` = 1;

-- --------------------------------------------------------

--
-- Table structure for table `crawler_jumias`
--

CREATE TABLE IF NOT EXISTS `crawler_jumias` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_block_ini` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'div[class="sku -gallery"]',
  `product_name_element` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'span[class="name"]',
  `product_url_element` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'a[class="link"]',
  `product_image_element` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '.lazy.image',
  `product_price_element` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'span.price',
  `affiliate_id_start` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `affiliate_id_end` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `crawler_jumias`
--

UPDATE `crawler_jumias` SET `id` = 1,`product_block_ini` = 'div[class=\"sku -gallery\"]',`product_name_element` = 'span[class=\"name\"]',`product_url_element` = 'a[class=\"link\"]',`product_image_element` = '.lazy.image',`product_price_element` = 'span.price',`affiliate_id_start` = '',`affiliate_id_end` = '',`created_at` = '2018-12-16 20:41:00',`updated_at` = '2018-12-16 20:41:00' WHERE `crawler_jumias`.`id` = 1;

-- --------------------------------------------------------

--
-- Table structure for table `crawler_kongas`
--

CREATE TABLE IF NOT EXISTS `crawler_kongas` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `affiliate_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'quickprice',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `crawler_kongas`
--

UPDATE `crawler_kongas` SET `id` = 1,`affiliate_id` = '',`created_at` = '2018-12-16 20:41:00',`updated_at` = '2018-12-16 20:41:00' WHERE `crawler_kongas`.`id` = 1;

-- --------------------------------------------------------

--
-- Table structure for table `credits`
--

CREATE TABLE IF NOT EXISTS `credits` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `package_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `rate_per_click` decimal(9,2) NOT NULL DEFAULT '1.00',
  `min` int(11) NOT NULL DEFAULT '1',
  `max` int(11) NOT NULL DEFAULT '1000000',
  `vat` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `credits_package_name_unique` (`package_name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `credits`
--

UPDATE `credits` SET `id` = 1,`package_name` = 'Package Name',`description` = 'Package Description',`rate_per_click` = '5.00',`min` = 50,`max` = 1000000,`vat` = 0,`created_at` = '2018-12-16 20:40:39',`updated_at` = '2018-12-16 20:40:39' WHERE `credits`.`id` = 1;

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE IF NOT EXISTS `currencies` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `symbol` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `html_entity` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=165 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `currencies`
--

UPDATE `currencies` SET `id` = 1,`name` = 'United Arab Emirates Dirham',`code` = 'AED',`symbol` = 'د.إ',`html_entity` = '',`created_at` = '2018-12-16 20:40:40',`updated_at` = '2018-12-16 20:40:40' WHERE `currencies`.`id` = 1;
UPDATE `currencies` SET `id` = 2,`name` = 'Afghan Afghani',`code` = 'AFN',`symbol` = '؋',`html_entity` = '',`created_at` = '2018-12-16 20:40:40',`updated_at` = '2018-12-16 20:40:40' WHERE `currencies`.`id` = 2;
UPDATE `currencies` SET `id` = 3,`name` = 'Albanian Lek',`code` = 'ALL',`symbol` = 'L',`html_entity` = '',`created_at` = '2018-12-16 20:40:40',`updated_at` = '2018-12-16 20:40:40' WHERE `currencies`.`id` = 3;
UPDATE `currencies` SET `id` = 4,`name` = 'Armenian Dram',`code` = 'AMD',`symbol` = 'դր.',`html_entity` = '',`created_at` = '2018-12-16 20:40:40',`updated_at` = '2018-12-16 20:40:40' WHERE `currencies`.`id` = 4;
UPDATE `currencies` SET `id` = 5,`name` = 'Netherlands Antillean Gulden',`code` = 'ANG',`symbol` = 'ƒ',`html_entity` = '&#x0192;',`created_at` = '2018-12-16 20:40:40',`updated_at` = '2018-12-16 20:40:40' WHERE `currencies`.`id` = 5;
UPDATE `currencies` SET `id` = 6,`name` = 'Angolan Kwanza',`code` = 'AOA',`symbol` = 'Kz',`html_entity` = '',`created_at` = '2018-12-16 20:40:40',`updated_at` = '2018-12-16 20:40:40' WHERE `currencies`.`id` = 6;
UPDATE `currencies` SET `id` = 7,`name` = 'Argentine Peso',`code` = 'ARS',`symbol` = '$',`html_entity` = '&#x20B1;',`created_at` = '2018-12-16 20:40:40',`updated_at` = '2018-12-16 20:40:40' WHERE `currencies`.`id` = 7;
UPDATE `currencies` SET `id` = 8,`name` = 'Australian Dollar',`code` = 'AUD',`symbol` = '$',`html_entity` = '$',`created_at` = '2018-12-16 20:40:40',`updated_at` = '2018-12-16 20:40:40' WHERE `currencies`.`id` = 8;
UPDATE `currencies` SET `id` = 9,`name` = 'Aruban Florin',`code` = 'AWG',`symbol` = 'ƒ',`html_entity` = '&#x0192;',`created_at` = '2018-12-16 20:40:40',`updated_at` = '2018-12-16 20:40:40' WHERE `currencies`.`id` = 9;
UPDATE `currencies` SET `id` = 10,`name` = 'Azerbaijani Manat',`code` = 'AZN',`symbol` = 'null',`html_entity` = '',`created_at` = '2018-12-16 20:40:40',`updated_at` = '2018-12-16 20:40:40' WHERE `currencies`.`id` = 10;
UPDATE `currencies` SET `id` = 11,`name` = 'Bosnia and Herzegovina Convertible Mark',`code` = 'BAM',`symbol` = 'КМ',`html_entity` = '',`created_at` = '2018-12-16 20:40:40',`updated_at` = '2018-12-16 20:40:40' WHERE `currencies`.`id` = 11;
UPDATE `currencies` SET `id` = 12,`name` = 'Barbadian Dollar',`code` = 'BBD',`symbol` = '$',`html_entity` = '$',`created_at` = '2018-12-16 20:40:41',`updated_at` = '2018-12-16 20:40:41' WHERE `currencies`.`id` = 12;
UPDATE `currencies` SET `id` = 13,`name` = 'Bangladeshi Taka',`code` = 'BDT',`symbol` = '৳',`html_entity` = '',`created_at` = '2018-12-16 20:40:41',`updated_at` = '2018-12-16 20:40:41' WHERE `currencies`.`id` = 13;
UPDATE `currencies` SET `id` = 14,`name` = 'Bulgarian Lev',`code` = 'BGN',`symbol` = 'лв',`html_entity` = '',`created_at` = '2018-12-16 20:40:41',`updated_at` = '2018-12-16 20:40:41' WHERE `currencies`.`id` = 14;
UPDATE `currencies` SET `id` = 15,`name` = 'Bahraini Dinar',`code` = 'BHD',`symbol` = 'ب.د',`html_entity` = '',`created_at` = '2018-12-16 20:40:41',`updated_at` = '2018-12-16 20:40:41' WHERE `currencies`.`id` = 15;
UPDATE `currencies` SET `id` = 16,`name` = 'Burundian Franc',`code` = 'BIF',`symbol` = 'Fr',`html_entity` = '',`created_at` = '2018-12-16 20:40:41',`updated_at` = '2018-12-16 20:40:41' WHERE `currencies`.`id` = 16;
UPDATE `currencies` SET `id` = 17,`name` = 'Bermudian Dollar',`code` = 'BMD',`symbol` = '$',`html_entity` = '$',`created_at` = '2018-12-16 20:40:41',`updated_at` = '2018-12-16 20:40:41' WHERE `currencies`.`id` = 17;
UPDATE `currencies` SET `id` = 18,`name` = 'Brunei Dollar',`code` = 'BND',`symbol` = '$',`html_entity` = '$',`created_at` = '2018-12-16 20:40:41',`updated_at` = '2018-12-16 20:40:41' WHERE `currencies`.`id` = 18;
UPDATE `currencies` SET `id` = 19,`name` = 'Bolivian Boliviano',`code` = 'BOB',`symbol` = 'Bs.',`html_entity` = '',`created_at` = '2018-12-16 20:40:41',`updated_at` = '2018-12-16 20:40:41' WHERE `currencies`.`id` = 19;
UPDATE `currencies` SET `id` = 20,`name` = 'Brazilian Real',`code` = 'BRL',`symbol` = 'R$',`html_entity` = 'R$',`created_at` = '2018-12-16 20:40:41',`updated_at` = '2018-12-16 20:40:41' WHERE `currencies`.`id` = 20;
UPDATE `currencies` SET `id` = 21,`name` = 'Bahamian Dollar',`code` = 'BSD',`symbol` = '$',`html_entity` = '$',`created_at` = '2018-12-16 20:40:41',`updated_at` = '2018-12-16 20:40:41' WHERE `currencies`.`id` = 21;
UPDATE `currencies` SET `id` = 22,`name` = 'Bhutanese Ngultrum',`code` = 'BTN',`symbol` = 'Nu.',`html_entity` = '',`created_at` = '2018-12-16 20:40:41',`updated_at` = '2018-12-16 20:40:41' WHERE `currencies`.`id` = 22;
UPDATE `currencies` SET `id` = 23,`name` = 'Botswana Pula',`code` = 'BWP',`symbol` = 'P',`html_entity` = '',`created_at` = '2018-12-16 20:40:41',`updated_at` = '2018-12-16 20:40:41' WHERE `currencies`.`id` = 23;
UPDATE `currencies` SET `id` = 24,`name` = 'Belarusian Ruble',`code` = 'BYR',`symbol` = 'Br',`html_entity` = '',`created_at` = '2018-12-16 20:40:41',`updated_at` = '2018-12-16 20:40:41' WHERE `currencies`.`id` = 24;
UPDATE `currencies` SET `id` = 25,`name` = 'Belize Dollar',`code` = 'BZD',`symbol` = '$',`html_entity` = '$',`created_at` = '2018-12-16 20:40:41',`updated_at` = '2018-12-16 20:40:41' WHERE `currencies`.`id` = 25;
UPDATE `currencies` SET `id` = 26,`name` = 'Canadian Dollar',`code` = 'CAD',`symbol` = '$',`html_entity` = '$',`created_at` = '2018-12-16 20:40:41',`updated_at` = '2018-12-16 20:40:41' WHERE `currencies`.`id` = 26;
UPDATE `currencies` SET `id` = 27,`name` = 'Congolese Franc',`code` = 'CDF',`symbol` = 'Fr',`html_entity` = '',`created_at` = '2018-12-16 20:40:41',`updated_at` = '2018-12-16 20:40:41' WHERE `currencies`.`id` = 27;
UPDATE `currencies` SET `id` = 28,`name` = 'Swiss Franc',`code` = 'CHF',`symbol` = 'Fr',`html_entity` = '',`created_at` = '2018-12-16 20:40:41',`updated_at` = '2018-12-16 20:40:41' WHERE `currencies`.`id` = 28;
UPDATE `currencies` SET `id` = 29,`name` = 'Unidad de Fomento',`code` = 'CLF',`symbol` = 'UF',`html_entity` = '&#x20B1;',`created_at` = '2018-12-16 20:40:42',`updated_at` = '2018-12-16 20:40:42' WHERE `currencies`.`id` = 29;
UPDATE `currencies` SET `id` = 30,`name` = 'Chilean Peso',`code` = 'CLP',`symbol` = '$',`html_entity` = '&#36;',`created_at` = '2018-12-16 20:40:42',`updated_at` = '2018-12-16 20:40:42' WHERE `currencies`.`id` = 30;
UPDATE `currencies` SET `id` = 31,`name` = 'Chinese Renminbi Yuan',`code` = 'CNY',`symbol` = '¥',`html_entity` = '&#20803;',`created_at` = '2018-12-16 20:40:42',`updated_at` = '2018-12-16 20:40:42' WHERE `currencies`.`id` = 31;
UPDATE `currencies` SET `id` = 32,`name` = 'Colombian Peso',`code` = 'COP',`symbol` = '$',`html_entity` = '&#x20B1;',`created_at` = '2018-12-16 20:40:42',`updated_at` = '2018-12-16 20:40:42' WHERE `currencies`.`id` = 32;
UPDATE `currencies` SET `id` = 33,`name` = 'Costa Rican Colón',`code` = 'CRC',`symbol` = '₡',`html_entity` = '&#x20A1;',`created_at` = '2018-12-16 20:40:42',`updated_at` = '2018-12-16 20:40:42' WHERE `currencies`.`id` = 33;
UPDATE `currencies` SET `id` = 34,`name` = 'Cuban Convertible Peso',`code` = 'CUC',`symbol` = '$',`html_entity` = '',`created_at` = '2018-12-16 20:40:42',`updated_at` = '2018-12-16 20:40:42' WHERE `currencies`.`id` = 34;
UPDATE `currencies` SET `id` = 35,`name` = 'Cuban Peso',`code` = 'CUP',`symbol` = '$',`html_entity` = '&#x20B1;',`created_at` = '2018-12-16 20:40:42',`updated_at` = '2018-12-16 20:40:42' WHERE `currencies`.`id` = 35;
UPDATE `currencies` SET `id` = 36,`name` = 'Cape Verdean Escudo',`code` = 'CVE',`symbol` = '$',`html_entity` = '',`created_at` = '2018-12-16 20:40:42',`updated_at` = '2018-12-16 20:40:42' WHERE `currencies`.`id` = 36;
UPDATE `currencies` SET `id` = 37,`name` = 'Czech Koruna',`code` = 'CZK',`symbol` = 'Kč',`html_entity` = '',`created_at` = '2018-12-16 20:40:42',`updated_at` = '2018-12-16 20:40:42' WHERE `currencies`.`id` = 37;
UPDATE `currencies` SET `id` = 38,`name` = 'Djiboutian Franc',`code` = 'DJF',`symbol` = 'Fdj',`html_entity` = '',`created_at` = '2018-12-16 20:40:42',`updated_at` = '2018-12-16 20:40:42' WHERE `currencies`.`id` = 38;
UPDATE `currencies` SET `id` = 39,`name` = 'Danish Krone',`code` = 'DKK',`symbol` = 'kr',`html_entity` = '',`created_at` = '2018-12-16 20:40:42',`updated_at` = '2018-12-16 20:40:42' WHERE `currencies`.`id` = 39;
UPDATE `currencies` SET `id` = 40,`name` = 'Dominican Peso',`code` = 'DOP',`symbol` = '$',`html_entity` = '&#x20B1;',`created_at` = '2018-12-16 20:40:42',`updated_at` = '2018-12-16 20:40:42' WHERE `currencies`.`id` = 40;
UPDATE `currencies` SET `id` = 41,`name` = 'Algerian Dinar',`code` = 'DZD',`symbol` = 'د.ج',`html_entity` = '',`created_at` = '2018-12-16 20:40:42',`updated_at` = '2018-12-16 20:40:42' WHERE `currencies`.`id` = 41;
UPDATE `currencies` SET `id` = 42,`name` = 'Egyptian Pound',`code` = 'EGP',`symbol` = 'ج.م',`html_entity` = '&#x00A3;',`created_at` = '2018-12-16 20:40:42',`updated_at` = '2018-12-16 20:40:42' WHERE `currencies`.`id` = 42;
UPDATE `currencies` SET `id` = 43,`name` = 'Eritrean Nakfa',`code` = 'ERN',`symbol` = 'Nfk',`html_entity` = '',`created_at` = '2018-12-16 20:40:42',`updated_at` = '2018-12-16 20:40:42' WHERE `currencies`.`id` = 43;
UPDATE `currencies` SET `id` = 44,`name` = 'Ethiopian Birr',`code` = 'ETB',`symbol` = 'Br',`html_entity` = '',`created_at` = '2018-12-16 20:40:43',`updated_at` = '2018-12-16 20:40:43' WHERE `currencies`.`id` = 44;
UPDATE `currencies` SET `id` = 45,`name` = 'Euro',`code` = 'EUR',`symbol` = '€',`html_entity` = '&#x20AC;',`created_at` = '2018-12-16 20:40:43',`updated_at` = '2018-12-16 20:40:43' WHERE `currencies`.`id` = 45;
UPDATE `currencies` SET `id` = 46,`name` = 'Fijian Dollar',`code` = 'FJD',`symbol` = '$',`html_entity` = '$',`created_at` = '2018-12-16 20:40:43',`updated_at` = '2018-12-16 20:40:43' WHERE `currencies`.`id` = 46;
UPDATE `currencies` SET `id` = 47,`name` = 'Falkland Pound',`code` = 'FKP',`symbol` = '£',`html_entity` = '&#x00A3;',`created_at` = '2018-12-16 20:40:43',`updated_at` = '2018-12-16 20:40:43' WHERE `currencies`.`id` = 47;
UPDATE `currencies` SET `id` = 48,`name` = 'British Pound',`code` = 'GBP',`symbol` = '£',`html_entity` = '&#x00A3;',`created_at` = '2018-12-16 20:40:43',`updated_at` = '2018-12-16 20:40:43' WHERE `currencies`.`id` = 48;
UPDATE `currencies` SET `id` = 49,`name` = 'Georgian Lari',`code` = 'GEL',`symbol` = 'ლ',`html_entity` = '',`created_at` = '2018-12-16 20:40:43',`updated_at` = '2018-12-16 20:40:43' WHERE `currencies`.`id` = 49;
UPDATE `currencies` SET `id` = 50,`name` = 'Ghanaian Cedi',`code` = 'GHS',`symbol` = '₵',`html_entity` = '&#x20B5;',`created_at` = '2018-12-16 20:40:43',`updated_at` = '2018-12-16 20:40:43' WHERE `currencies`.`id` = 50;
UPDATE `currencies` SET `id` = 51,`name` = 'Gibraltar Pound',`code` = 'GIP',`symbol` = '£',`html_entity` = '&#x00A3;',`created_at` = '2018-12-16 20:40:43',`updated_at` = '2018-12-16 20:40:43' WHERE `currencies`.`id` = 51;
UPDATE `currencies` SET `id` = 52,`name` = 'Gambian Dalasi',`code` = 'GMD',`symbol` = 'D',`html_entity` = '',`created_at` = '2018-12-16 20:40:43',`updated_at` = '2018-12-16 20:40:43' WHERE `currencies`.`id` = 52;
UPDATE `currencies` SET `id` = 53,`name` = 'Guinean Franc',`code` = 'GNF',`symbol` = 'Fr',`html_entity` = '',`created_at` = '2018-12-16 20:40:43',`updated_at` = '2018-12-16 20:40:43' WHERE `currencies`.`id` = 53;
UPDATE `currencies` SET `id` = 54,`name` = 'Guatemalan Quetzal',`code` = 'GTQ',`symbol` = 'Q',`html_entity` = '',`created_at` = '2018-12-16 20:40:43',`updated_at` = '2018-12-16 20:40:43' WHERE `currencies`.`id` = 54;
UPDATE `currencies` SET `id` = 55,`name` = 'Guyanese Dollar',`code` = 'GYD',`symbol` = '$',`html_entity` = '$',`created_at` = '2018-12-16 20:40:43',`updated_at` = '2018-12-16 20:40:43' WHERE `currencies`.`id` = 55;
UPDATE `currencies` SET `id` = 56,`name` = 'Hong Kong Dollar',`code` = 'HKD',`symbol` = '$',`html_entity` = '$',`created_at` = '2018-12-16 20:40:43',`updated_at` = '2018-12-16 20:40:43' WHERE `currencies`.`id` = 56;
UPDATE `currencies` SET `id` = 57,`name` = 'Honduran Lempira',`code` = 'HNL',`symbol` = 'L',`html_entity` = '',`created_at` = '2018-12-16 20:40:43',`updated_at` = '2018-12-16 20:40:43' WHERE `currencies`.`id` = 57;
UPDATE `currencies` SET `id` = 58,`name` = 'Croatian Kuna',`code` = 'HRK',`symbol` = 'kn',`html_entity` = '',`created_at` = '2018-12-16 20:40:43',`updated_at` = '2018-12-16 20:40:43' WHERE `currencies`.`id` = 58;
UPDATE `currencies` SET `id` = 59,`name` = 'Haitian Gourde',`code` = 'HTG',`symbol` = 'G',`html_entity` = '',`created_at` = '2018-12-16 20:40:44',`updated_at` = '2018-12-16 20:40:44' WHERE `currencies`.`id` = 59;
UPDATE `currencies` SET `id` = 60,`name` = 'Hungarian Forint',`code` = 'HUF',`symbol` = 'Ft',`html_entity` = '',`created_at` = '2018-12-16 20:40:44',`updated_at` = '2018-12-16 20:40:44' WHERE `currencies`.`id` = 60;
UPDATE `currencies` SET `id` = 61,`name` = 'Indonesian Rupiah',`code` = 'IDR',`symbol` = 'Rp',`html_entity` = '',`created_at` = '2018-12-16 20:40:44',`updated_at` = '2018-12-16 20:40:44' WHERE `currencies`.`id` = 61;
UPDATE `currencies` SET `id` = 62,`name` = 'Israeli New Sheqel',`code` = 'ILS',`symbol` = '₪',`html_entity` = '&#x20AA;',`created_at` = '2018-12-16 20:40:44',`updated_at` = '2018-12-16 20:40:44' WHERE `currencies`.`id` = 62;
UPDATE `currencies` SET `id` = 63,`name` = 'Indian Rupee',`code` = 'INR',`symbol` = '₹',`html_entity` = '&#x20b9;',`created_at` = '2018-12-16 20:40:44',`updated_at` = '2018-12-16 20:40:44' WHERE `currencies`.`id` = 63;
UPDATE `currencies` SET `id` = 64,`name` = 'Iraqi Dinar',`code` = 'IQD',`symbol` = 'ع.د',`html_entity` = '',`created_at` = '2018-12-16 20:40:44',`updated_at` = '2018-12-16 20:40:44' WHERE `currencies`.`id` = 64;
UPDATE `currencies` SET `id` = 65,`name` = 'Iranian Rial',`code` = 'IRR',`symbol` = '﷼',`html_entity` = '&#xFDFC;',`created_at` = '2018-12-16 20:40:44',`updated_at` = '2018-12-16 20:40:44' WHERE `currencies`.`id` = 65;
UPDATE `currencies` SET `id` = 66,`name` = 'Icelandic Króna',`code` = 'ISK',`symbol` = 'kr',`html_entity` = '',`created_at` = '2018-12-16 20:40:44',`updated_at` = '2018-12-16 20:40:44' WHERE `currencies`.`id` = 66;
UPDATE `currencies` SET `id` = 67,`name` = 'Jamaican Dollar',`code` = 'JMD',`symbol` = '$',`html_entity` = '$',`created_at` = '2018-12-16 20:40:44',`updated_at` = '2018-12-16 20:40:44' WHERE `currencies`.`id` = 67;
UPDATE `currencies` SET `id` = 68,`name` = 'Jordanian Dinar',`code` = 'JOD',`symbol` = 'د.ا',`html_entity` = '',`created_at` = '2018-12-16 20:40:44',`updated_at` = '2018-12-16 20:40:44' WHERE `currencies`.`id` = 68;
UPDATE `currencies` SET `id` = 69,`name` = 'Japanese Yen',`code` = 'JPY',`symbol` = '¥',`html_entity` = '&#x00A5;',`created_at` = '2018-12-16 20:40:44',`updated_at` = '2018-12-16 20:40:44' WHERE `currencies`.`id` = 69;
UPDATE `currencies` SET `id` = 70,`name` = 'Kenyan Shilling',`code` = 'KES',`symbol` = 'KSh',`html_entity` = '',`created_at` = '2018-12-16 20:40:44',`updated_at` = '2018-12-16 20:40:44' WHERE `currencies`.`id` = 70;
UPDATE `currencies` SET `id` = 71,`name` = 'Kyrgyzstani Som',`code` = 'KGS',`symbol` = 'som',`html_entity` = '',`created_at` = '2018-12-16 20:40:45',`updated_at` = '2018-12-16 20:40:45' WHERE `currencies`.`id` = 71;
UPDATE `currencies` SET `id` = 72,`name` = 'Cambodian Riel',`code` = 'KHR',`symbol` = '៛',`html_entity` = '&#x17DB;',`created_at` = '2018-12-16 20:40:45',`updated_at` = '2018-12-16 20:40:45' WHERE `currencies`.`id` = 72;
UPDATE `currencies` SET `id` = 73,`name` = 'Comorian Franc',`code` = 'KMF',`symbol` = 'Fr',`html_entity` = '',`created_at` = '2018-12-16 20:40:45',`updated_at` = '2018-12-16 20:40:45' WHERE `currencies`.`id` = 73;
UPDATE `currencies` SET `id` = 74,`name` = 'North Korean Won',`code` = 'KPW',`symbol` = '₩',`html_entity` = '&#x20A9;',`created_at` = '2018-12-16 20:40:45',`updated_at` = '2018-12-16 20:40:45' WHERE `currencies`.`id` = 74;
UPDATE `currencies` SET `id` = 75,`name` = 'South Korean Won',`code` = 'KRW',`symbol` = '₩',`html_entity` = '&#x20A9;',`created_at` = '2018-12-16 20:40:45',`updated_at` = '2018-12-16 20:40:45' WHERE `currencies`.`id` = 75;
UPDATE `currencies` SET `id` = 76,`name` = 'Kuwaiti Dinar',`code` = 'KWD',`symbol` = 'د.ك',`html_entity` = '',`created_at` = '2018-12-16 20:40:46',`updated_at` = '2018-12-16 20:40:46' WHERE `currencies`.`id` = 76;
UPDATE `currencies` SET `id` = 77,`name` = 'Cayman Islands Dollar',`code` = 'KYD',`symbol` = '$',`html_entity` = '$',`created_at` = '2018-12-16 20:40:46',`updated_at` = '2018-12-16 20:40:46' WHERE `currencies`.`id` = 77;
UPDATE `currencies` SET `id` = 78,`name` = 'Kazakhstani Tenge',`code` = 'KZT',`symbol` = '〒',`html_entity` = '',`created_at` = '2018-12-16 20:40:46',`updated_at` = '2018-12-16 20:40:46' WHERE `currencies`.`id` = 78;
UPDATE `currencies` SET `id` = 79,`name` = 'Lao Kip',`code` = 'LAK',`symbol` = '₭',`html_entity` = '&#x20AD;',`created_at` = '2018-12-16 20:40:46',`updated_at` = '2018-12-16 20:40:46' WHERE `currencies`.`id` = 79;
UPDATE `currencies` SET `id` = 80,`name` = 'Lebanese Pound',`code` = 'LBP',`symbol` = 'ل.ل',`html_entity` = '&#x00A3;',`created_at` = '2018-12-16 20:40:46',`updated_at` = '2018-12-16 20:40:46' WHERE `currencies`.`id` = 80;
UPDATE `currencies` SET `id` = 81,`name` = 'Sri Lankan Rupee',`code` = 'LKR',`symbol` = '₨',`html_entity` = '&#x0BF9;',`created_at` = '2018-12-16 20:40:46',`updated_at` = '2018-12-16 20:40:46' WHERE `currencies`.`id` = 81;
UPDATE `currencies` SET `id` = 82,`name` = 'Liberian Dollar',`code` = 'LRD',`symbol` = '$',`html_entity` = '$',`created_at` = '2018-12-16 20:40:46',`updated_at` = '2018-12-16 20:40:46' WHERE `currencies`.`id` = 82;
UPDATE `currencies` SET `id` = 83,`name` = 'Lesotho Loti',`code` = 'LSL',`symbol` = 'L',`html_entity` = '',`created_at` = '2018-12-16 20:40:46',`updated_at` = '2018-12-16 20:40:46' WHERE `currencies`.`id` = 83;
UPDATE `currencies` SET `id` = 84,`name` = 'Lithuanian Litas',`code` = 'LTL',`symbol` = 'Lt',`html_entity` = '',`created_at` = '2018-12-16 20:40:46',`updated_at` = '2018-12-16 20:40:46' WHERE `currencies`.`id` = 84;
UPDATE `currencies` SET `id` = 85,`name` = 'Latvian Lats',`code` = 'LVL',`symbol` = 'Ls',`html_entity` = '',`created_at` = '2018-12-16 20:40:46',`updated_at` = '2018-12-16 20:40:46' WHERE `currencies`.`id` = 85;
UPDATE `currencies` SET `id` = 86,`name` = 'Libyan Dinar',`code` = 'LYD',`symbol` = 'ل.د',`html_entity` = '',`created_at` = '2018-12-16 20:40:47',`updated_at` = '2018-12-16 20:40:47' WHERE `currencies`.`id` = 86;
UPDATE `currencies` SET `id` = 87,`name` = 'Moroccan Dirham',`code` = 'MAD',`symbol` = 'د.م.',`html_entity` = '',`created_at` = '2018-12-16 20:40:47',`updated_at` = '2018-12-16 20:40:47' WHERE `currencies`.`id` = 87;
UPDATE `currencies` SET `id` = 88,`name` = 'Moldovan Leu',`code` = 'MDL',`symbol` = 'L',`html_entity` = '',`created_at` = '2018-12-16 20:40:47',`updated_at` = '2018-12-16 20:40:47' WHERE `currencies`.`id` = 88;
UPDATE `currencies` SET `id` = 89,`name` = 'Malagasy Ariary',`code` = 'MGA',`symbol` = 'Ar',`html_entity` = '',`created_at` = '2018-12-16 20:40:47',`updated_at` = '2018-12-16 20:40:47' WHERE `currencies`.`id` = 89;
UPDATE `currencies` SET `id` = 90,`name` = 'Macedonian Denar',`code` = 'MKD',`symbol` = 'ден',`html_entity` = '',`created_at` = '2018-12-16 20:40:47',`updated_at` = '2018-12-16 20:40:47' WHERE `currencies`.`id` = 90;
UPDATE `currencies` SET `id` = 91,`name` = 'Myanmar Kyat',`code` = 'MMK',`symbol` = 'K',`html_entity` = '',`created_at` = '2018-12-16 20:40:48',`updated_at` = '2018-12-16 20:40:48' WHERE `currencies`.`id` = 91;
UPDATE `currencies` SET `id` = 92,`name` = 'Mongolian Tögrög',`code` = 'MNT',`symbol` = '₮',`html_entity` = '&#x20AE;',`created_at` = '2018-12-16 20:40:48',`updated_at` = '2018-12-16 20:40:48' WHERE `currencies`.`id` = 92;
UPDATE `currencies` SET `id` = 93,`name` = 'Macanese Pataca',`code` = 'MOP',`symbol` = 'P',`html_entity` = '',`created_at` = '2018-12-16 20:40:48',`updated_at` = '2018-12-16 20:40:48' WHERE `currencies`.`id` = 93;
UPDATE `currencies` SET `id` = 94,`name` = 'Mauritanian Ouguiya',`code` = 'MRO',`symbol` = 'UM',`html_entity` = '',`created_at` = '2018-12-16 20:40:48',`updated_at` = '2018-12-16 20:40:48' WHERE `currencies`.`id` = 94;
UPDATE `currencies` SET `id` = 95,`name` = 'Mauritian Rupee',`code` = 'MUR',`symbol` = '₨',`html_entity` = '&#x20A8;',`created_at` = '2018-12-16 20:40:48',`updated_at` = '2018-12-16 20:40:48' WHERE `currencies`.`id` = 95;
UPDATE `currencies` SET `id` = 96,`name` = 'Maldivian Rufiyaa',`code` = 'MVR',`symbol` = 'MVR',`html_entity` = '',`created_at` = '2018-12-16 20:40:49',`updated_at` = '2018-12-16 20:40:49' WHERE `currencies`.`id` = 96;
UPDATE `currencies` SET `id` = 97,`name` = 'Malawian Kwacha',`code` = 'MWK',`symbol` = 'MK',`html_entity` = '',`created_at` = '2018-12-16 20:40:49',`updated_at` = '2018-12-16 20:40:49' WHERE `currencies`.`id` = 97;
UPDATE `currencies` SET `id` = 98,`name` = 'Mexican Peso',`code` = 'MXN',`symbol` = '$',`html_entity` = '$',`created_at` = '2018-12-16 20:40:49',`updated_at` = '2018-12-16 20:40:49' WHERE `currencies`.`id` = 98;
UPDATE `currencies` SET `id` = 99,`name` = 'Malaysian Ringgit',`code` = 'MYR',`symbol` = 'RM',`html_entity` = '',`created_at` = '2018-12-16 20:40:49',`updated_at` = '2018-12-16 20:40:49' WHERE `currencies`.`id` = 99;
UPDATE `currencies` SET `id` = 100,`name` = 'Mozambican Metical',`code` = 'MZN',`symbol` = 'MTn',`html_entity` = '',`created_at` = '2018-12-16 20:40:49',`updated_at` = '2018-12-16 20:40:49' WHERE `currencies`.`id` = 100;
UPDATE `currencies` SET `id` = 101,`name` = 'Namibian Dollar',`code` = 'NAD',`symbol` = '$',`html_entity` = '$',`created_at` = '2018-12-16 20:40:50',`updated_at` = '2018-12-16 20:40:50' WHERE `currencies`.`id` = 101;
UPDATE `currencies` SET `id` = 102,`name` = 'Nigerian Naira',`code` = 'NGN',`symbol` = '₦',`html_entity` = '&#x20A6;',`created_at` = '2018-12-16 20:40:50',`updated_at` = '2018-12-16 20:40:50' WHERE `currencies`.`id` = 102;
UPDATE `currencies` SET `id` = 103,`name` = 'Nicaraguan Córdoba',`code` = 'NIO',`symbol` = 'C$',`html_entity` = '',`created_at` = '2018-12-16 20:40:50',`updated_at` = '2018-12-16 20:40:50' WHERE `currencies`.`id` = 103;
UPDATE `currencies` SET `id` = 104,`name` = 'Norwegian Krone',`code` = 'NOK',`symbol` = 'kr',`html_entity` = 'kr',`created_at` = '2018-12-16 20:40:50',`updated_at` = '2018-12-16 20:40:50' WHERE `currencies`.`id` = 104;
UPDATE `currencies` SET `id` = 105,`name` = 'Nepalese Rupee',`code` = 'NPR',`symbol` = '₨',`html_entity` = '&#x20A8;',`created_at` = '2018-12-16 20:40:51',`updated_at` = '2018-12-16 20:40:51' WHERE `currencies`.`id` = 105;
UPDATE `currencies` SET `id` = 106,`name` = 'New Zealand Dollar',`code` = 'NZD',`symbol` = '$',`html_entity` = '$',`created_at` = '2018-12-16 20:40:51',`updated_at` = '2018-12-16 20:40:51' WHERE `currencies`.`id` = 106;
UPDATE `currencies` SET `id` = 107,`name` = 'Omani Rial',`code` = 'OMR',`symbol` = 'ر.ع.',`html_entity` = '&#xFDFC;',`created_at` = '2018-12-16 20:40:52',`updated_at` = '2018-12-16 20:40:52' WHERE `currencies`.`id` = 107;
UPDATE `currencies` SET `id` = 108,`name` = 'Panamanian Balboa',`code` = 'PAB',`symbol` = 'B/.',`html_entity` = '',`created_at` = '2018-12-16 20:40:52',`updated_at` = '2018-12-16 20:40:52' WHERE `currencies`.`id` = 108;
UPDATE `currencies` SET `id` = 109,`name` = 'Peruvian Nuevo Sol',`code` = 'PEN',`symbol` = 'S/.',`html_entity` = 'S/.',`created_at` = '2018-12-16 20:40:52',`updated_at` = '2018-12-16 20:40:52' WHERE `currencies`.`id` = 109;
UPDATE `currencies` SET `id` = 110,`name` = 'Papua New Guinean Kina',`code` = 'PGK',`symbol` = 'K',`html_entity` = '',`created_at` = '2018-12-16 20:40:52',`updated_at` = '2018-12-16 20:40:52' WHERE `currencies`.`id` = 110;
UPDATE `currencies` SET `id` = 111,`name` = 'Philippine Peso',`code` = 'PHP',`symbol` = '₱',`html_entity` = '&#x20B1;',`created_at` = '2018-12-16 20:40:52',`updated_at` = '2018-12-16 20:40:52' WHERE `currencies`.`id` = 111;
UPDATE `currencies` SET `id` = 112,`name` = 'Pakistani Rupee',`code` = 'PKR',`symbol` = '₨',`html_entity` = '&#x20A8;',`created_at` = '2018-12-16 20:40:53',`updated_at` = '2018-12-16 20:40:53' WHERE `currencies`.`id` = 112;
UPDATE `currencies` SET `id` = 113,`name` = 'Polish Złoty',`code` = 'PLN',`symbol` = 'zł',`html_entity` = 'z&#322;',`created_at` = '2018-12-16 20:40:53',`updated_at` = '2018-12-16 20:40:53' WHERE `currencies`.`id` = 113;
UPDATE `currencies` SET `id` = 114,`name` = 'Paraguayan Guaraní',`code` = 'PYG',`symbol` = '₲',`html_entity` = '&#x20B2;',`created_at` = '2018-12-16 20:40:53',`updated_at` = '2018-12-16 20:40:53' WHERE `currencies`.`id` = 114;
UPDATE `currencies` SET `id` = 115,`name` = 'Qatari Riyal',`code` = 'QAR',`symbol` = 'ر.ق',`html_entity` = '&#xFDFC;',`created_at` = '2018-12-16 20:40:53',`updated_at` = '2018-12-16 20:40:53' WHERE `currencies`.`id` = 115;
UPDATE `currencies` SET `id` = 116,`name` = 'Romanian Leu',`code` = 'RON',`symbol` = 'Lei',`html_entity` = '',`created_at` = '2018-12-16 20:40:53',`updated_at` = '2018-12-16 20:40:53' WHERE `currencies`.`id` = 116;
UPDATE `currencies` SET `id` = 117,`name` = 'Serbian Dinar',`code` = 'RSD',`symbol` = 'РСД',`html_entity` = '',`created_at` = '2018-12-16 20:40:53',`updated_at` = '2018-12-16 20:40:53' WHERE `currencies`.`id` = 117;
UPDATE `currencies` SET `id` = 118,`name` = 'Russian Ruble',`code` = 'RUB',`symbol` = 'р.',`html_entity` = '&#x0440;&#x0443;&#x0431;',`created_at` = '2018-12-16 20:40:53',`updated_at` = '2018-12-16 20:40:53' WHERE `currencies`.`id` = 118;
UPDATE `currencies` SET `id` = 119,`name` = 'Rwandan Franc',`code` = 'RWF',`symbol` = 'FRw',`html_entity` = '',`created_at` = '2018-12-16 20:40:53',`updated_at` = '2018-12-16 20:40:53' WHERE `currencies`.`id` = 119;
UPDATE `currencies` SET `id` = 120,`name` = 'Saudi Riyal',`code` = 'SAR',`symbol` = 'ر.س',`html_entity` = '&#xFDFC;',`created_at` = '2018-12-16 20:40:54',`updated_at` = '2018-12-16 20:40:54' WHERE `currencies`.`id` = 120;
UPDATE `currencies` SET `id` = 121,`name` = 'Solomon Islands Dollar',`code` = 'SBD',`symbol` = '$',`html_entity` = '$',`created_at` = '2018-12-16 20:40:54',`updated_at` = '2018-12-16 20:40:54' WHERE `currencies`.`id` = 121;
UPDATE `currencies` SET `id` = 122,`name` = 'Seychellois Rupee',`code` = 'SCR',`symbol` = '₨',`html_entity` = '&#x20A8;',`created_at` = '2018-12-16 20:40:54',`updated_at` = '2018-12-16 20:40:54' WHERE `currencies`.`id` = 122;
UPDATE `currencies` SET `id` = 123,`name` = 'Sudanese Pound',`code` = 'SDG',`symbol` = '£',`html_entity` = '',`created_at` = '2018-12-16 20:40:54',`updated_at` = '2018-12-16 20:40:54' WHERE `currencies`.`id` = 123;
UPDATE `currencies` SET `id` = 124,`name` = 'Swedish Krona',`code` = 'SEK',`symbol` = 'kr',`html_entity` = '',`created_at` = '2018-12-16 20:40:54',`updated_at` = '2018-12-16 20:40:54' WHERE `currencies`.`id` = 124;
UPDATE `currencies` SET `id` = 125,`name` = 'Singapore Dollar',`code` = 'SGD',`symbol` = '$',`html_entity` = '$',`created_at` = '2018-12-16 20:40:54',`updated_at` = '2018-12-16 20:40:54' WHERE `currencies`.`id` = 125;
UPDATE `currencies` SET `id` = 126,`name` = 'Saint Helenian Pound',`code` = 'SHP',`symbol` = '£',`html_entity` = '&#x00A3;',`created_at` = '2018-12-16 20:40:54',`updated_at` = '2018-12-16 20:40:54' WHERE `currencies`.`id` = 126;
UPDATE `currencies` SET `id` = 127,`name` = 'Slovak Koruna',`code` = 'SKK',`symbol` = 'Sk',`html_entity` = '',`created_at` = '2018-12-16 20:40:55',`updated_at` = '2018-12-16 20:40:55' WHERE `currencies`.`id` = 127;
UPDATE `currencies` SET `id` = 128,`name` = 'Sierra Leonean Leone',`code` = 'SLL',`symbol` = 'Le',`html_entity` = '',`created_at` = '2018-12-16 20:40:55',`updated_at` = '2018-12-16 20:40:55' WHERE `currencies`.`id` = 128;
UPDATE `currencies` SET `id` = 129,`name` = 'Somali Shilling',`code` = 'SOS',`symbol` = 'Sh',`html_entity` = '',`created_at` = '2018-12-16 20:40:55',`updated_at` = '2018-12-16 20:40:55' WHERE `currencies`.`id` = 129;
UPDATE `currencies` SET `id` = 130,`name` = 'Surinamese Dollar',`code` = 'SRD',`symbol` = '$',`html_entity` = '',`created_at` = '2018-12-16 20:40:55',`updated_at` = '2018-12-16 20:40:55' WHERE `currencies`.`id` = 130;
UPDATE `currencies` SET `id` = 131,`name` = 'South Sudanese Pound',`code` = 'SSP',`symbol` = '£',`html_entity` = '&#x00A3;',`created_at` = '2018-12-16 20:40:55',`updated_at` = '2018-12-16 20:40:55' WHERE `currencies`.`id` = 131;
UPDATE `currencies` SET `id` = 132,`name` = 'São Tomé and Príncipe Dobra',`code` = 'STD',`symbol` = 'Db',`html_entity` = '',`created_at` = '2018-12-16 20:40:55',`updated_at` = '2018-12-16 20:40:55' WHERE `currencies`.`id` = 132;
UPDATE `currencies` SET `id` = 133,`name` = 'Salvadoran Colón',`code` = 'SVC',`symbol` = '₡',`html_entity` = '&#x20A1;',`created_at` = '2018-12-16 20:40:55',`updated_at` = '2018-12-16 20:40:55' WHERE `currencies`.`id` = 133;
UPDATE `currencies` SET `id` = 134,`name` = 'Syrian Pound',`code` = 'SYP',`symbol` = '£S',`html_entity` = '&#x00A3;',`created_at` = '2018-12-16 20:40:55',`updated_at` = '2018-12-16 20:40:55' WHERE `currencies`.`id` = 134;
UPDATE `currencies` SET `id` = 135,`name` = 'Swazi Lilangeni',`code` = 'SZL',`symbol` = 'L',`html_entity` = '',`created_at` = '2018-12-16 20:40:56',`updated_at` = '2018-12-16 20:40:56' WHERE `currencies`.`id` = 135;
UPDATE `currencies` SET `id` = 136,`name` = 'Thai Baht',`code` = 'THB',`symbol` = '฿',`html_entity` = '&#x0E3F;',`created_at` = '2018-12-16 20:40:56',`updated_at` = '2018-12-16 20:40:56' WHERE `currencies`.`id` = 136;
UPDATE `currencies` SET `id` = 137,`name` = 'Tajikistani Somoni',`code` = 'TJS',`symbol` = 'ЅМ',`html_entity` = '',`created_at` = '2018-12-16 20:40:56',`updated_at` = '2018-12-16 20:40:56' WHERE `currencies`.`id` = 137;
UPDATE `currencies` SET `id` = 138,`name` = 'Turkmenistani Manat',`code` = 'TMT',`symbol` = 'T',`html_entity` = '',`created_at` = '2018-12-16 20:40:56',`updated_at` = '2018-12-16 20:40:56' WHERE `currencies`.`id` = 138;
UPDATE `currencies` SET `id` = 139,`name` = 'Tunisian Dinar',`code` = 'TND',`symbol` = 'د.ت',`html_entity` = '',`created_at` = '2018-12-16 20:40:56',`updated_at` = '2018-12-16 20:40:56' WHERE `currencies`.`id` = 139;
UPDATE `currencies` SET `id` = 140,`name` = 'Tongan Paʻanga',`code` = 'TOP',`symbol` = 'T$',`html_entity` = '',`created_at` = '2018-12-16 20:40:56',`updated_at` = '2018-12-16 20:40:56' WHERE `currencies`.`id` = 140;
UPDATE `currencies` SET `id` = 141,`name` = 'Turkish Lira',`code` = 'TRY',`symbol` = 'TL',`html_entity` = '',`created_at` = '2018-12-16 20:40:56',`updated_at` = '2018-12-16 20:40:56' WHERE `currencies`.`id` = 141;
UPDATE `currencies` SET `id` = 142,`name` = 'Trinidad and Tobago Dollar',`code` = 'TTD',`symbol` = '$',`html_entity` = '$',`created_at` = '2018-12-16 20:40:56',`updated_at` = '2018-12-16 20:40:56' WHERE `currencies`.`id` = 142;
UPDATE `currencies` SET `id` = 143,`name` = 'New Taiwan Dollar',`code` = 'TWD',`symbol` = '$',`html_entity` = '$',`created_at` = '2018-12-16 20:40:56',`updated_at` = '2018-12-16 20:40:56' WHERE `currencies`.`id` = 143;
UPDATE `currencies` SET `id` = 144,`name` = 'Tanzanian Shilling',`code` = 'TZS',`symbol` = 'Sh',`html_entity` = '',`created_at` = '2018-12-16 20:40:57',`updated_at` = '2018-12-16 20:40:57' WHERE `currencies`.`id` = 144;
UPDATE `currencies` SET `id` = 145,`name` = 'Ukrainian Hryvnia',`code` = 'UAH',`symbol` = '₴',`html_entity` = '&#x20B4;',`created_at` = '2018-12-16 20:40:57',`updated_at` = '2018-12-16 20:40:57' WHERE `currencies`.`id` = 145;
UPDATE `currencies` SET `id` = 146,`name` = 'Ugandan Shilling',`code` = 'UGX',`symbol` = 'USh',`html_entity` = '',`created_at` = '2018-12-16 20:40:57',`updated_at` = '2018-12-16 20:40:57' WHERE `currencies`.`id` = 146;
UPDATE `currencies` SET `id` = 147,`name` = 'United States Dollar',`code` = 'USD',`symbol` = '$',`html_entity` = '$',`created_at` = '2018-12-16 20:40:57',`updated_at` = '2018-12-16 20:40:57' WHERE `currencies`.`id` = 147;
UPDATE `currencies` SET `id` = 148,`name` = 'Uruguayan Peso',`code` = 'UYU',`symbol` = '$',`html_entity` = '&#x20B1;',`created_at` = '2018-12-16 20:40:57',`updated_at` = '2018-12-16 20:40:57' WHERE `currencies`.`id` = 148;
UPDATE `currencies` SET `id` = 149,`name` = 'Uzbekistani Som',`code` = 'UZS',`symbol` = 'null',`html_entity` = '',`created_at` = '2018-12-16 20:40:57',`updated_at` = '2018-12-16 20:40:57' WHERE `currencies`.`id` = 149;
UPDATE `currencies` SET `id` = 150,`name` = 'Venezuelan Bolívar',`code` = 'VEF',`symbol` = 'Bs F',`html_entity` = '',`created_at` = '2018-12-16 20:40:57',`updated_at` = '2018-12-16 20:40:57' WHERE `currencies`.`id` = 150;
UPDATE `currencies` SET `id` = 151,`name` = 'Vietnamese Đồng',`code` = 'VND',`symbol` = '₫',`html_entity` = '&#x20AB;',`created_at` = '2018-12-16 20:40:57',`updated_at` = '2018-12-16 20:40:57' WHERE `currencies`.`id` = 151;
UPDATE `currencies` SET `id` = 152,`name` = 'Vanuatu Vatu',`code` = 'VUV',`symbol` = 'Vt',`html_entity` = '',`created_at` = '2018-12-16 20:40:57',`updated_at` = '2018-12-16 20:40:57' WHERE `currencies`.`id` = 152;
UPDATE `currencies` SET `id` = 153,`name` = 'Samoan Tala',`code` = 'WST',`symbol` = 'T',`html_entity` = '',`created_at` = '2018-12-16 20:40:58',`updated_at` = '2018-12-16 20:40:58' WHERE `currencies`.`id` = 153;
UPDATE `currencies` SET `id` = 154,`name` = 'Central African Cfa Franc',`code` = 'XAF',`symbol` = 'Fr',`html_entity` = '',`created_at` = '2018-12-16 20:40:58',`updated_at` = '2018-12-16 20:40:58' WHERE `currencies`.`id` = 154;
UPDATE `currencies` SET `id` = 155,`name` = 'Silver (Troy Ounce)',`code` = 'XAG',`symbol` = 'oz t',`html_entity` = '',`created_at` = '2018-12-16 20:40:58',`updated_at` = '2018-12-16 20:40:58' WHERE `currencies`.`id` = 155;
UPDATE `currencies` SET `id` = 156,`name` = 'Gold (Troy Ounce)',`code` = 'XAU',`symbol` = 'oz t',`html_entity` = '',`created_at` = '2018-12-16 20:40:58',`updated_at` = '2018-12-16 20:40:58' WHERE `currencies`.`id` = 156;
UPDATE `currencies` SET `id` = 157,`name` = 'East Caribbean Dollar',`code` = 'XCD',`symbol` = '$',`html_entity` = '$',`created_at` = '2018-12-16 20:40:58',`updated_at` = '2018-12-16 20:40:58' WHERE `currencies`.`id` = 157;
UPDATE `currencies` SET `id` = 158,`name` = 'Special Drawing Rights',`code` = 'XDR',`symbol` = 'SDR',`html_entity` = '$',`created_at` = '2018-12-16 20:40:58',`updated_at` = '2018-12-16 20:40:58' WHERE `currencies`.`id` = 158;
UPDATE `currencies` SET `id` = 159,`name` = 'West African Cfa Franc',`code` = 'XOF',`symbol` = 'Fr',`html_entity` = '',`created_at` = '2018-12-16 20:40:59',`updated_at` = '2018-12-16 20:40:59' WHERE `currencies`.`id` = 159;
UPDATE `currencies` SET `id` = 160,`name` = 'Cfp Franc',`code` = 'XPF',`symbol` = 'Fr',`html_entity` = '',`created_at` = '2018-12-16 20:40:59',`updated_at` = '2018-12-16 20:40:59' WHERE `currencies`.`id` = 160;
UPDATE `currencies` SET `id` = 161,`name` = 'Yemeni Rial',`code` = 'YER',`symbol` = '﷼',`html_entity` = '&#xFDFC;',`created_at` = '2018-12-16 20:40:59',`updated_at` = '2018-12-16 20:40:59' WHERE `currencies`.`id` = 161;
UPDATE `currencies` SET `id` = 162,`name` = 'South African Rand',`code` = 'ZAR',`symbol` = 'R',`html_entity` = '&#x0052;',`created_at` = '2018-12-16 20:40:59',`updated_at` = '2018-12-16 20:40:59' WHERE `currencies`.`id` = 162;
UPDATE `currencies` SET `id` = 163,`name` = 'Zambian Kwacha',`code` = 'ZMK',`symbol` = 'ZK',`html_entity` = '',`created_at` = '2018-12-16 20:40:59',`updated_at` = '2018-12-16 20:40:59' WHERE `currencies`.`id` = 163;
UPDATE `currencies` SET `id` = 164,`name` = 'Zambian Kwacha',`code` = 'ZMW',`symbol` = 'ZK',`html_entity` = '',`created_at` = '2018-12-16 20:40:59',`updated_at` = '2018-12-16 20:40:59' WHERE `currencies`.`id` = 164;

-- --------------------------------------------------------

--
-- Table structure for table `gateways`
--

CREATE TABLE IF NOT EXISTS `gateways` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `bankwire_active` tinyint(1) DEFAULT '1',
  `paypal_active` tinyint(1) DEFAULT '1',
  `paypal_client_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paypal_client_secret` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stripe_active` tinyint(1) DEFAULT '1',
  `stripe_publishable_key` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stripe_secret_key` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `voguepay_active` tinyint(1) DEFAULT '1',
  `voguepay_merchant_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gateways`
--

UPDATE `gateways` SET `id` = 1,`bankwire_active` = 1,`paypal_active` = 1,`paypal_client_id` = 'AXb8idM5vv4RPIgVi5GD6bEcEzZg7T11xCiclUfGslJVMbBKvm-U40zikgZNRXEdYdXliBzsZrJCBDJ8',`paypal_client_secret` = 'EOtz3yaEggAFcW8gHeBK1CBCmXZVkwQaR9KmitRxHwq0MHfkFDF2A7u27DYMMugcCA-w9fvkXJNmt3bX',`stripe_active` = 1,`stripe_publishable_key` = 'pk_test_qKe8nGFSUZkLRt0ETMieMh80',`stripe_secret_key` = 'sk_test_ySExMEvYiX71wvqDmLAMu1UC',`voguepay_active` = 1,`voguepay_merchant_id` = 'demo',`created_at` = '2018-12-16 20:40:59',`updated_at` = '2018-12-16 20:40:59' WHERE `gateways`.`id` = 1;

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE IF NOT EXISTS `invoices` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `package_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `invoice_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_method` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Nil',
  `rate_per_click` decimal(9,2) NOT NULL DEFAULT '1.00',
  `amount` decimal(9,2) NOT NULL DEFAULT '0.00',
  `amount_raw` decimal(9,2) NOT NULL DEFAULT '0.00',
  `status` int(11) NOT NULL DEFAULT '0',
  `vat` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `invoices_invoice_number_unique` (`invoice_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=293 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

UPDATE `migrations` SET `id` = 274,`migration` = '2014_10_12_000000_create_users_table',`batch` = 1 WHERE `migrations`.`id` = 274;
UPDATE `migrations` SET `id` = 275,`migration` = '2014_10_12_100000_create_password_resets_table',`batch` = 1 WHERE `migrations`.`id` = 275;
UPDATE `migrations` SET `id` = 276,`migration` = '2018_06_21_161840_create_admins_table',`batch` = 1 WHERE `migrations`.`id` = 276;
UPDATE `migrations` SET `id` = 277,`migration` = '2018_06_21_161920_create_products_table',`batch` = 1 WHERE `migrations`.`id` = 277;
UPDATE `migrations` SET `id` = 278,`migration` = '2018_06_24_140720_create_categories_table',`batch` = 1 WHERE `migrations`.`id` = 278;
UPDATE `migrations` SET `id` = 279,`migration` = '2018_06_30_233046_create_credits_table',`batch` = 1 WHERE `migrations`.`id` = 279;
UPDATE `migrations` SET `id` = 280,`migration` = '2018_07_01_212126_create_invoices_table',`batch` = 1 WHERE `migrations`.`id` = 280;
UPDATE `migrations` SET `id` = 281,`migration` = '2018_07_05_151057_create_settings_table',`batch` = 1 WHERE `migrations`.`id` = 281;
UPDATE `migrations` SET `id` = 282,`migration` = '2018_07_07_024421_create_posts_table',`batch` = 1 WHERE `migrations`.`id` = 282;
UPDATE `migrations` SET `id` = 283,`migration` = '2018_07_07_032231_create_pages_table',`batch` = 1 WHERE `migrations`.`id` = 283;
UPDATE `migrations` SET `id` = 284,`migration` = '2018_07_07_081109_create_banks_table',`batch` = 1 WHERE `migrations`.`id` = 284;
UPDATE `migrations` SET `id` = 285,`migration` = '2018_07_11_155645_create_currencies_table',`batch` = 1 WHERE `migrations`.`id` = 285;
UPDATE `migrations` SET `id` = 286,`migration` = '2018_07_18_092530_create_reports_table',`batch` = 1 WHERE `migrations`.`id` = 286;
UPDATE `migrations` SET `id` = 287,`migration` = '2018_07_18_220051_create_gateways_table',`batch` = 1 WHERE `migrations`.`id` = 287;
UPDATE `migrations` SET `id` = 288,`migration` = '2018_07_29_163304_create_sliders_table',`batch` = 1 WHERE `migrations`.`id` = 288;
UPDATE `migrations` SET `id` = 289,`migration` = '2018_08_04_211753_create_crawler_jumias_table',`batch` = 1 WHERE `migrations`.`id` = 289;
UPDATE `migrations` SET `id` = 290,`migration` = '2018_08_04_211837_create_crawler_kongas_table',`batch` = 1 WHERE `migrations`.`id` = 290;
UPDATE `migrations` SET `id` = 291,`migration` = '2018_08_04_211858_create_crawler_ebays_table',`batch` = 1 WHERE `migrations`.`id` = 291;
UPDATE `migrations` SET `id` = 292,`migration` = '2018_08_04_213359_create_crawler_amazons_table',`batch` = 1 WHERE `migrations`.`id` = 292;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

UPDATE `pages` SET `id` = 1,`title` = 'About',`slug` = 'about',`content` = 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).',`created_at` = '2018-12-16 20:40:39',`updated_at` = '2018-12-16 20:40:39' WHERE `pages`.`id` = 1;
UPDATE `pages` SET `id` = 2,`title` = 'Policy Privacy',`slug` = 'policy-privacy',`content` = 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).',`created_at` = '2018-12-16 20:40:39',`updated_at` = '2018-12-16 20:40:39' WHERE `pages`.`id` = 2;
UPDATE `pages` SET `id` = 3,`title` = 'Terms and Conditions',`slug` = 'tos',`content` = 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).',`created_at` = '2018-12-16 20:40:39',`updated_at` = '2018-12-16 20:40:39' WHERE `pages`.`id` = 3;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `author` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` decimal(9,2) DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `affiliate_url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `original_url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` mediumint(9) NOT NULL,
  `views_count` bigint(20) NOT NULL DEFAULT '0',
  `click_count` bigint(20) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL DEFAULT '1',
  `rating_id` int(11) DEFAULT '4',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

CREATE TABLE IF NOT EXISTS `reports` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `source` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `destination` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `currency_symbol` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `currency_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `site_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `site_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `site_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `site_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `site_about` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `keywords` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'keywords,keyword',
  `meta_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'Compare get the best deal',
  `search_element` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'amount',
  `search_order` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT 'desc',
  `currency_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'USD',
  `disqus` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'https://comparison-1.disqus.com/embed.js',
  `social_facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'https://facebook.com',
  `social_twitter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'https://twitter.com',
  `social_instagram` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'https://instagram.com',
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `csv_import_limit` int(11) DEFAULT '1000',
  `live_production` tinyint(1) DEFAULT '1',
  `home_rand_pro` int(11) DEFAULT '8',
  `home_posts` int(11) DEFAULT '4',
  `home_users` int(11) DEFAULT '6',
  `compare_percentage` int(11) DEFAULT '50',
  `compared_products` int(11) DEFAULT '10',
  `enable_admin` tinyint(1) DEFAULT '0',
  `buy_button` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'BuyNow!',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

UPDATE `settings` SET `id` = 1,`currency_symbol` = '$',`currency_name` = 'United States Dollars',`site_name` = 'Affiliate Boss Compare',`site_email` = 'info@products.com.ng',`site_number` = '+123456789',`site_address` = 'Lorem ipsum dolor sit amet, anim id est laborum. Sed ut perspconsectetur, adipisci vam aliquam qua',`site_about` = 'Lorem ipsum dolor sit amet, anim id est laborum. Sed ut perspconsectetur, adipisci vam aliquam qua',`keywords` = 'keywords,keyword',`meta_name` = 'Compare get the best deal',`search_element` = 'amount',`search_order` = 'desc',`currency_code` = 'USD',`disqus` = 'https://comparison-1.disqus.com/embed.js',`social_facebook` = 'https://facebook.com',`social_twitter` = 'https://twitter.com',`social_instagram` = 'https://instagram.com',`logo` = 'uploads/logo/logo.png',`csv_import_limit` = 1000,`live_production` = 1,`home_rand_pro` = 8,`home_posts` = 4,`home_users` = 6,`compare_percentage` = 50,`compared_products` = 10,`enable_admin` = 0,`buy_button` = 'BuyNow!',`created_at` = '2018-12-16 20:40:59',`updated_at` = '2018-12-16 20:40:59' WHERE `settings`.`id` = 1;

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE IF NOT EXISTS `sliders` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders`
--

UPDATE `sliders` SET `id` = 1,`title` = 'Slider1',`image` = 'uploads/slider/1.jpg',`url` = '',`created_at` = '2018-12-16 20:40:59',`updated_at` = '2018-12-16 20:40:59' WHERE `sliders`.`id` = 1;
UPDATE `sliders` SET `id` = 2,`title` = 'Slider2',`image` = 'uploads/slider/2.jpg',`url` = '',`created_at` = '2018-12-16 20:40:59',`updated_at` = '2018-12-16 20:40:59' WHERE `sliders`.`id` = 2;
UPDATE `sliders` SET `id` = 3,`title` = 'Slider3',`image` = 'uploads/slider/3.jpg',`url` = '',`created_at` = '2018-12-16 20:41:00',`updated_at` = '2018-12-16 20:41:00' WHERE `sliders`.`id` = 3;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `about` text COLLATE utf8mb4_unicode_ci,
  `phone_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `credit` decimal(9,2) NOT NULL DEFAULT '0.00',
  `currency_id` int(11) NOT NULL DEFAULT '147',
  `validation_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `price_update_block` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `price_update_element` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `description_update_element` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_name_unique` (`name`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
