-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 16, 2018 at 10:45 PM
-- Server version: 5.7.19
-- PHP Version: 7.1.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecommerce`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `display_name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'rootuser', 'Root', 'info@products.com.ng', '$2y$10$skJItBy6MkU9BqVglTIo/OCokvaipHNKHM2lYNOhUocecD0hpGtR.', NULL, '2018-12-16 20:40:39', '2018-12-16 20:40:39');

-- --------------------------------------------------------

--
-- Table structure for table `banks`
--

DROP TABLE IF EXISTS `banks`;
CREATE TABLE `banks` (
  `id` int(10) UNSIGNED NOT NULL,
  `account_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bank_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `other_details` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banks`
--

INSERT INTO `banks` (`id`, `account_name`, `account_number`, `bank_name`, `other_details`, `created_at`, `updated_at`) VALUES
(1, 'John Doe', '987654321', 'Gurantee Trust Bank', 'Extra Information Such as Swift Code', '2018-12-16 20:40:40', '2018-12-16 20:40:40');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `name`, `slug`, `description`, `created_at`, `updated_at`) VALUES
(1, 0, 'General', 'general', 'Default Root Table', '2018-12-16 20:40:39', '2018-12-16 20:40:39');

-- --------------------------------------------------------

--
-- Table structure for table `crawler_amazons`
--

DROP TABLE IF EXISTS `crawler_amazons`;
CREATE TABLE `crawler_amazons` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `crawler_ebays`
--

DROP TABLE IF EXISTS `crawler_ebays`;
CREATE TABLE `crawler_ebays` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_block_ini` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'li.s-item',
  `product_name_element` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'h3.s-item__title',
  `product_url_element` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'a.s-item__link',
  `product_image_element` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'img.s-item__image-img',
  `product_price_element` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'span.s-item__price',
  `affiliate_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `crawler_ebays`
--

INSERT INTO `crawler_ebays` (`id`, `product_block_ini`, `product_name_element`, `product_url_element`, `product_image_element`, `product_price_element`, `affiliate_id`, `created_at`, `updated_at`) VALUES
(1, 'li.s-item', 'h3.s-item__title', 'a.s-item__link', 'img.s-item__image-img', 'span.s-item__price', '', '2018-12-16 20:41:00', '2018-12-16 20:41:00');

-- --------------------------------------------------------

--
-- Table structure for table `crawler_jumias`
--

DROP TABLE IF EXISTS `crawler_jumias`;
CREATE TABLE `crawler_jumias` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_block_ini` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'div[class="sku -gallery"]',
  `product_name_element` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'span[class="name"]',
  `product_url_element` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'a[class="link"]',
  `product_image_element` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '.lazy.image',
  `product_price_element` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'span.price',
  `affiliate_id_start` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `affiliate_id_end` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `crawler_jumias`
--

INSERT INTO `crawler_jumias` (`id`, `product_block_ini`, `product_name_element`, `product_url_element`, `product_image_element`, `product_price_element`, `affiliate_id_start`, `affiliate_id_end`, `created_at`, `updated_at`) VALUES
(1, 'div[class=\"sku -gallery\"]', 'span[class=\"name\"]', 'a[class=\"link\"]', '.lazy.image', 'span.price', '', '', '2018-12-16 20:41:00', '2018-12-16 20:41:00');

-- --------------------------------------------------------

--
-- Table structure for table `crawler_kongas`
--

DROP TABLE IF EXISTS `crawler_kongas`;
CREATE TABLE `crawler_kongas` (
  `id` int(10) UNSIGNED NOT NULL,
  `affiliate_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'quickprice',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `crawler_kongas`
--

INSERT INTO `crawler_kongas` (`id`, `affiliate_id`, `created_at`, `updated_at`) VALUES
(1, '', '2018-12-16 20:41:00', '2018-12-16 20:41:00');

-- --------------------------------------------------------

--
-- Table structure for table `credits`
--

DROP TABLE IF EXISTS `credits`;
CREATE TABLE `credits` (
  `id` int(10) UNSIGNED NOT NULL,
  `package_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `rate_per_click` decimal(9,2) NOT NULL DEFAULT '1.00',
  `min` int(11) NOT NULL DEFAULT '1',
  `max` int(11) NOT NULL DEFAULT '1000000',
  `vat` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `credits`
--

INSERT INTO `credits` (`id`, `package_name`, `description`, `rate_per_click`, `min`, `max`, `vat`, `created_at`, `updated_at`) VALUES
(1, 'Package Name', 'Package Description', '5.00', 50, 1000000, 0, '2018-12-16 20:40:39', '2018-12-16 20:40:39');

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

DROP TABLE IF EXISTS `currencies`;
CREATE TABLE `currencies` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `symbol` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `html_entity` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `name`, `code`, `symbol`, `html_entity`, `created_at`, `updated_at`) VALUES
(1, 'United Arab Emirates Dirham', 'AED', 'د.إ', '', '2018-12-16 20:40:40', '2018-12-16 20:40:40'),
(2, 'Afghan Afghani', 'AFN', '؋', '', '2018-12-16 20:40:40', '2018-12-16 20:40:40'),
(3, 'Albanian Lek', 'ALL', 'L', '', '2018-12-16 20:40:40', '2018-12-16 20:40:40'),
(4, 'Armenian Dram', 'AMD', 'դր.', '', '2018-12-16 20:40:40', '2018-12-16 20:40:40'),
(5, 'Netherlands Antillean Gulden', 'ANG', 'ƒ', '&#x0192;', '2018-12-16 20:40:40', '2018-12-16 20:40:40'),
(6, 'Angolan Kwanza', 'AOA', 'Kz', '', '2018-12-16 20:40:40', '2018-12-16 20:40:40'),
(7, 'Argentine Peso', 'ARS', '$', '&#x20B1;', '2018-12-16 20:40:40', '2018-12-16 20:40:40'),
(8, 'Australian Dollar', 'AUD', '$', '$', '2018-12-16 20:40:40', '2018-12-16 20:40:40'),
(9, 'Aruban Florin', 'AWG', 'ƒ', '&#x0192;', '2018-12-16 20:40:40', '2018-12-16 20:40:40'),
(10, 'Azerbaijani Manat', 'AZN', 'null', '', '2018-12-16 20:40:40', '2018-12-16 20:40:40'),
(11, 'Bosnia and Herzegovina Convertible Mark', 'BAM', 'КМ', '', '2018-12-16 20:40:40', '2018-12-16 20:40:40'),
(12, 'Barbadian Dollar', 'BBD', '$', '$', '2018-12-16 20:40:41', '2018-12-16 20:40:41'),
(13, 'Bangladeshi Taka', 'BDT', '৳', '', '2018-12-16 20:40:41', '2018-12-16 20:40:41'),
(14, 'Bulgarian Lev', 'BGN', 'лв', '', '2018-12-16 20:40:41', '2018-12-16 20:40:41'),
(15, 'Bahraini Dinar', 'BHD', 'ب.د', '', '2018-12-16 20:40:41', '2018-12-16 20:40:41'),
(16, 'Burundian Franc', 'BIF', 'Fr', '', '2018-12-16 20:40:41', '2018-12-16 20:40:41'),
(17, 'Bermudian Dollar', 'BMD', '$', '$', '2018-12-16 20:40:41', '2018-12-16 20:40:41'),
(18, 'Brunei Dollar', 'BND', '$', '$', '2018-12-16 20:40:41', '2018-12-16 20:40:41'),
(19, 'Bolivian Boliviano', 'BOB', 'Bs.', '', '2018-12-16 20:40:41', '2018-12-16 20:40:41'),
(20, 'Brazilian Real', 'BRL', 'R$', 'R$', '2018-12-16 20:40:41', '2018-12-16 20:40:41'),
(21, 'Bahamian Dollar', 'BSD', '$', '$', '2018-12-16 20:40:41', '2018-12-16 20:40:41'),
(22, 'Bhutanese Ngultrum', 'BTN', 'Nu.', '', '2018-12-16 20:40:41', '2018-12-16 20:40:41'),
(23, 'Botswana Pula', 'BWP', 'P', '', '2018-12-16 20:40:41', '2018-12-16 20:40:41'),
(24, 'Belarusian Ruble', 'BYR', 'Br', '', '2018-12-16 20:40:41', '2018-12-16 20:40:41'),
(25, 'Belize Dollar', 'BZD', '$', '$', '2018-12-16 20:40:41', '2018-12-16 20:40:41'),
(26, 'Canadian Dollar', 'CAD', '$', '$', '2018-12-16 20:40:41', '2018-12-16 20:40:41'),
(27, 'Congolese Franc', 'CDF', 'Fr', '', '2018-12-16 20:40:41', '2018-12-16 20:40:41'),
(28, 'Swiss Franc', 'CHF', 'Fr', '', '2018-12-16 20:40:41', '2018-12-16 20:40:41'),
(29, 'Unidad de Fomento', 'CLF', 'UF', '&#x20B1;', '2018-12-16 20:40:42', '2018-12-16 20:40:42'),
(30, 'Chilean Peso', 'CLP', '$', '&#36;', '2018-12-16 20:40:42', '2018-12-16 20:40:42'),
(31, 'Chinese Renminbi Yuan', 'CNY', '¥', '&#20803;', '2018-12-16 20:40:42', '2018-12-16 20:40:42'),
(32, 'Colombian Peso', 'COP', '$', '&#x20B1;', '2018-12-16 20:40:42', '2018-12-16 20:40:42'),
(33, 'Costa Rican Colón', 'CRC', '₡', '&#x20A1;', '2018-12-16 20:40:42', '2018-12-16 20:40:42'),
(34, 'Cuban Convertible Peso', 'CUC', '$', '', '2018-12-16 20:40:42', '2018-12-16 20:40:42'),
(35, 'Cuban Peso', 'CUP', '$', '&#x20B1;', '2018-12-16 20:40:42', '2018-12-16 20:40:42'),
(36, 'Cape Verdean Escudo', 'CVE', '$', '', '2018-12-16 20:40:42', '2018-12-16 20:40:42'),
(37, 'Czech Koruna', 'CZK', 'Kč', '', '2018-12-16 20:40:42', '2018-12-16 20:40:42'),
(38, 'Djiboutian Franc', 'DJF', 'Fdj', '', '2018-12-16 20:40:42', '2018-12-16 20:40:42'),
(39, 'Danish Krone', 'DKK', 'kr', '', '2018-12-16 20:40:42', '2018-12-16 20:40:42'),
(40, 'Dominican Peso', 'DOP', '$', '&#x20B1;', '2018-12-16 20:40:42', '2018-12-16 20:40:42'),
(41, 'Algerian Dinar', 'DZD', 'د.ج', '', '2018-12-16 20:40:42', '2018-12-16 20:40:42'),
(42, 'Egyptian Pound', 'EGP', 'ج.م', '&#x00A3;', '2018-12-16 20:40:42', '2018-12-16 20:40:42'),
(43, 'Eritrean Nakfa', 'ERN', 'Nfk', '', '2018-12-16 20:40:42', '2018-12-16 20:40:42'),
(44, 'Ethiopian Birr', 'ETB', 'Br', '', '2018-12-16 20:40:43', '2018-12-16 20:40:43'),
(45, 'Euro', 'EUR', '€', '&#x20AC;', '2018-12-16 20:40:43', '2018-12-16 20:40:43'),
(46, 'Fijian Dollar', 'FJD', '$', '$', '2018-12-16 20:40:43', '2018-12-16 20:40:43'),
(47, 'Falkland Pound', 'FKP', '£', '&#x00A3;', '2018-12-16 20:40:43', '2018-12-16 20:40:43'),
(48, 'British Pound', 'GBP', '£', '&#x00A3;', '2018-12-16 20:40:43', '2018-12-16 20:40:43'),
(49, 'Georgian Lari', 'GEL', 'ლ', '', '2018-12-16 20:40:43', '2018-12-16 20:40:43'),
(50, 'Ghanaian Cedi', 'GHS', '₵', '&#x20B5;', '2018-12-16 20:40:43', '2018-12-16 20:40:43'),
(51, 'Gibraltar Pound', 'GIP', '£', '&#x00A3;', '2018-12-16 20:40:43', '2018-12-16 20:40:43'),
(52, 'Gambian Dalasi', 'GMD', 'D', '', '2018-12-16 20:40:43', '2018-12-16 20:40:43'),
(53, 'Guinean Franc', 'GNF', 'Fr', '', '2018-12-16 20:40:43', '2018-12-16 20:40:43'),
(54, 'Guatemalan Quetzal', 'GTQ', 'Q', '', '2018-12-16 20:40:43', '2018-12-16 20:40:43'),
(55, 'Guyanese Dollar', 'GYD', '$', '$', '2018-12-16 20:40:43', '2018-12-16 20:40:43'),
(56, 'Hong Kong Dollar', 'HKD', '$', '$', '2018-12-16 20:40:43', '2018-12-16 20:40:43'),
(57, 'Honduran Lempira', 'HNL', 'L', '', '2018-12-16 20:40:43', '2018-12-16 20:40:43'),
(58, 'Croatian Kuna', 'HRK', 'kn', '', '2018-12-16 20:40:43', '2018-12-16 20:40:43'),
(59, 'Haitian Gourde', 'HTG', 'G', '', '2018-12-16 20:40:44', '2018-12-16 20:40:44'),
(60, 'Hungarian Forint', 'HUF', 'Ft', '', '2018-12-16 20:40:44', '2018-12-16 20:40:44'),
(61, 'Indonesian Rupiah', 'IDR', 'Rp', '', '2018-12-16 20:40:44', '2018-12-16 20:40:44'),
(62, 'Israeli New Sheqel', 'ILS', '₪', '&#x20AA;', '2018-12-16 20:40:44', '2018-12-16 20:40:44'),
(63, 'Indian Rupee', 'INR', '₹', '&#x20b9;', '2018-12-16 20:40:44', '2018-12-16 20:40:44'),
(64, 'Iraqi Dinar', 'IQD', 'ع.د', '', '2018-12-16 20:40:44', '2018-12-16 20:40:44'),
(65, 'Iranian Rial', 'IRR', '﷼', '&#xFDFC;', '2018-12-16 20:40:44', '2018-12-16 20:40:44'),
(66, 'Icelandic Króna', 'ISK', 'kr', '', '2018-12-16 20:40:44', '2018-12-16 20:40:44'),
(67, 'Jamaican Dollar', 'JMD', '$', '$', '2018-12-16 20:40:44', '2018-12-16 20:40:44'),
(68, 'Jordanian Dinar', 'JOD', 'د.ا', '', '2018-12-16 20:40:44', '2018-12-16 20:40:44'),
(69, 'Japanese Yen', 'JPY', '¥', '&#x00A5;', '2018-12-16 20:40:44', '2018-12-16 20:40:44'),
(70, 'Kenyan Shilling', 'KES', 'KSh', '', '2018-12-16 20:40:44', '2018-12-16 20:40:44'),
(71, 'Kyrgyzstani Som', 'KGS', 'som', '', '2018-12-16 20:40:45', '2018-12-16 20:40:45'),
(72, 'Cambodian Riel', 'KHR', '៛', '&#x17DB;', '2018-12-16 20:40:45', '2018-12-16 20:40:45'),
(73, 'Comorian Franc', 'KMF', 'Fr', '', '2018-12-16 20:40:45', '2018-12-16 20:40:45'),
(74, 'North Korean Won', 'KPW', '₩', '&#x20A9;', '2018-12-16 20:40:45', '2018-12-16 20:40:45'),
(75, 'South Korean Won', 'KRW', '₩', '&#x20A9;', '2018-12-16 20:40:45', '2018-12-16 20:40:45'),
(76, 'Kuwaiti Dinar', 'KWD', 'د.ك', '', '2018-12-16 20:40:46', '2018-12-16 20:40:46'),
(77, 'Cayman Islands Dollar', 'KYD', '$', '$', '2018-12-16 20:40:46', '2018-12-16 20:40:46'),
(78, 'Kazakhstani Tenge', 'KZT', '〒', '', '2018-12-16 20:40:46', '2018-12-16 20:40:46'),
(79, 'Lao Kip', 'LAK', '₭', '&#x20AD;', '2018-12-16 20:40:46', '2018-12-16 20:40:46'),
(80, 'Lebanese Pound', 'LBP', 'ل.ل', '&#x00A3;', '2018-12-16 20:40:46', '2018-12-16 20:40:46'),
(81, 'Sri Lankan Rupee', 'LKR', '₨', '&#x0BF9;', '2018-12-16 20:40:46', '2018-12-16 20:40:46'),
(82, 'Liberian Dollar', 'LRD', '$', '$', '2018-12-16 20:40:46', '2018-12-16 20:40:46'),
(83, 'Lesotho Loti', 'LSL', 'L', '', '2018-12-16 20:40:46', '2018-12-16 20:40:46'),
(84, 'Lithuanian Litas', 'LTL', 'Lt', '', '2018-12-16 20:40:46', '2018-12-16 20:40:46'),
(85, 'Latvian Lats', 'LVL', 'Ls', '', '2018-12-16 20:40:46', '2018-12-16 20:40:46'),
(86, 'Libyan Dinar', 'LYD', 'ل.د', '', '2018-12-16 20:40:47', '2018-12-16 20:40:47'),
(87, 'Moroccan Dirham', 'MAD', 'د.م.', '', '2018-12-16 20:40:47', '2018-12-16 20:40:47'),
(88, 'Moldovan Leu', 'MDL', 'L', '', '2018-12-16 20:40:47', '2018-12-16 20:40:47'),
(89, 'Malagasy Ariary', 'MGA', 'Ar', '', '2018-12-16 20:40:47', '2018-12-16 20:40:47'),
(90, 'Macedonian Denar', 'MKD', 'ден', '', '2018-12-16 20:40:47', '2018-12-16 20:40:47'),
(91, 'Myanmar Kyat', 'MMK', 'K', '', '2018-12-16 20:40:48', '2018-12-16 20:40:48'),
(92, 'Mongolian Tögrög', 'MNT', '₮', '&#x20AE;', '2018-12-16 20:40:48', '2018-12-16 20:40:48'),
(93, 'Macanese Pataca', 'MOP', 'P', '', '2018-12-16 20:40:48', '2018-12-16 20:40:48'),
(94, 'Mauritanian Ouguiya', 'MRO', 'UM', '', '2018-12-16 20:40:48', '2018-12-16 20:40:48'),
(95, 'Mauritian Rupee', 'MUR', '₨', '&#x20A8;', '2018-12-16 20:40:48', '2018-12-16 20:40:48'),
(96, 'Maldivian Rufiyaa', 'MVR', 'MVR', '', '2018-12-16 20:40:49', '2018-12-16 20:40:49'),
(97, 'Malawian Kwacha', 'MWK', 'MK', '', '2018-12-16 20:40:49', '2018-12-16 20:40:49'),
(98, 'Mexican Peso', 'MXN', '$', '$', '2018-12-16 20:40:49', '2018-12-16 20:40:49'),
(99, 'Malaysian Ringgit', 'MYR', 'RM', '', '2018-12-16 20:40:49', '2018-12-16 20:40:49'),
(100, 'Mozambican Metical', 'MZN', 'MTn', '', '2018-12-16 20:40:49', '2018-12-16 20:40:49'),
(101, 'Namibian Dollar', 'NAD', '$', '$', '2018-12-16 20:40:50', '2018-12-16 20:40:50'),
(102, 'Nigerian Naira', 'NGN', '₦', '&#x20A6;', '2018-12-16 20:40:50', '2018-12-16 20:40:50'),
(103, 'Nicaraguan Córdoba', 'NIO', 'C$', '', '2018-12-16 20:40:50', '2018-12-16 20:40:50'),
(104, 'Norwegian Krone', 'NOK', 'kr', 'kr', '2018-12-16 20:40:50', '2018-12-16 20:40:50'),
(105, 'Nepalese Rupee', 'NPR', '₨', '&#x20A8;', '2018-12-16 20:40:51', '2018-12-16 20:40:51'),
(106, 'New Zealand Dollar', 'NZD', '$', '$', '2018-12-16 20:40:51', '2018-12-16 20:40:51'),
(107, 'Omani Rial', 'OMR', 'ر.ع.', '&#xFDFC;', '2018-12-16 20:40:52', '2018-12-16 20:40:52'),
(108, 'Panamanian Balboa', 'PAB', 'B/.', '', '2018-12-16 20:40:52', '2018-12-16 20:40:52'),
(109, 'Peruvian Nuevo Sol', 'PEN', 'S/.', 'S/.', '2018-12-16 20:40:52', '2018-12-16 20:40:52'),
(110, 'Papua New Guinean Kina', 'PGK', 'K', '', '2018-12-16 20:40:52', '2018-12-16 20:40:52'),
(111, 'Philippine Peso', 'PHP', '₱', '&#x20B1;', '2018-12-16 20:40:52', '2018-12-16 20:40:52'),
(112, 'Pakistani Rupee', 'PKR', '₨', '&#x20A8;', '2018-12-16 20:40:53', '2018-12-16 20:40:53'),
(113, 'Polish Złoty', 'PLN', 'zł', 'z&#322;', '2018-12-16 20:40:53', '2018-12-16 20:40:53'),
(114, 'Paraguayan Guaraní', 'PYG', '₲', '&#x20B2;', '2018-12-16 20:40:53', '2018-12-16 20:40:53'),
(115, 'Qatari Riyal', 'QAR', 'ر.ق', '&#xFDFC;', '2018-12-16 20:40:53', '2018-12-16 20:40:53'),
(116, 'Romanian Leu', 'RON', 'Lei', '', '2018-12-16 20:40:53', '2018-12-16 20:40:53'),
(117, 'Serbian Dinar', 'RSD', 'РСД', '', '2018-12-16 20:40:53', '2018-12-16 20:40:53'),
(118, 'Russian Ruble', 'RUB', 'р.', '&#x0440;&#x0443;&#x0431;', '2018-12-16 20:40:53', '2018-12-16 20:40:53'),
(119, 'Rwandan Franc', 'RWF', 'FRw', '', '2018-12-16 20:40:53', '2018-12-16 20:40:53'),
(120, 'Saudi Riyal', 'SAR', 'ر.س', '&#xFDFC;', '2018-12-16 20:40:54', '2018-12-16 20:40:54'),
(121, 'Solomon Islands Dollar', 'SBD', '$', '$', '2018-12-16 20:40:54', '2018-12-16 20:40:54'),
(122, 'Seychellois Rupee', 'SCR', '₨', '&#x20A8;', '2018-12-16 20:40:54', '2018-12-16 20:40:54'),
(123, 'Sudanese Pound', 'SDG', '£', '', '2018-12-16 20:40:54', '2018-12-16 20:40:54'),
(124, 'Swedish Krona', 'SEK', 'kr', '', '2018-12-16 20:40:54', '2018-12-16 20:40:54'),
(125, 'Singapore Dollar', 'SGD', '$', '$', '2018-12-16 20:40:54', '2018-12-16 20:40:54'),
(126, 'Saint Helenian Pound', 'SHP', '£', '&#x00A3;', '2018-12-16 20:40:54', '2018-12-16 20:40:54'),
(127, 'Slovak Koruna', 'SKK', 'Sk', '', '2018-12-16 20:40:55', '2018-12-16 20:40:55'),
(128, 'Sierra Leonean Leone', 'SLL', 'Le', '', '2018-12-16 20:40:55', '2018-12-16 20:40:55'),
(129, 'Somali Shilling', 'SOS', 'Sh', '', '2018-12-16 20:40:55', '2018-12-16 20:40:55'),
(130, 'Surinamese Dollar', 'SRD', '$', '', '2018-12-16 20:40:55', '2018-12-16 20:40:55'),
(131, 'South Sudanese Pound', 'SSP', '£', '&#x00A3;', '2018-12-16 20:40:55', '2018-12-16 20:40:55'),
(132, 'São Tomé and Príncipe Dobra', 'STD', 'Db', '', '2018-12-16 20:40:55', '2018-12-16 20:40:55'),
(133, 'Salvadoran Colón', 'SVC', '₡', '&#x20A1;', '2018-12-16 20:40:55', '2018-12-16 20:40:55'),
(134, 'Syrian Pound', 'SYP', '£S', '&#x00A3;', '2018-12-16 20:40:55', '2018-12-16 20:40:55'),
(135, 'Swazi Lilangeni', 'SZL', 'L', '', '2018-12-16 20:40:56', '2018-12-16 20:40:56'),
(136, 'Thai Baht', 'THB', '฿', '&#x0E3F;', '2018-12-16 20:40:56', '2018-12-16 20:40:56'),
(137, 'Tajikistani Somoni', 'TJS', 'ЅМ', '', '2018-12-16 20:40:56', '2018-12-16 20:40:56'),
(138, 'Turkmenistani Manat', 'TMT', 'T', '', '2018-12-16 20:40:56', '2018-12-16 20:40:56'),
(139, 'Tunisian Dinar', 'TND', 'د.ت', '', '2018-12-16 20:40:56', '2018-12-16 20:40:56'),
(140, 'Tongan Paʻanga', 'TOP', 'T$', '', '2018-12-16 20:40:56', '2018-12-16 20:40:56'),
(141, 'Turkish Lira', 'TRY', 'TL', '', '2018-12-16 20:40:56', '2018-12-16 20:40:56'),
(142, 'Trinidad and Tobago Dollar', 'TTD', '$', '$', '2018-12-16 20:40:56', '2018-12-16 20:40:56'),
(143, 'New Taiwan Dollar', 'TWD', '$', '$', '2018-12-16 20:40:56', '2018-12-16 20:40:56'),
(144, 'Tanzanian Shilling', 'TZS', 'Sh', '', '2018-12-16 20:40:57', '2018-12-16 20:40:57'),
(145, 'Ukrainian Hryvnia', 'UAH', '₴', '&#x20B4;', '2018-12-16 20:40:57', '2018-12-16 20:40:57'),
(146, 'Ugandan Shilling', 'UGX', 'USh', '', '2018-12-16 20:40:57', '2018-12-16 20:40:57'),
(147, 'United States Dollar', 'USD', '$', '$', '2018-12-16 20:40:57', '2018-12-16 20:40:57'),
(148, 'Uruguayan Peso', 'UYU', '$', '&#x20B1;', '2018-12-16 20:40:57', '2018-12-16 20:40:57'),
(149, 'Uzbekistani Som', 'UZS', 'null', '', '2018-12-16 20:40:57', '2018-12-16 20:40:57'),
(150, 'Venezuelan Bolívar', 'VEF', 'Bs F', '', '2018-12-16 20:40:57', '2018-12-16 20:40:57'),
(151, 'Vietnamese Đồng', 'VND', '₫', '&#x20AB;', '2018-12-16 20:40:57', '2018-12-16 20:40:57'),
(152, 'Vanuatu Vatu', 'VUV', 'Vt', '', '2018-12-16 20:40:57', '2018-12-16 20:40:57'),
(153, 'Samoan Tala', 'WST', 'T', '', '2018-12-16 20:40:58', '2018-12-16 20:40:58'),
(154, 'Central African Cfa Franc', 'XAF', 'Fr', '', '2018-12-16 20:40:58', '2018-12-16 20:40:58'),
(155, 'Silver (Troy Ounce)', 'XAG', 'oz t', '', '2018-12-16 20:40:58', '2018-12-16 20:40:58'),
(156, 'Gold (Troy Ounce)', 'XAU', 'oz t', '', '2018-12-16 20:40:58', '2018-12-16 20:40:58'),
(157, 'East Caribbean Dollar', 'XCD', '$', '$', '2018-12-16 20:40:58', '2018-12-16 20:40:58'),
(158, 'Special Drawing Rights', 'XDR', 'SDR', '$', '2018-12-16 20:40:58', '2018-12-16 20:40:58'),
(159, 'West African Cfa Franc', 'XOF', 'Fr', '', '2018-12-16 20:40:59', '2018-12-16 20:40:59'),
(160, 'Cfp Franc', 'XPF', 'Fr', '', '2018-12-16 20:40:59', '2018-12-16 20:40:59'),
(161, 'Yemeni Rial', 'YER', '﷼', '&#xFDFC;', '2018-12-16 20:40:59', '2018-12-16 20:40:59'),
(162, 'South African Rand', 'ZAR', 'R', '&#x0052;', '2018-12-16 20:40:59', '2018-12-16 20:40:59'),
(163, 'Zambian Kwacha', 'ZMK', 'ZK', '', '2018-12-16 20:40:59', '2018-12-16 20:40:59'),
(164, 'Zambian Kwacha', 'ZMW', 'ZK', '', '2018-12-16 20:40:59', '2018-12-16 20:40:59');

-- --------------------------------------------------------

--
-- Table structure for table `gateways`
--

DROP TABLE IF EXISTS `gateways`;
CREATE TABLE `gateways` (
  `id` int(10) UNSIGNED NOT NULL,
  `bankwire_active` tinyint(1) DEFAULT '1',
  `paypal_active` tinyint(1) DEFAULT '1',
  `paypal_client_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paypal_client_secret` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stripe_active` tinyint(1) DEFAULT '1',
  `stripe_publishable_key` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stripe_secret_key` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `voguepay_active` tinyint(1) DEFAULT '1',
  `voguepay_merchant_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gateways`
--

INSERT INTO `gateways` (`id`, `bankwire_active`, `paypal_active`, `paypal_client_id`, `paypal_client_secret`, `stripe_active`, `stripe_publishable_key`, `stripe_secret_key`, `voguepay_active`, `voguepay_merchant_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'AXb8idM5vv4RPIgVi5GD6bEcEzZg7T11xCiclUfGslJVMbBKvm-U40zikgZNRXEdYdXliBzsZrJCBDJ8', 'EOtz3yaEggAFcW8gHeBK1CBCmXZVkwQaR9KmitRxHwq0MHfkFDF2A7u27DYMMugcCA-w9fvkXJNmt3bX', 1, 'pk_test_qKe8nGFSUZkLRt0ETMieMh80', 'sk_test_ySExMEvYiX71wvqDmLAMu1UC', 1, 'demo', '2018-12-16 20:40:59', '2018-12-16 20:40:59');

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

DROP TABLE IF EXISTS `invoices`;
CREATE TABLE `invoices` (
  `id` int(10) UNSIGNED NOT NULL,
  `package_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `invoice_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_method` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Nil',
  `rate_per_click` decimal(9,2) NOT NULL DEFAULT '1.00',
  `amount` decimal(9,2) NOT NULL DEFAULT '0.00',
  `amount_raw` decimal(9,2) NOT NULL DEFAULT '0.00',
  `status` int(11) NOT NULL DEFAULT '0',
  `vat` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(274, '2014_10_12_000000_create_users_table', 1),
(275, '2014_10_12_100000_create_password_resets_table', 1),
(276, '2018_06_21_161840_create_admins_table', 1),
(277, '2018_06_21_161920_create_products_table', 1),
(278, '2018_06_24_140720_create_categories_table', 1),
(279, '2018_06_30_233046_create_credits_table', 1),
(280, '2018_07_01_212126_create_invoices_table', 1),
(281, '2018_07_05_151057_create_settings_table', 1),
(282, '2018_07_07_024421_create_posts_table', 1),
(283, '2018_07_07_032231_create_pages_table', 1),
(284, '2018_07_07_081109_create_banks_table', 1),
(285, '2018_07_11_155645_create_currencies_table', 1),
(286, '2018_07_18_092530_create_reports_table', 1),
(287, '2018_07_18_220051_create_gateways_table', 1),
(288, '2018_07_29_163304_create_sliders_table', 1),
(289, '2018_08_04_211753_create_crawler_jumias_table', 1),
(290, '2018_08_04_211837_create_crawler_kongas_table', 1),
(291, '2018_08_04_211858_create_crawler_ebays_table', 1),
(292, '2018_08_04_213359_create_crawler_amazons_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `slug`, `content`, `created_at`, `updated_at`) VALUES
(1, 'About', 'about', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '2018-12-16 20:40:39', '2018-12-16 20:40:39'),
(2, 'Policy Privacy', 'policy-privacy', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '2018-12-16 20:40:39', '2018-12-16 20:40:39'),
(3, 'Terms and Conditions', 'tos', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '2018-12-16 20:40:39', '2018-12-16 20:40:39');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `author` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` decimal(9,2) DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `affiliate_url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `original_url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` mediumint(9) NOT NULL,
  `views_count` bigint(20) NOT NULL DEFAULT '0',
  `click_count` bigint(20) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL DEFAULT '1',
  `rating_id` int(11) DEFAULT '4',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

DROP TABLE IF EXISTS `reports`;
CREATE TABLE `reports` (
  `id` int(10) UNSIGNED NOT NULL,
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `source` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `destination` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `currency_symbol` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `currency_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `site_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `site_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `site_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `site_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `site_about` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `keywords` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'keywords,keyword',
  `meta_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'Compare get the best deal',
  `search_element` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'amount',
  `search_order` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT 'desc',
  `currency_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'USD',
  `disqus` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'https://comparison-1.disqus.com/embed.js',
  `social_facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'https://facebook.com',
  `social_twitter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'https://twitter.com',
  `social_instagram` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'https://instagram.com',
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `csv_import_limit` int(11) DEFAULT '1000',
  `live_production` tinyint(1) DEFAULT '1',
  `home_rand_pro` int(11) DEFAULT '8',
  `home_posts` int(11) DEFAULT '4',
  `home_users` int(11) DEFAULT '6',
  `compare_percentage` int(11) DEFAULT '50',
  `compared_products` int(11) DEFAULT '10',
  `enable_admin` tinyint(1) DEFAULT '0',
  `buy_button` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'BuyNow!',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `currency_symbol`, `currency_name`, `site_name`, `site_email`, `site_number`, `site_address`, `site_about`, `keywords`, `meta_name`, `search_element`, `search_order`, `currency_code`, `disqus`, `social_facebook`, `social_twitter`, `social_instagram`, `logo`, `csv_import_limit`, `live_production`, `home_rand_pro`, `home_posts`, `home_users`, `compare_percentage`, `compared_products`, `enable_admin`, `buy_button`, `created_at`, `updated_at`) VALUES
(1, '$', 'United States Dollars', 'Affiliate Boss Compare', 'info@products.com.ng', '+123456789', 'Lorem ipsum dolor sit amet, anim id est laborum. Sed ut perspconsectetur, adipisci vam aliquam qua', 'Lorem ipsum dolor sit amet, anim id est laborum. Sed ut perspconsectetur, adipisci vam aliquam qua', 'keywords,keyword', 'Compare get the best deal', 'amount', 'desc', 'USD', 'https://comparison-1.disqus.com/embed.js', 'https://facebook.com', 'https://twitter.com', 'https://instagram.com', 'uploads/logo/logo.png', 1000, 1, 8, 4, 6, 50, 10, 0, 'BuyNow!', '2018-12-16 20:40:59', '2018-12-16 20:40:59');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

DROP TABLE IF EXISTS `sliders`;
CREATE TABLE `sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `title`, `image`, `url`, `created_at`, `updated_at`) VALUES
(1, 'Slider1', 'uploads/slider/1.jpg', '', '2018-12-16 20:40:59', '2018-12-16 20:40:59'),
(2, 'Slider2', 'uploads/slider/2.jpg', '', '2018-12-16 20:40:59', '2018-12-16 20:40:59'),
(3, 'Slider3', 'uploads/slider/3.jpg', '', '2018-12-16 20:41:00', '2018-12-16 20:41:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `about` text COLLATE utf8mb4_unicode_ci,
  `phone_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `credit` decimal(9,2) NOT NULL DEFAULT '0.00',
  `currency_id` int(11) NOT NULL DEFAULT '147',
  `validation_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `price_update_block` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `price_update_element` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `description_update_element` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_name_unique` (`name`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `banks`
--
ALTER TABLE `banks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`);

--
-- Indexes for table `crawler_amazons`
--
ALTER TABLE `crawler_amazons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crawler_ebays`
--
ALTER TABLE `crawler_ebays`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crawler_jumias`
--
ALTER TABLE `crawler_jumias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crawler_kongas`
--
ALTER TABLE `crawler_kongas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `credits`
--
ALTER TABLE `credits`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `credits_package_name_unique` (`package_name`);

--
-- Indexes for table `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gateways`
--
ALTER TABLE `gateways`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `invoices_invoice_number_unique` (`invoice_number`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reports`
--
ALTER TABLE `reports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_name_unique` (`name`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `banks`
--
ALTER TABLE `banks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `crawler_amazons`
--
ALTER TABLE `crawler_amazons`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `crawler_ebays`
--
ALTER TABLE `crawler_ebays`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `crawler_jumias`
--
ALTER TABLE `crawler_jumias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `crawler_kongas`
--
ALTER TABLE `crawler_kongas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `credits`
--
ALTER TABLE `credits`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=165;

--
-- AUTO_INCREMENT for table `gateways`
--
ALTER TABLE `gateways`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=293;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reports`
--
ALTER TABLE `reports`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
